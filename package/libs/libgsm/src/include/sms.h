#ifndef SMS_H
#define SMS_H

#include "modem.h"
#include "uci_function.h"

#include <signal.h>
#include <stdio.h> // Standard input/output definitions
#include <stdlib.h>
#include <string.h> // String function definitions

//reikalingi gsmctl_get_ip funkcijai
#include <netdb.h>
#include <ifaddrs.h>
#include "operator_list.h"

//reikia statuso gavimui
#include <time.h>

#define VERSION "0.2b"

#define TIMEOUT_DEFAULT 6
#define TIMEOUT_SMSSEND 16
#define TIMEOUT_LONG 205

#define OK_ERR			 0
#define NO_ERR			-1
#define READ_ERR		-2
#define PACKET_ERR	-3
#define PARSE_ERR		-4
#define SYSCALL_ERR	-5
#define OPTION_ERR	-6
#define UCICALL_ERR	-7
#define IFADDRCALL_ERR	-8

#define CME_10_ERR	10		// sim not inserted
#define CME_11_ERR	11		// sim pin required
#define CME_12_ERR	12		// sim puk required
#define CME_13_ERR	13		// sim failure
#define CME_14_ERR	14		// sim busy
#define CME_15_ERR	15		// sim wrong
#define CME_20_ERR	20		// memory full
#define CME_21_ERR	21		// invalid memory index
#define CME_22_ERR	22		// not found
#define CME_23_ERR	23		// memory failure
#define CME_30_ERR	30		// No network service failure
#define CME_100_ERR	100		// unknown error

#define CMS_300_ERR	300		// me failure
#define CMS_301_ERR	301		// sms service of me reserved
#define CMS_302_ERR	302		// operation not allowed
#define CMS_303_ERR	303		// operation not supported
#define CMS_304_ERR	304		// invalid pdu mode parameter
#define CMS_305_ERR	305		// invalid text mode parameter
#define CMS_310_ERR	310		// sim not inserted
#define CMS_311_ERR	311		// sim pin required
#define CMS_312_ERR	312		// ph-sim pin required
#define CMS_313_ERR	313		// sim failure
#define CMS_314_ERR	314		// sim busy
#define CMS_315_ERR	315		// sim wrong
#define CMS_316_ERR	316		// sim puk required
#define CMS_317_ERR	317		// sim pin2 required
#define CMS_318_ERR	318		// sim puk2 required
#define CMS_320_ERR	320		// memory failure
#define CMS_321_ERR	321		// invalid memory index
#define CMS_322_ERR	322		// memory full
#define CMS_500_ERR	500		// unknow error

#define BUFFER_SIZE 128

// Supported modes.
typedef enum {
	MODE_GSM,
	MODE_SMS,
	MODE_SHELL,
} APPMODE;

// SMS structure.
typedef struct {
	unsigned char SMSC_Length;
	unsigned char SMSC_Type;
	char SMSC_Number[32];
	int FO_SMSD;
	unsigned char AddrLength;
	unsigned char AddrType;
	char Sender[32];	// sms sender
	unsigned char TP_PID;
	unsigned char TP_DCS;
	char TimeStamp[24];	// sms time
	unsigned char DataLength;
	unsigned char DataheaderLength;
	char Data[161];		// sms text
} SMS;

// SMS structure full.
typedef struct {
	int Index;		// sms Index
	int Status;	// sms Status
	SMS stru_for_pdu;
} ONE_SMS;

// SMS structure with element number in it.
typedef struct {
	int num;
	int err;
	ONE_SMS  *list;
} SMS_LIST;

// Supported commands.
typedef enum {
	IP,
	CONNSTATE,
	NETSTATE,
	IMEI,
	MODEL,
	MANUF,
	SERIALNUM,
	REVISION,
	IMSI,
	ICCID,
	LAC,
	MCC,
	MNC,
	COPS,
	PRIOR,
	PINSTATE,
	SIMSTATE,
	SIGNAL,
	RSCP,
	ECIO,
	RSRP,
	SINR,
	RSRQ,
	CELLID,
	OPERATOR,
	OPERNUM,
	CONNTYPE,
	BSENT,
	BRECV,
	ATCMD,
	TEMPERATURE,
	SMSREAD,
	SMSLIST,
	SMSMEM,
	SMSSEND,
	SMSSEND_B64,
	SMSDEL,
	SHUTDOWN,
	PINCOUNT
} request_buffer;

// GSM functions.
int gsmctl_send(int socket, char *command, char **output);
int gsmctl_get(int socket, char *command, char **output);
int gsmctl_get_ip(char *ifname, char **output);
int gsmctl_get_connstate(int socket, modem_dev device, char **output);
int gsmctl_get_netstate(int socket, modem_dev device, char **output);
int gsmctl_get_imei(int socket, modem_dev device, char **output);
int gsmctl_get_model(int socket, modem_dev device, char **output);
int gsmctl_get_manuf(int socket, modem_dev device, char **output);
int gsmctl_get_serialnum(int socket, modem_dev device, char **output);
int gsmctl_get_rev(int socket, modem_dev device, char **output);
int gsmctl_get_imsi(int socket, modem_dev device, char **output);
int gsmctl_get_iccid(int socket, modem_dev device, char **output);
int gsmctl_get_pinstate(int socket, modem_dev device, char **output);
int gsmctl_get_simstate(int socket, modem_dev device, char **output);
int gsmctl_get_signal_quality(int socket, modem_dev device, char **output);
int gsmctl_get_signal_rscp(int socket, modem_dev device, char **output); //-------
int gsmctl_get_signal_ecio(int socket, modem_dev device, char **output); //-------
int gsmctl_get_signal_rsrp(int socket, modem_dev device, char **output); //-------
int gsmctl_get_signal_sinr(int socket, modem_dev device, char **output); //-------
int gsmctl_get_signal_rsrq(int socket, modem_dev device, char **output); //-------
int gsmctl_get_signal_cell_id(int socket, modem_dev device, char **output); //-------
int gsmctl_get_operator(int socket, modem_dev device, char **output);
int gsmctl_get_opernum(int socket, modem_dev device, char **output);
int gsmctl_get_conntype(int socket, modem_dev device, char **output);
int gsmctl_get_iface_stats(char *ifname, const char *side, char **output);
int gsmctl_get_auxiliary(int socket, modem_dev device, char *cmd, char **output);
int gsmctl_get_temperature(int socket, modem_dev device, char **output);
int gsmctl_get_pincount(int socket, modem_dev device, char **output);
int gsmctl_get_lac(int socket, modem_dev device, char **output);
int gsmctl_get_mcc(int socket, modem_dev device, char **output);
int gsmctl_get_mnc(int socket, modem_dev device, char **output);
int gsmctl_get_network_selection_mode(int socket, modem_dev device, char **output);
int gsmctl_get_network_access_order(int socket, modem_dev device, char **output);

// SMS functions.
int message_parser(char *pBuffer, SMS *pSMS);
int utf8_to_8859_converter(unsigned char *buffer, char *output, int sms_num, int sms_length);
unsigned char char_converter(unsigned int letter);
int create_pdu(char *number, char *text, char *pdu, char nr_type[3], int all_sms, int sms_num, int random, int sms_length);
SMS_LIST gsmctl_get_sms_list_struct(int socket, modem_dev device, int index, char **output);
int gsmctl_get_sms(int socket, modem_dev device, int index, int total, char **output);
int gsmctl_get_sms_list(int socket, modem_dev device, char *list, char **output);
int gsmctl_get_sms_memory(int socket, modem_dev device, char **output);
int gsmctl_send_sms(int socket, modem_dev device, char *number, char *text, char **output);
int gsmctl_delete_sms(int socket, modem_dev device, int index, int total, char **output);

// Based functions.
int check_output_errors(char *output);
void check_alocate(void *name);
void quit(int error_code);
void check_error(int err_code, char **buffer);

//
void option_list_init(request_buffer *requests);

// Parse msg functions to get status.
char *read_msg(int socket, modem_dev device, char *text, char *event_type, char *event_text);
char *get_status_values(int socket, modem_dev device, char *param, char *event, char *event_txt);

unsigned char __attribute__((weak)) nolog;
char __attribute__((weak)) *appname_gsmlib = "gsmlib";

#endif
