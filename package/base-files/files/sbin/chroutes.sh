#!/bin/sh
# Copyright (C) 2015 Teltonika

local int=0
while [ $int -lt 30 ]; do
	config=`uci -q get network.@route[$int]`
	if [ ! "$config" == "" ]; then
		local interface=`uci -q get network.@route[$int].interface`
		if [ ! "$interface" == "wan" ] && [ ! "$interface" == "lan" ] && [ ! "$interface" == "ppp" ]; then
			local target=`uci -q get network.@route[$int].target`
			local netmask=`uci -q get network.@route[$int].netmask`
			local gw=`uci -q get network.@route[$int].gateway`
			local metric=`uci -q get network.@route[$int].metric`
			local add_route=""
			
			if [ ! "$netmask" == "" ]; then
				netmask="netmask $netmask"
			fi
			if [ ! "$gw" == "" ]; then
				gw="gw $gw"
			fi
			if [ ! "$metric" == "" ]; then
				metric="metric $metric"
			fi
			
			add_route=$(/sbin/route add -net $target $netmask $gw $metric dev $interface >/dev/null 2>&1)
			
			if [ "$?" == "0" ]; then
				logger -t network "Route '$target $netmask' added on interface '${interface}'"
			fi
		fi
	else
		break
	fi
	int=`expr $int + 1`
done
