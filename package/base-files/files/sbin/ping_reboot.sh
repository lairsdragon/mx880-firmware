#!/bin/sh

do_action() {
	local failure=$2

	case $1 in
		1)
			logger -p $LOG_LEVEL PING Reboot. Rebooting router after $failure unsuccessful retries
			reboot -p
			;;
		2)
			logger -p $LOG_LEVEL PING Reboot. Restarting modem after $failure unsuccessful retries
			/etc/init.d/modem restart
			$CONFIG_SET.fail_counter=0
			;;
		3)
			logger -p $LOG_LEVEL PING Reboot. Restarting mobile data connection after $failure unsuccessful retries
			ifdown ppp
			sleep 1
			ifup ppp
			$CONFIG_SET.fail_counter=0
			;;
		4)
			logger -p $LOG_LEVEL PING Reboot. Reregistering after $failure unsuccessful retries
			ifdown ppp
			/usr/sbin/gsmctl -A AT+COPS=2
			/etc/init.d/gsmd restart
			$CONFIG_SET.fail_counter=0
			;;
		*)
			logger -p $LOG_LEVEL PING Reboot. $failure unsuccessful retries
			$CONFIG_SET.fail_counter=0
			;;
	esac
}

LOG_LEVEL=5
SECTION=$1
CONFIG_GET="uci get ping_reboot.$SECTION"
CONFIG_SET="uci set ping_reboot.$SECTION"

ENABLE=`$CONFIG_GET.enable`

if [ ! `ls /tmp/ping_reboot_offset 2>/dev/null` ]; then
	echo `tr -cd 0-1 </dev/urandom | head -c 1``tr -cd 0-9 </dev/urandom | head -c 2` > /tmp/ping_reboot_offset
	logger -p $LOG_LEVEL "ping reboot offset is set to" `cat /tmp/ping_reboot_offset`
fi

sleep `cat /tmp/ping_reboot_offset`

if [ "$ENABLE" -eq 1 ]; then


	WAIT=`$CONFIG_GET.time_out`
	P_SIZE=`$CONFIG_GET.packet_size`
	ACTION=`$CONFIG_GET.action`
	RETRY=`$CONFIG_GET.retry`
	INTERFACE=`$CONFIG_GET.interface`

	if [ "$INTERFACE" -eq 2 ]; then
		GET_SIM_SLOT=`/sbin/gpio.sh get SIM`
		if [ "$GET_SIM_SLOT" -eq 0 ]; then
			HOST=`$CONFIG_GET.host2`
		else
			HOST=`$CONFIG_GET.host1`
		fi
		IFNAME=`uci get -q network.ppp.ifname`
	else
		HOST=`$CONFIG_GET.host`
	fi

	if [ "$INTERFACE" -eq 2 ]; then
		ping -c 1 -W $WAIT -s $P_SIZE $HOST -I $IFNAME > /dev/null 2>&1
	else
		ping -c 1 -W $WAIT -s $P_SIZE $HOST > /dev/null 2>&1
	fi

	case $? in
		0)
			$CONFIG_SET.fail_counter=0
			logger -p $LOG_LEVEL PING Reboot. PING echo receive sucecss.
			exit 0
			;;
		1)
			local failure=`$CONFIG_GET.fail_counter`
			failure=$(( failure + 1 ))
			$CONFIG_SET.fail_counter=$failure

			logger -p $LOG_LEVEL PING Reboot. PING echo receive failed. Retry $failure

			if [ "$failure" -ge "$RETRY" ] ; then
				do_action $ACTION $failure
# 				reboot -p
			else
				exit 0
			fi
			;;
		*)
			exit 1
			;;
	esac
fi

