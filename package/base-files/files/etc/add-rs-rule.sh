#! /bin/sh

local kk="0"
local vv="0"
set_rules(){
	config_get interface "$1" "interface" ""
	if [ "$interface" != "" ]; then
		config_get allow_ip "$1" "allow_ip" ""
		IFS=$' '
		for j in $allow_ip; do
			if [ "$interface" == "wan" ]; then
				iptables -I zone_${interface}_input --proto $protocol -s $j --destination-port $port_listen -j ACCEPT -m comment --comment "Enable_$4"
				kk="1"
			elif [ "$interface" == "vpn" ]; then
				iptables -I zone_${interface}_input --proto $protocol -s $j --destination-port $port_listen -j ACCEPT -m comment --comment "Enable_$4"
				vv="1"
			else
				iptables -A zone_${interface}_input --proto $protocol -s $j --destination-port $port_listen -j ACCEPT -m comment --comment "Enable_$4"
			fi
		done

		if [ "$interface" == "lan" ]; then
			iptables -A zone_${interface}_input --proto $protocol --destination-port $port_listen -j REJECT -m comment --comment "Enable_$4"
		fi
		if [ "$interface" == "wan" ]; then
			if [ "$kk" == "0" ]; then
				iptables -I zone_${interface}_input --proto $protocol -s "0.0.0.0/0" --destination-port $port_listen -j ACCEPT -m comment --comment "Enable_$4"
				kk="1"
			fi
		fi
		if [ "$interface" == "vpn" ]; then
			if [ "$vv" == "0" ]; then
				iptables -I zone_${interface}_input --proto $protocol -s "0.0.0.0/0" --destination-port $port_listen -j ACCEPT -m comment --comment "Enable_$4"
				vv="1"
			fi
		fi
	fi
}

check_enable(){
if [ "$1" == "1" ]; then
	rs="$2"
	type=`uci get -q rs."$rs".type`

	rules_set=0
	if [ "$type" == "modem" ]; then
		rules_set=1

	elif [ "$type" == "overip" ]; then
		mode=`uci get -q rs."$rs".mode`

		if [ "$mode" == "bidirect" -o "$mode" == "server" ]; then
			rules_set=1
		fi
	fi

	if [ "$rules_set" == "1" ]; then
		. /lib/functions.sh
		config_load "rs"

		config_get type "$rs" "type" ""
		config_get protocol "$rs" "protocol" ""
		config_get port_listen "$rs" "port_listen" ""
		
		if [ "$type" == "modem" ]; then
			protocol="tcp"
			config_get port_listen "$rs" "modem_port" ""
		fi

		config_foreach set_rules "ip_filter_$rs" "$protocol" "$port_listen" "$rs"

# 		if [ "$kk" == "0" ]; then
# 			iptables -I zone_wan_input --proto $protocol -s "0.0.0.0/0" --destination-port $port_listen -j ACCEPT -m comment --comment "Enable_$rs"
# 		fi

		#taisykle pridedama, kad veiktu net ijungus DMZ
		iptables -t nat -D prerouting_wan_rule -p $protocol --dport $port_listen -j ACCEPT
		iptables -t nat -I prerouting_wan_rule 1 -p $protocol --dport $port_listen -j ACCEPT

	fi

fi
}

enable_232=`uci get -q rs.rs232.enabled`
check_enable "$enable_232" "rs232"
enable_485=`uci get -q rs.rs485.enabled`
check_enable "$enable_485" "rs485"
