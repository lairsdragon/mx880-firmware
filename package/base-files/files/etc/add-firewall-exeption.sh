#!/bin/sh

. /lib/functions.sh
. /lib/teltonika-functions.sh

fw_dmz_add_exception() {
	local ProtoType=$1
	local Port=$2
	iptables -t nat -D prerouting_${Zone}_rule -p $ProtoType --dport $Port -j ACCEPT
	iptables -t nat -I prerouting_${Zone}_rule 1 -p $ProtoType --dport $Port -j ACCEPT
}

add_dmz_rule() {
	local ip=`uci -q get firewall.DMZ.dest_ip`
	iptables -t nat -D zone_${Zone}_prerouting -p all -j DNAT --to-destination $ip -m comment --comment "DMZ"
	iptables -t nat -A zone_${Zone}_prerouting -p all -j DNAT --to-destination $ip -m comment --comment "DMZ"
}
    
fw_dmz_exceptions() {

	# ssh
	config_get name "$1" "name" ""
	
	case "$name" in
		"Enable_SSH_WAN")
			config_get enabled "$1" "enabled" "1"
			;;
		"Enable_HTTP_WAN")
			config_get enabled "$1" "enabled" "1"
			;;
		"Enable_HTTPS_WAN")
			config_get enabled "$1" "enabled" "1"
			;;
		"SNMP_REMOTE_Access")
			config_get enabled "$1" "enabled" "1"
			;;
		"Enable_SAMBA_WAN")
			config_get enabled "$1" "enabled" "1"
			;;
	esac

	if [ "$enabled" == "1" ]; then
		config_get dest_port "$1" "dest_port" ""
		fw_dmz_add_exception tcp $dest_port 
		fw_dmz_add_exception udp $dest_port 
	fi
}

add_rs_rule() {
	rs232=`uci -q get rs.rs232.enabled`
	if [ "$rs232" == "1" ]; then
		mode=`uci -q get rs.rs232.mode`
		if [ "$mode" == "server" || "$mode" == "bidirect" ]; then
			dest_port=`uci -q get rs.rs232.port_listen`
			fw_dmz_add_exception tcp $dest_port
		fi
	fi
	rs485=`uci -q get rs.rs485.enabled`
	if [ "$rs485" == "1" ]; then
		mode=`uci -q get rs.rs485.mode`
		if [ "$mode" == "server" || "$mode" == "bidirect" ]; then
			dest_port=`uci -q get rs.rs485.port_listen`
			fw_dmz_add_exception tcp $dest_port
		fi
	fi

	#clear existing dmz rule and set dmz rule to down
	add_dmz_rule $Zone
}

enabled=`uci -q get firewall.DMZ.enabled`
local Zone=`uci -q get firewall.DMZ.src`

if [ "$enabled" == "1" ]; then
	config_load firewall
	config_foreach fw_dmz_exceptions "rule"
	add_rs_rule
fi  
