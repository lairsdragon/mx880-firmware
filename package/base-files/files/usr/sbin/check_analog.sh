#!/bin/sh
# Copyright (C) 2015 Teltonika
. /lib/functions.sh
. /lib/teltonika-functions.sh
sleep_delay=5

get_analog_value()
{
	anvalue=`cat /sys/class/hwmon/hwmon0/device/in0_input`
	anvalue=`awk -v anvalue=$anvalue 'BEGIN { print (anvalue / 1000) }'`
}

job_loop()
{
	while [ 1 ]
	do
		sleep_delay=`uci get -q ioman.ioman.interval`
		get_analog_value
		/usr/sbin/ioman.sh "analog" "$anvalue"
		sleep $sleep_delay
	done
}
job_loop
