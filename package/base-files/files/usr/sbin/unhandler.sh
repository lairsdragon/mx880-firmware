#!/bin/sh
. /lib/functions.sh
. /lib/teltonika-functions.sh
DEBUG=""
UNSOLICITED_DATA="/tmp/unsolicited_data"
UNSOLICITED_LOCK="/var/run/unsolicited_data"
UNSOLICITED_DEBUG="/tmp/unsolicited_log"
SMS_DELETE="/sbin/sms_utils/sms_utils -c &"
SMS_READ="/sbin/sms_utils/sms_utils"
CALL_utils="/usr/sbin/call_utilities.sh"
MODULE_VIDPID="get_vidpid_tlt"
signal_strength_level=""

# Writes parameter to file, saves previous state
save_state() {
	name="$1"
	value="$2"
	file="$3"
	lock $UNSOLICITED_LOCK
	prev_value=$(cat "$file" | grep "$name:=" | cut -d'=' -f2 )
	#Exit if value has not been changed
	[ "$value" = "$prev_value" ] &&  {
		lock -u $UNSOLICITED_LOCK
		return
	}
	
	sed -i "/$name""_prev:=/d" "$file" 2>/dev/null
	sed -i "/$name:=/d" "$file" 2>/dev/null
	echo "$name:=$value" >> "$file"
	echo "$name""_prev:=$prev_value" >> "$file"
	lock -u $UNSOLICITED_LOCK
	/usr/sbin/ledsman.sh "$name" "$value"
	[ "$name" = "connstate" ] && log_connection_event "$value" "$file"
	[ "$name" = "conntype" ] && check_con_type "$value"
}

Signal_huawei() {
	set -- $(echo "$1"  | awk -F ',' '{print $1" "$2}')
	local parse_signal=$2
	if [ "$parse_signal" -ge 0 ] && [ "$parse_signal" -le 96 ]; then
		parse_signal="-"$((121-$parse_signal))
	elif [ "$parse_signal" -eq 255 ]; then
		parse_signal="unknown or undetectable"
	else
		parse_signal="error"
	fi
	Signal_script "$parse_signal" 
}

Signal_quectel() {
	set -- $(echo "$1"  | awk -F ',' '{print $1" "$2}')
	local parse_signal=$2
	if [ "$1" == "NOSERVICE" ]; then
		parse_signal="unknown or undetectable"
	fi
	Signal_script "$parse_signal"
}

Signal_telit() {
	case "$1" in
		0)
			Signal_script "-113";;
		1)
			Signal_script "-98";;
		2)
			Signal_script "-93";;
		3)
			Signal_script "-75";;
		4)
			Signal_script "-60";;
		5)
			Signal_script "-50";;
		*)
			Signal_script "-121";;
	esac
}

check_con_type() {
	local ctype=""
	local contype=$1
	if [ $contype == "CDMA" ] || [ $contype == "EDGE" ] || [ $contype == "GPRS" ] || [ $contype == "GSM" ]; then
		ctype="2G"
		/usr/bin/eventslog add "Network Type" "Joined $ctype ($contype)"

	elif [ $contype == "WCDMA" ] || [ $contype == "HSDPA" ] || [ $contype == "HSUPA" ] || [ $contype == "HSPA" ] || [ $contype == "HSPA+" ] || [ $contype == "DC-HSPA+" ]; then
		ctype="3G"
		/usr/bin/eventslog add "Network Type" "Joined $ctype ($contype)"
		
	elif [ $contype == "LTE" ]; then
		ctype="4G"
		/usr/bin/eventslog add "Network Type" "Joined $ctype ($contype)"
	fi
}

log_connection_event() {
	local event=$1
	local file=$2
	
	if [ "$event" == "connected" ]; then
		. /lib/teltonika-functions.sh
		conntype=$(cat "$file" | grep "conntype:=" | cut -d'=' -f2 )
		oper=$(gsmctl -o)
		ppp_ifname=$(uci -q get network.ppp.ifname)
		ipaddr=$(tlt_get_iface_ipaddr "$ppp_ifname")
		/usr/bin/eventslog add "Mobile Data" "Mobile data connected, IP: $ipaddr ($conntype) $oper"
	elif [ "$event" == "disconnected" ]; then
		/usr/bin/eventslog add "Mobile Data" "Mobile data disconnected"
	fi
}

unsolicited_conntype_keeper() {
	local conntype=""
	case "$1" in
		0)
			conntype="GPRS";;
		1)
			conntype="EDGE";;
		2)
			conntype="WCDMA";;
		3)
			conntype="HSDPA";;
		4)
			if [ "$($MODULE_VIDPID)" == "$TELIT_LTE" ]; then
				conntype="LTE"
			elif [ "$($MODULE_VIDPID)" == "$TELIT" ]; then
				conntype="UNKNOWN"
			fi;;
		*)
			conntype="UNKNOWN";;
	esac
	save_state "conntype" "$conntype" "$UNSOLICITED_DATA"
}

unsolicited_conntype_keeper_huawei() {
	contype=$( echo "$1" | cut -d\" -f2 )
	[ "$DEBUG" = 1 ] && echo "conntype:=$contype"  >> $UNSOLICITED_DEBUG
	save_state "conntype" "$contype" "$UNSOLICITED_DATA"
}

unsolicited_conntype_keeper_sierra() {
	[ "$($MODULE_VIDPID)" == "$SIERRA" ] || exit
	local conntype=""
	case "$1" in
		3)
			conntype="GSM";;
		5)
			conntype="UMTS";;
		*)
			conntype="LTE";;
	esac
	save_state "conntype" "$conntype" "$UNSOLICITED_DATA"
}

unsolicited_netstate_keeper() {
	local renew=0
	local netstate=""
	case "$1" in
		0)
			netstate="unregistered";;
		1)
			netstate="registered (home)"
			`/sbin/start_messaged.sh unsolicited_special_sms from_unhandler`
			renew=1
			;;
		2)
			netstate="searching";;
		3)
			netstate="denied";;
		4)
			netstate="unknown";;
		5)
			netstate="registered (roaming)"
			`/sbin/start_messaged.sh unsolicited_special_sms from_unhandler`
			renew=1
			;;
		*)
			netstate="unknown";;
	esac
	[ "$DEBUG" = 1 ] && echo "netstate:=$netstate" >> $UNSOLICITED_DEBUG
	save_state "netstate" "$netstate" "$UNSOLICITED_DATA"

	#Renew DHCP lease for Telit LTE
	if [ "$renew" = 1 -a "$($MODULE_VIDPID)" = "$TELIT_LTE" ]; then
		pid=`ps | grep udhcpc | grep wwan0 | awk -F " " '{print $1}'`
		if [ -n "$pid" ]; then
			logger -t "unhandler" "Reloading DHCP for wwan0"
			kill "$pid"
		fi
	fi

	if [ "$renew" = 1 ]; then
		coper=`gsmctl -o`
		/usr/bin/eventslog add "Network Operator" "Connected to $coper operator"
	fi
}

unsolicited_netstate_keeper_huawei() {
	case "$1" in
		0|1)
			netstate="unregistered"
			[ "$DEBUG" = 1 ] && echo "netstate:=$netstate" >> $UNSOLICITED_DEBUG
			save_state "netstate" "$netstate" "$UNSOLICITED_DATA"
			;;
	esac
}

unsolicited_simstate_keeper() {
	local simstate=""
	case "$1" in
		0)
			simstate="not inserted";;
		1)
			simstate="inserted";;
		2)
			simstate="UNLOCKED";;
		3)
			simstate="READY";;
		*)
			simstate="unknown";;
	esac
	save_state "simstate" "$simstate" "$UNSOLICITED_DATA"
}

unsolicited_simstate_keeper_huawei() {
	local simstate=""
	case "$1" in
		0)
			return 1;;	#Ignore
		1|2|3|4)
			simstate="inserted";;
		255)
			simstate="not inserted";;
		*)
			simstate="unknown";;
	esac
	[ "$DEBUG" = 1 ] && echo "simstate:=$simstate" >> $UNSOLICITED_DEBUG
	save_state "simstate" "$simstate" "$UNSOLICITED_DATA"
}


check_the_rules() {
	
	config_get event "$1" event
	config_get event_Mark "$1" eventMark
	config_get event_enable "$1" enable
	config_get restrict_en "$1" enable_block
	config_get restrict_time "$1" restrict_time
	local now_restricted=`uci -q get events_reporting.send_blocking.blocked` or 0
		
		if [ "$event" == "Signal strength" ] && [ "$event_enable" == "1" ]; then
			if [ "$event_Mark" == "Signal strength droped below -113 dBm" ] || [ "$event_Mark" == "all" ] && [ "$signal_strength_level" -le -113 ]; then
				if [ "$now_restricted" -ne "1" ]; then
					/usr/bin/eventslog insert "Signal strength" "Signal strength droped below -113 dBm"
					if [ "$restrict_en" -eq "1" ]; then
						uci set events_reporting.send_blocking.blocked=1
						uci commit events_reporting
						echo "*/$restrict_time * * * * /sbin/signal_strength_protection.sh">>/etc/crontabs/root
						/etc/init.d/cron restart
					fi
				fi
			elif [ "$event_Mark" == "Signal strength droped below -98 dBm" ] || [ "$event_Mark" == "all" ] && [ "$signal_strength_level" -le -98 ]; then
				if [ "$now_restricted" -ne "1" ]; then
					/usr/bin/eventslog insert "Signal strength" "Signal strength droped below -98 dBm"
					if [ "$restrict_en" -eq "1" ]; then
						uci set events_reporting.send_blocking.blocked=1
						uci commit events_reporting
						echo "*/$restrict_time * * * * /sbin/signal_strength_protection.sh">>/etc/crontabs/root
						/etc/init.d/cron restart
					fi
				fi
			elif [ "$event_Mark" == "Signal strength droped below -93 dBm" ] || [ "$event_Mark" == "all" ] && [ "$signal_strength_level" -le -93 ]; then
				if [ "$now_restricted" -ne "1" ]; then
					/usr/bin/eventslog insert "Signal strength" "Signal strength droped below -93 dBm"
					if [ "$restrict_en" -eq "1" ]; then
						uci set events_reporting.send_blocking.blocked=1
						uci commit events_reporting
						echo "*/$restrict_time * * * * /sbin/signal_strength_protection.sh">>/etc/crontabs/root
						/etc/init.d/cron restart
					fi
				fi
			elif [ "$event_Mark" == "Signal strength droped below -75 dBm" ] || [ "$event_Mark" == "all" ] && [ "$signal_strength_level" -le -75 ]; then
				if [ "$now_restricted" -ne "1" ]; then
					/usr/bin/eventslog insert "Signal strength" "Signal strength droped below -75 dBm"
					if [ "$restrict_en" -eq "1" ]; then
						uci set events_reporting.send_blocking.blocked=1
						uci commit events_reporting
						echo "*/$restrict_time * * * * /sbin/signal_strength_protection.sh">>/etc/crontabs/root
						/etc/init.d/cron restart
					fi
				fi
			elif [ "$event_Mark" == "Signal strength droped below -60 dBm" ] || [ "$event_Mark" == "all" ] && [ "$signal_strength_level" -le -60 ]; then
				if [ "$now_restricted" -ne "1" ]; then
					/usr/bin/eventslog insert "Signal strength" "Signal strength droped below -60 dBm"
					if [ "$restrict_en" -eq "1" ]; then
						uci set events_reporting.send_blocking.blocked=1
						uci commit events_reporting
						echo "*/$restrict_time * * * * /sbin/signal_strength_protection.sh">>/etc/crontabs/root
						/etc/init.d/cron restart
					fi
				fi
			elif [ "$event_Mark" == "Signal strength droped below -50 dBm" ] || [ "$event_Mark" == "all" ] && [ "$signal_strength_level" -le -50 ]; then
				if [ "$now_restricted" -ne "1" ]; then
					/usr/bin/eventslog insert "Signal strength" "Signal strength droped below -50 dBm"
					if [ "$restrict_en" -eq "1" ]; then
						uci set events_reporting.send_blocking.blocked=1
						uci commit events_reporting
						echo "*/$restrict_time * * * * /sbin/signal_strength_protection.sh">>/etc/crontabs/root
						/etc/init.d/cron restart
					fi
				fi
			fi
		fi
}


Signal_script() {
	[ "$DEBUG" = 1 ] && echo "signal:=$1" >> $UNSOLICITED_DEBUG
	save_state "signal" "$1" "$UNSOLICITED_DATA"
	signal_strength_level=$1
	
	config_load events_reporting
	config_foreach check_the_rules rule
}

SMS_script() {
	id=`echo "$1" | awk -F ',' '{print $2}'`
	case "$($MODULE_VIDPID)" in
		"$HUAWEI_LTE")
			id=$((id+1))
			;;
		"$TELIT"|"$TELIT_LTE")
			id="$id"
			;;
	esac
	$SMS_READ -r "$id" &
}

Calling_telit() {
	local phone=`echo "$1" | awk -F '"' '{print $2}'`
	$CALL_utils "$phone" &
}

Roaming() {
	local pppRoaming=`uci -q get network.ppp.roaming`
	if [ "$pppRoaming" == "1" ]; then

		if [ -f /tmp/pppRoamingFlagFile ]
		then
			pppRoamingFlag=1
		fi

		if [ "$1" == "1" ] && [ "$pppRoamingFlag" == "1" ]; then
			ifup ppp
			rm /tmp/pppRoamingFlagFile
		elif [ "$1" == "5" ]; then
			ifdown ppp
			touch /tmp/pppRoamingFlagFile
		fi
	fi
}

operators_control() {
	local enabled=`uci -q get operctl.general.enabled`
	local number=$1

	if [ "$enabled" == "1" ]; then
		if [ "$number" != "5" ] && [ "$number" != "1" ]; then
			/etc/init.d/operators_control start 2>/dev/null &
		fi
	fi
}

interface_hotplug_event() {
	local connstate=""
	case "$1" in
		ifup)
			connstate="connected"
			`/sbin/start_messaged.sh unsolicited_special_email from_unhandler`
			;;
		ifdown)
			connstate="disconnected";;
		*)
			return 1;;
	esac
	[ "$DEBUG" = 1 ] && echo "connstate=$connstate" >> $UNSOLICITED_DEBUG
	save_state "connstate" "$connstate" "$UNSOLICITED_DATA"
}


local params=$(echo "$1" | tr -d ' ')
local command=${params%%:*}
local 	variable=${params#*:}

[ "$DEBUG" = 1 ] && echo \[`date "+%F %T"`\] "$params" >> $UNSOLICITED_DEBUG
case "$command" in
	"#QSS")
		unsolicited_simstate_keeper "$variable"			#save data about status from unsolicited messages
		;;
	"^SIMST")
		unsolicited_simstate_keeper_huawei "$variable"		#save data about status from unsolicited messages
		;;
	"+CREG")
		unsolicited_netstate_keeper "$variable"
		Roaming "$variable"
		operators_control $variable
		#gsmctl -A AT+CMER=3 					#unsolicited for telit on, does not start from startup
		;;
	"^SRVST")
		unsolicited_netstate_keeper_huawei "$variable"
		;;

	"#PSNT")							#save data about status from unsolicited messages
		unsolicited_conntype_keeper "$variable"
		;;
	"^HCSQ")							#Huawei signal change
		Signal_huawei "$variable"
		unsolicited_conntype_keeper_huawei "$variable"
		;;
	"+QCSQ")							#Quectel signal change
		Signal_quectel "$variable"
		unsolicited_conntype_keeper_huawei "$variable"
		;;
	"^MODE")
		unsolicited_conntype_keeper_sierra "$variable"
		;;
	"^NDISSTAT")							#Huawei NDISSTAT
		#/usr/sbin/ledsman.sh
		;;
	"+CMTI")							#New SMS
		SMS_script "$variable"
		;;
	"^SMMEMFULL")							#Huawei SMS full unsolicited
		$SMS_DELETE
		;;
	"+CIEV")
		local ciev_command=${variable%%,*}
		local ciev_variable=${variable#*,}
		case "$ciev_command" in
			"smsfull")					#Telit SMS full unsolicited
				if [ "$ciev_variable" -eq 1 ]; then
					$SMS_DELETE
				fi
				;;
			"rssi")
				Signal_telit "$ciev_variable"
				;;
			#*)
			#	[ "$DEBUG" = 1 ] && echo \[`date "+%F %T"`\] "$1" >> $UNSOLICITED_DEBUG
			#	;;
		esac
		;;
	"+CLIP")							#Telit incoming call
		Calling_telit "$variable"
		;;
	"connection")
		interface_hotplug_event "$variable"
		;;
esac
