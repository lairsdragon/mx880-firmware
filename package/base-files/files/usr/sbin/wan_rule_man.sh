#!/bin/sh

action=$1
ip=$2
interface=$3
need_log="0"
if [ "$need_log" == "1" ]; then
	logger "WWWWWWWWWWWWWWWWWWWWWWWW action=|$1| ip=|$2| interface=|$3|"
fi

delete_old_rules() {
	local rules=""
	ip rule list | grep -w $1 | while read -r line; do
		ipdel=`echo $line | awk -F ' ' '{print $3}'`
		ifacedel=`echo $line | awk -F ' ' '{print $5}'`
		`ip rule del from "$ipdel" table "$ifacedel"`
		if [ "$need_log" == "1" ]; then
			logger "WWWWWWWWWWWWWWWWWWWWWWWW deleted from \"$ipdel\" table \"$ifacedel"
		fi
	done
}

chaeck_for_beckup(){
	local wan2 wan3 wan4
	wan2=`uci get network.wan2.enabled`
	wan3=`uci get network.wan3.enabled`
	[ `uci get network.wan4` ] &&  wan4=`uci get network.wan4.enabled` || wan4=0

	if [ "$wan2" -eq "0" -o "$wan3" -eq "0" -o "$wan4" -eq "0" ]; then
		return 0
	else
		return 1
	fi

}

if [ "$action" == "del_all" ]; then
	delete_old_rules "wan"
	delete_old_rules "wan2"
	delete_old_rules "wan3"
elif [ "$action" == "edit" ]; then
	delete_old_rules "$interface"
	chaeck_for_beckup
	[ "$?" -ne "0" ] && `ip rule add from "$ip" table "$interface"`
	if [ "$need_log" == "1" ]; then
		logger "WWWWWWWWWWWWWWWWWWWWWWWW Edit  from \"$ip\" table \"$interface\""
	fi
elif [ "$action" == "add" ]; then
	chaeck_for_beckup
	[ "$?" -ne "0" ] && `ip rule add from "$ip" table "$interface"`
	if [ "$need_log" == "1" ]; then
		logger "WWWWWWWWWWWWWWWWWWWWWWWW added  from \"$ip\" table \"$interface\""
	fi
fi
