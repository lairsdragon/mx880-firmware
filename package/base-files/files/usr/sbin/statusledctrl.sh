#!/bin/sh
# Copyright (C) 2014 Teltonika

LED_BASE_DIR="/sys/class/leds"
LED_RED="$LED_BASE_DIR/status_red"
LED_GREEN="$LED_BASE_DIR/status_green"

modes="B_GR, B_GYR, LB_GYR, B_R, B_Y, B_G, S_R, S_Y, S_G"

PrintUsage() {
	echo "Usage: `basename $0` [blink mode ($modes)]"
	exit 1
}

if [ $# -ne 1 ]; then
	PrintUsage
fi

LED() {
	local led
	local state
	
	led="status_$1"
	state="$2"
	
	if [ "$state" == "on" ]; then
		state=1
	else
		state=0
	fi
	
	echo "$state" > "$LED_BASE_DIR/$led/brightness"
}

ClearLeds() {
	LED "red" "off"
	LED "green" "off"
}

NoSimBadPin() {
	ClearLeds
	
	while [ 1 ]; do
		LED "red" "on"
		usleep 500000
		LED "red" "off"
		LED "green" "on"
		usleep 500000
		LED "green" "off"
	done
}

Connecting() {
	ClearLeds
	
	while [ 1 ]; do
		LED "red" "on"
		usleep 500000
		LED "green" "on"
		usleep 500000
		LED "red" "off"
		usleep 500000
		LED "green" "off"
	done
}

LongConnecting() {
	ClearLeds
	
	while [ 1 ]; do
		LED "red" "on"
		usleep 1000000
		LED "green" "on"
		usleep 1000000
		LED "red" "off"
		usleep 1000000
		LED "green" "off"
	done
}

G2NoData() {
	ClearLeds
	
	while [ 1 ]; do
		LED "red" "on"
		sleep 1
		LED "red" "off"
		sleep 1
	done
}

G3NoData() {
	ClearLeds
	
	while [ 1 ]; do
		LED "red" "on"
		LED "green" "on"
		sleep 1
		LED "red" "off"
		LED "green" "off"
		sleep 1
	done
}

G4NoData() {
	ClearLeds
	
	while [ 1 ]; do
		LED "green" "on"
		sleep 1
		LED "green" "off"
		sleep 1
	done
}

led_netdev() {
	local ifname
	local led
	
	led=$1
	ifname=$(uci get network.ppp.ifname)
	
	echo "netdev" > "$led/trigger"
	echo "link tx rx" > "$led/mode"
	echo "$ifname" > "$led/device_name"
}

G2On() {
	ClearLeds
	led_netdev $LED_RED
}

G3On() {
	ClearLeds
	led_netdev $LED_GREEN
	led_netdev $LED_RED
}

G4On() {
	ClearLeds
	led_netdev $LED_GREEN
}

case "$1" in
	B_GR)
		NoSimBadPin
		;;
	B_GYR)
		Connecting
		;;
	LB_GYR)
		LongConnecting
		;;
	B_R)
		G2NoData
		;;
	B_Y)
		G3NoData
		;;
	B_G)
		G4NoData
		;;
	S_R)
		G2On
		;;
	S_Y)
		G3On
		;;
	S_G)
		G4On
		;;
	Off)
		ClearLeds
		;;
	*)
		PrintUsage
		;;
esac
