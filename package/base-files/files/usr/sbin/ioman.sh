#!/bin/sh
# Copyright (C) 2014 Teltonika

. /lib/functions.sh
. /lib/teltonika-functions.sh

WIFI_MAN_SCRIPT="/sbin/wifi"
GPIO_MAN_SCRIPT="/sbin/gpio.sh"
SIM_SWITCH_SCRIPT="/usr/sbin/sim_conf_switch.sh"

if [ $# -ne 2 ]; then
  echo "Usage: `basename $0` [input (digital1, digital2, analog)] [value (0, 1, volts (0.018))]"
  exit 1
fi

dbg="0"
DOUT1=""
DOUT2=""
RUNS="0"

input="$1"
value="$2"

debug(){
	if [ "$dbg" == "1" ]; then
		logger "$1"
	fi
}

debug "INPUT+++++++++++++++++++++++++++++++$input"
debug "VALUE+++++++++++++++++++++++++++++++|$value|"


if [ "$input" == "digital1" ] || [ "$input" == "digital2" ]; then
	if [ "$value" == "0" ]; then
		value="no"
	else
		value="nc"
	fi
fi
debug "VALUE+++++++++++++++++++++++++++++++$value"

SendSMS() {
	local phone_number
	local text

	phone_number="$1"
	text="$2"

	/usr/sbin/gsmctl -S -s "$phone_number $text" &

}

ManageSMS() {
	local rule
	local text

	rule="$1"

	config_get text "$rule" "smstxt" ""
	config_get min "$rule" "min" ""
	config_get max "$rule" "max" ""
	text=`/usr/sbin/parse_msg "$text" "$min" "$max"`

	if [ "$text" != "" ]; then
		config_list_foreach "$rule" "telnum" SendSMS "$text"
	fi
}

FormatRecString() {
	if [ -n "$recipients" ] && [ "$recipients" != "" ]; then
		recipients="$recipients $1"
	else
		recipients="$1"
	fi
}

SendEmail() {
	local rule
	local smtp_host
	local smtp_port
	local subject
	local message
	local user_name
	local password
	local sender
	local secure_conn

	rule="$1"

	config_get smtp_host "$rule" "smtpIP" ""
	config_get smtp_port "$rule" "smtpPort" ""
	config_get subject "$rule" "subject" ""
	config_get message "$rule" "message" ""
	config_get user_name "$rule" "userName" ""
	config_get password "$rule" "password" ""
	config_get sender "$rule" "senderEmail" ""
	config_get secure_conn "$rule" "secureConnection" ""
	config_get min "$rule" "min" ""
	config_get max "$rule" "max" ""

	config_list_foreach "$rule" "recipEmail" FormatRecString

	message=`/usr/sbin/parse_msg "$message" "$min" "$max"`

	if [ -z "$user_name" ] || [ -z "$password" ]; then
		user_name=""
		password=""
	else
		user_name="-au"$user_name
		password="-ap"$password
	fi

	if [ -z "$smtp_host" ] || [ -z "$smtp_port" ] || [ -z "$sender" ]; then
		debug "EXIT"
		return 1
	fi

	if [ "$secure_conn" != "1" ]; then
		debug "SENDING EMAIL NON SECURE"

# 		echo -e "subject:$subject\nfrom:$sender\n\n$message" | sendmail -S "$smtp_host:$smtp_port" -f "$sender" "$user_name" "$password" "$recipients"
		sendmail -S $smtp_host:$smtp_port -f $sender $user_name $password $recipients <<EOF
subject:$subject
from:$sender

$message
EOF
	else
	debug "SENDING EMAIL SECURE"
		sendmail -H "exec openssl s_client -quiet -connect $smtp_host:$smtp_port -tls1 -starttls smtp" -f "$sender" $user_name $password $recipients <<EOF
subject:$subject
from:$sender

$message
EOF
	fi
}

ChangeSIM() {
	$SIM_SWITCH_SCRIPT 2
}

ChangeProfile() {
	local profile_name
	local date
	local profiles_path
	local profile_file

	profile_name="/$1_"

	profiles_path=$(uci get profiles.profiles.path)

	for file in "$profiles_path"/*.tar.gz; do
		case $file in
			*$profile_name*)
				profile_file="$file"
				date=${file##*_}
				date=${date%%.*}
				break
			;;
		esac
	done

	uci set profiles.profiles.profile="$1"
	uci set profiles.profiles.date="$date"
	uci commit

	sysupgrade -r "$profile_file"
	reboot -i
}

ManageProfileChange() {
	local cfg_profiles_list
	local existing_profiles_list=""
	local current_profile
	local profiles_path
	local temp=""
	local first
	local first_found
	local found

	rule="$1"

	config_get cfg_profiles_list $rule "profiles" ""

	if [ "$cfg_profiles_list" == "" ]; then
		return 1
	fi

	profiles_path=$(uci get profiles.profiles.path)
	current_profile=$(uci get profiles.profiles.profile)

	for file in "$profiles_path"/*.tar.gz; do
		file=${file##*/}
		file=${file%%_*}

		if [ "$existing_profiles_list" != "" ]; then
			existing_profiles_list="$existing_profiles_list $file"
		else
			existing_profiles_list="$file"
		fi
	done

	if [ "$existing_profiles_list" == "" ]; then
		return 1
	fi

	for cfg_profile in $cfg_profiles_list; do
		for dir_profile in $existing_profiles_list; do
			if [ "$cfg_profile" == "$dir_profile" ]; then
				if [ "$temp" != "" ]; then
					temp="$temp $cfg_profile"
				else
					temp="$cfg_profile"
				fi
				break
			fi
		done
	done

	cfg_profiles_list="$temp"

	if [ "$cfg_profiles_list" == "" ] || [ "$cfg_profiles_list" == "$current_profile" ]; then
		return 1
	fi

	first_found=0
	found=0
	temp=""
	for profile in $cfg_profiles_list; do
		if [ $found -eq 1 ]; then
			temp="$profile"
			break
		fi

		if [ $first_found -eq 0 ]; then
			first="$profile"
			first_found=1
		fi

		if [ "$profile" == "$current_profile" ]; then
			found=1
		fi
	done

	if [ "$temp" == "" ]; then
		next_profile="$first"
	else
		next_profile="$temp"
	fi

	ChangeProfile "$next_profile"
}

ManageWifi() {
	local mode
	local wan_name
	local index

	mode="$1"

	wan_name=$(uci get network.wan.ifname)

	if [ "$wan_name" == "wlan0" ]; then
		index="1"
	else
		index="0"
	fi

	uci set wireless.@wifi-iface["$index"].user_enable="$mode"
	if [ "$mode" == "1" ]; then
		uci delete wireless.@wifi-iface["$index"].disabled
	else
		uci set wireless.@wifi-iface["$index"].disabled=1
	fi
	uci commit wireless

	$WIFI_MAN_SCRIPT
}

ManageOutput() {
	local rule
	local out_duration
	local output
	local result
	local state
	local active

	debug "ManageOutput"

	rule="$1"

	config_get out_duration $rule "outputtime" "1"
	config_get continuous $rule "continuous" ""

	config_get output $rule "outputnb" ""
	if [ "$output" != "1" ] && [ "$output" != "2" ]; then
		return 1
	fi


	if [ "$continuous" == "1" ]; then
		exist=`$GPIO_MAN_SCRIPT get "DOUT$output"`
		if [ "$input" != "digital1" ] || [ "$input" != "digital2" ];then
			if [ "$exist" == "0" ]; then
				$GPIO_MAN_SCRIPT set "DOUT$output"
			else
				$GPIO_MAN_SCRIPT clear "DOUT$output"
			fi
		else
			if [ "$exist" == "0" ]; then
				$GPIO_MAN_SCRIPT set "DOUT$output"
			fi
			if [ "$output" == "1" ];then
				let DOUT1+=1
			elif [ "$output" == "2" ];then
				let DOUT2+=1
			fi
		fi
	else
		if [ "$RUNS" == "1" ]; then
			$GPIO_MAN_SCRIPT set "DOUT$output" && (sleep "$out_duration"; $GPIO_MAN_SCRIPT clear "DOUT$output")&
		fi
	fi

}

RebootDevice() {
	local rule
	local delay

	rule="$1"

	config_get delay $rule "reboottime" "0"

	reboot -d "$delay" &
}

swich(){
local rule="$1"
local action="$2"

if [ -n "$action" ]; then
debug "ACTION == $action"
	case "$action" in
		sendSMS)
			ManageSMS "$rule"
			;;
		sendEmail)
			SendEmail "$rule"
			;;
		changeProfile)
			ManageProfileChange "$rule"
			;;
		changeSimCard)
			ChangeSIM
			;;
		wifioff)
			ManageWifi "0"
			;;
		wifion)
			ManageWifi "1"
			;;
		output)
			ManageOutput "$rule"
			;;
		reboot)
			RebootDevice "$rule"
			;;
	esac
fi

}

ExecuteRules() {
	local enabled
	local type
	local action
	local triger
	local inputtrigger=""
	local rule="$1"

	config_get enabled "$rule" "enabled" "0"
	debug "rule $rule enabled:$enabled"
	if [ "$enabled" == "1" ]; then

		config_get type "$rule" "type"
		config_get action "$rule" "action"
		config_get triger "$rule" "triger"

		debug "TYPE == $type"
		if [ "$type" == "analog" ]; then
			config_get min "$rule" "min"
			config_get max "$rule" "max"

			debug "triger == $triger"
			debug "min:$min | max$max"

			trigermin=`echo a | awk -v n1="$value" -v n2="$min" '{if (n1<n2) print 1; else print 0;}'`
			trigermax=`echo a | awk -v n1="$value" -v n2="$max" '{if (n1<n2) print 1; else print 0;}'`
			config_get is_event "$rule" "rule"

			if [ "$triger" == "in" -a "$trigermin" == "0" -a "$trigermax" == "1" ] || [ "$triger" == "out" -a "$trigermin" == "1" ] || [ "$triger" == "out" -a "$trigermax" == "0" ]; then
				debug "VVVVVVVVV $is_event"
				if [ "$is_event" == "false" ]; then
					uci set ioman.$rule.rule=true
					uci commit ioman
					RUNS="1"
					swich "$rule" "$action"

					if [ "$triger" == "in" -a "$trigermin" == "0" -a "$trigermax" == "1" ]; then
						/usr/bin/eventslog insert "Input" "Analog input trigered because in ($min v - $max v) range"
					elif [ "$triger" == "out" -a "$trigermin" == "1" ] || [ "$triger" == "out" -a "$trigermax" == "0" ]; then
						/usr/bin/eventslog insert "Input" "Analog input trigered because out of ($min v - $max v) range"
					fi
				fi
			else
				debug "CCCCCCCCCC $is_event"
				if [ "$is_event" == "true" ]; then
					config_get output $rule "outputnb" ""
					if [ "$output" == "1" ];then
						let DOUT1+=0
					fi
					if [ "$output" == "2" ];then
						let DOUT2+=0
					fi
					uci set ioman.$rule.rule=false
					uci commit ioman
				fi
			fi
		fi

		if [ "$type" == "digital1" ] && [ "$input" == "digital1" ]; then
			inputtype="Digital"
			debug "Digital"
			debug "triger ====== $triger"
			debug "value  ====== $value"
			debug "action  ====== $action"

			if [ "$triger" == "both" ]; then
				inputtrigger="Both"
			elif [ "$triger" == "no" ] && [ "$value" == "no" ]; then
				inputtrigger="Input open"
				RUNS="1"
			elif [ "$triger" == "nc" ] && [ "$value" == "nc" ]; then
				inputtrigger="Input shorted"
				RUNS="1"
			fi

			if [ "$inputtrigger" != "" ] || [ "$action" == "output" ]; then
				swich "$rule" "$action"
			fi

			if [ "$value" == "no" ]; then
				inputtrigger="Input open"
			elif [ "$value" == "nc" ]; then
				inputtrigger="Input shorted"
			fi
			/usr/bin/eventslog insert "Input" "$inputtype input trigered by $inputtrigger"

		elif [ "$type" == "digital2" ] && [ "$input" == "digital2" ]; then
			inputtype="Digital isolated"
			debug "Digital isolated"
			debug "triger ====== $triger"
			debug "value  ====== $value"
			debug "action  ====== $action"
			if [ "$triger" == "both" ]; then
				inputtrigger="Both"
			elif [ "$triger" == "no" ] && [ "$value" == "no" ]; then
				inputtrigger="Low logic level"
				RUNS="1"
			elif [ "$triger" == "nc" ] && [ "$value" == "nc" ]; then
				inputtrigger="High logic level"
				RUNS="1"
			fi

			if [ "$inputtrigger" != "" ] || [ "$action" == "output" ]; then
				swich "$rule" "$action"
			fi

			if [ "$value" == "no" ]; then
				inputtrigger="Low logic level"
			elif [ "$value" == "nc" ]; then
				inputtrigger="High logic level"
			fi
			/usr/bin/eventslog insert "Input" "$inputtype input trigered by $inputtrigger"
		fi

	fi
	debug " "
}
#isvalomi failai
echo -ne >/tmp/active_output1
echo -ne >/tmp/active_output2

#loadinamas ioman config
config_load ioman
#eina per rules
config_foreach ExecuteRules "rule"

#jeigu faile 0 reikia isjungti jei ne palikti busiena
debug "LOGGER DOUT1 $DOUT1"
if [ "$DOUT1" == "0" ]; then
	clear1=`/sbin/gpio.sh clear DOUT1`
	if [ "$clear1" == "1" ]; then
		/sbin/gpio.sh clear DOUT1
	fi
fi
debug "LOGGER DOUT2 $DOUT2"
if [ "$DOUT2" == "0" ]; then
	clear2=`/sbin/gpio.sh clear DOUT2`
	if [ "$clear2" == "1" ]; then
		/sbin/gpio.sh clear DOUT2
	fi
fi

debug "#################"
