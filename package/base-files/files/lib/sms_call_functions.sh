#!/bin/sh 
# Copyright (C) 2014 Teltonika

. /lib/functions.sh
. /lib/teltonika-functions.sh
WIFI_MAN_SCRIPT="/sbin/wifi"
AUTO_UPDATE_SCRIPT="/usr/sbin/auto_update.sh"
in_list=false
in_senders_list=false
in_smtp_list=false
recipients=""

SendSMS() {
	gsmctl -S -s "$1 $2"
}

DeleteSMS() {
	gsmctl -S -d "$1"
}

Check_phone() {
	local group="$1"
	local tel="$2"
	group_addr=`uci show sms_utils | grep "sms_utils.@group" | grep ".name=$1" | awk -F '.' '{print $2}'`
	group_phone=`uci get -q sms_utils."$group_addr".tel`
	IFS=$' '
	for phone in $group_phone
	do
		if [ "$phone" == "$tel" ]; then
			return "0"
		fi
	done
	return "1"
}

ManageWifi() {
	local index
	local mode="$1"
	if [ "$2" -eq 1 ]; then
		local wan_name=$(uci get -q network.wan.ifname)
		if [ "$wan_name" == "wlan0" ]; then
			index="1"
		else
			index="0"
		fi
		uci set -q wireless.@wifi-iface["$index"].user_enable="$mode"
		if [ "$mode" -eq 1 ]; then
			uci delete -q wireless.@wifi-iface["$index"].disabled
		else
			uci set -q wireless.@wifi-iface["$index"].disabled=1
		fi
		uci commit wireless
		$WIFI_MAN_SCRIPT
	else
		if [ "$mode" == "1" ]; then
			$WIFI_MAN_SCRIPT "up"
		else
			$WIFI_MAN_SCRIPT "down"
		fi
	fi
}

Manage3G() {
	local mode
	local enabled=`wan_section_enabled type mobile`

	if [ "$enabled" == "1" ] ; then
		mode="$1"
		if [ "$2" == "1" ]; then
			if [ "$mode" == "1" ]; then
				uci set -q network.ppp.enabled=1
				uci commit network
				ifup ppp >/dev/null 2>/dev/null
			else
				uci set -q network.ppp.enabled=0
				uci commit network
				ifdown ppp >/dev/null 2>/dev/null
			fi
		else
			if [ "$mode" == "1" ]; then
				ifup ppp >/dev/null 2>/dev/null
			else
				ifdown ppp >/dev/null 2>/dev/null
			fi
		fi
	fi
}

SendStatus() {
	local text=""
	local message="$2"
	local phone="$1"

	text=`/usr/sbin/parse_msg "$message"`

	SendSMS "$phone" "$text"
}

ManageGPS() {
	if [ "$1" == "0" ]; then
		uci set -q gps.gps.enabled=0
		uci commit gps
		/etc/init.d/gpsd restart >/dev/null 2>/dev/null
	else
		uci set -q gps.gps.enabled=1
		uci commit gps
		/etc/init.d/gpsd restart >/dev/null 2>/dev/null
	fi
}

gps_status() {
	local text=""
	local fix=`gpsctl -f; echo special_str_$?`
	flag=`echo $fix|awk '{print match($0,"special_str_0")}'`
	if [ $flag -gt 0 ];then
		IFS=$'\n'
		for item in $fix
		do
			if [ $item -gt 0 ];then
				local lat=`gpsctl -i`
				local num=`expr length $lat`
				num=`echo $(( num - 7 ))`
				lat=`eval "echo $lat | sed 's/\(.\{$num\}\)/\1./'"`
				local lon=`gpsctl -x`
				num=`expr length $lon`
				num=`echo $(( num - 7 ))`
				lon=`eval "echo $lon | sed 's/\(.\{$num\}\)/\1./'"`
				local time=`echo $(( item / 1000 ))`
				time=`date +"%F %T" @$time`
				text="Fix time: $time Latitude: $lat Longitude: $lon http://maps.google.com/?q=$lat,$lon&om=1speed:0";
			else
				text="No fix"
			fi
			break;
		done
	else
		text="No fix"
	fi
	SendSMS "$1" "$text"
}

send_reply(){
	enable=`uci get sms_utils.auto_reply.enabled`
	if [ $enable -gt 0 ];then
		text=`uci get -q sms_utils.auto_reply.msg`
		type=`uci get -q sms_utils.auto_reply.mode`
		if [ "$type" == "everyone" ]; then
			SendSMS "$1" "$text"
		else
			numbers=`uci get sms_utils.auto_reply.number`
			for number in $numbers; do
				if [ "$number" == "$1" ]; then
					SendSMS "$number" "$text";
					break
				fi
			done
		fi
		delete_msg=`uci get -q sms_utils.auto_reply.delete_msg`
		if [ $delete_msg -gt 0 ]; then
			index=`grep "Index" | gsmctl -S -t | awk -F ": " '{print $2}' | head -1`
			DeleteSMS "$index"
		fi
	fi
}

check_the_numbers() {
	list_number=$1
	sender=$2
	if [ "$sender" == "$list_number" ]; then
		in_list=true
	fi
}
forward_to_http(){

	sender=$1
	in_list=false
	mode=`uci get -q sms_gateway.forwarding_to_http.mode`

	if [ "$mode" == "everyone" ]; then
		in_list=true
	else
		config_load sms_gateway
		config_list_foreach forwarding_to_http number check_the_numbers "$sender"
	fi

	if [ "$in_list" == true ]; then
		text=""
		IFS=$'\n'
		for item in $2
		do
			if [ ! -z "$text" ]; then
				text="$text""%0D%0A""$item"
			else
				text="$item"
			fi
		done
		method=`uci get -q sms_gateway.forwarding_to_http.method`
		url=`uci get -q sms_gateway.forwarding_to_http.url`
		sender_value_name=`uci get -q sms_gateway.forwarding_to_http.number_name`
		message_value_name=`uci get -q sms_gateway.forwarding_to_http.message_name`

		extra_name1=`uci get -q sms_gateway.forwarding_to_http.extra_name1`
		extra_value1=`uci get -q sms_gateway.forwarding_to_http.extra_value1`
		extra_name2=`uci get -q sms_gateway.forwarding_to_http.extra_name2`
		extra_value2=`uci get -q sms_gateway.forwarding_to_http.extra_value2`

		data="$sender_value_name""=""$sender""&""$message_value_name""=""$text"

		if [ ! -z "$extra_name1" -a ! -z "$extra_value1" ]; then
			data="$data""&""$extra_name1""=""$extra_value1"
		fi

		if [ ! -z "$extra_name2" -o ! -z "$extra_value2" ]; then
			data="$data""&""$extra_name2""=""$extra_value2"
		fi
		if [ "$method" == "get" ]; then
			curl --request GET "$url""?""$data"
		elif [ "$method" == "post" ]; then
			curl --data  "$data" "$url"
		fi
	fi
}

format_recipients(){
	if [ -n "$recipients" ]; then
		recipients="$recipients $1"
	else
		recipients="$1"
	fi
}

check_smtp_senders(){
	list_number=$1
	sender=$2
	if [ "$sender" == "$list_number" ]; then
		in_smtp_list=true
	fi
}

check_senders_list(){
	list_number=$1
	sender=$2
	if [ "$sender" == "$list_number" ]; then
		in_senders_list=true
	fi
}

check_the_recipients() {
	list_number=$1
	text=$2
	lock /tmp/sms_forwarding_temp
	response=`gsmctl -Ss "$list_number $text"`
	lock -u /tmp/sms_forwarding_temp
}

forward_to_sms(){
	sender=$1
	text="$2"
	sender_num=`uci get -q sms_gateway.forwarding_to_sms.sender_num`
	mode=`uci get -q sms_gateway.forwarding_to_sms.mode`
	
	if [ "$sender_num" == "1" ]; then
		included_txt=$'\nfrom:\n'
		text_temp="$text$included_txt$sender"
		text_len="${#text_temp}"
		special_cases="${text_temp//[^\]\[\{\}\|~^\\\]}"
		special_case_count="${#special_cases}"
		total_len=$(($text_len+$special_case_count))
		
		if [ "$total_len" -le 480 ]; then
			text="$text_temp"
		fi
	fi
	
	if [ "$mode" == "everyone" ]; then
		in_senders_list=true
	else
		config_load sms_gateway
		config_list_foreach forwarding_to_sms senders_number check_senders_list "$sender"
	fi
	
	if [ "$in_senders_list" == true ]; then
		config_load sms_gateway
		config_list_foreach forwarding_to_sms number check_the_recipients "$text"
	fi
}

forward_to_smtp(){
	sender=$1
	text="$2"
	mode=`uci get -q sms_gateway.forwarding_to_smtp.mode`
	subject=`uci get -q sms_gateway.forwarding_to_smtp.subject`
	username=`uci get -q sms_gateway.forwarding_to_smtp.username`
	password=`uci get -q sms_gateway.forwarding_to_smtp.password`
	smtpip=`uci get -q sms_gateway.forwarding_to_smtp.smtpip`
	smtpport=`uci get -q sms_gateway.forwarding_to_smtp.smtpport`
	secureconnection=`uci get -q sms_gateway.forwarding_to_smtp.secureconnection`
	senderemail=`uci get -q sms_gateway.forwarding_to_smtp.senderemail`
	sendernum=`uci get -q sms_gateway.forwarding_to_smtp.sender_num`
	
	if [ "$sendernum" == "1" ]; then
		if [ -n "$text" ]; then
		text="$text
SMS forwarded from: $sender"
		else
		text="SMS forwarded from: $sender"
		fi
	fi
	
	if [ "$mode" == "everyone" ]; then
		in_smtp_list=true
	else
		config_load sms_gateway
		config_list_foreach forwarding_to_smtp senders_number check_smtp_senders "$sender"
	fi

	if [ "$in_smtp_list" == true ]; then
		config_load sms_gateway
		config_list_foreach forwarding_to_smtp recipemail format_recipients

		if [ "$secureconnection" != "1" ]; then
			sendmail -S "$smtpip:$smtpport" -f "$senderemail" -au"$username" -ap"$password" $recipients<<EOF
subject:$subject
from:$senderemail
$text
EOF
			`/usr/bin/eventslog insert "Mail" "Email was sent to $recipients."`
		else
			sendmail -H "exec openssl s_client -quiet -connect $smtpip:$smtpport -tls1 -starttls smtp" -f "$senderemail" -au"$username" -ap"$password" $recipients <<EOF
subject:$subject
from:$senderemail
$text
EOF
			`/usr/bin/eventslog insert "Mail" "Email was sent to $recipients."`
		fi
	fi
}

fw_upgrade_from_server() {
	local auto_update=`/usr/sbin/auto_update.sh get 000000; echo $?`
	if [ $auto_update -eq 0 ];then
		/sbin/sysupgrade -n /tmp/firmware.img
	fi
}

config_update_from_server() {
	local auto_update=`/usr/sbin/auto_update_conf.sh download; echo $?`
	if [ $auto_update -eq 0 ]; then
		local config_update=`tar -xzf /tmp/config.tar.gz -C /; echo $?`
		if [ $config_update -eq 0 ]; then
			`reboot`
		fi
	fi
}
