#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <openssl/md5.h>
char CONFIG_PART[] = "config";
char TMP_FILE[] = "/tmp/mnf_tmp";
char config_mtd_num[10];


int get_hex(char *off, char *len){
  char str[150];
  char mac[50];
  sprintf(str, "hexdump -s %s -n %s -v -e \"%s/1 \\\"%%02x\\\"\" /dev/mtdblock%s \n", off,len,len,config_mtd_num);
  FILE *tmp;
  extern FILE *popen();
  char buff[50];
  if(!(tmp = popen(str,"r"))){
      printf("Failed to read mac address! \n");
      exit(1);
  }
  while(fgets(buff, sizeof(buff), tmp) !=NULL){
    strcpy (mac, buff);
  }
  return mac;
}

void get_eth_mac(char mac[]){
  char str[100];
  char hexmac[50];
  sprintf(str,"printf %%d 0x%s",mac);
  FILE *tmp;
  extern FILE *popen();
  char buff[50];
  if(!(tmp = popen(str,"r"))){
    printf("Failed to read hex mac address! \n");
    exit(1);
  }
  while(fgets(buff, sizeof(buff), tmp) !=NULL){
    strcpy (hexmac, buff);
  }
  int int_hexmac;
  sscanf(hexmac,"%d",&int_hexmac);
  if(int_hexmac > 281474976710655){
    printf("000000000000\n");
  }else{
    int_hexmac += 1;
    sprintf(str,"printf \"%%0.12X\" %i",int_hexmac);
    if(!(tmp = popen(str,"r"))){
      printf("Failed to read maceth address! \n");
      exit(1);
    }
    while(fgets(buff, sizeof(buff), tmp) !=NULL){
      printf("%s\n",buff);
    }
  }
}


void print_help(){
  printf("Displays manufacturer information\n");
	printf("<PARAM>	-	mac, name, wps, sn, batch, hwver, simpin\n");
}

int read_flash_chunk(char *off, char *len, int var_len){
  char buff[100];
  char temp[100];
  char rd[100];
  char out[100];
  sprintf(temp,"dd if=/dev/mtdblock%s bs=1 skip=%s count=%s 2>/dev/null", config_mtd_num, off, len);
  FILE *tmp;
  extern FILE *popen();
  tmp = popen(temp,"r");
  if( tmp == NULL ){
    printf("Reading error");
    exit(1);
  }
  while( fgets(buff, sizeof(buff), tmp) !=NULL){
    strcpy (rd, buff);
    break;
  }
  fclose(tmp);
  // check if char is printable
  int t=0;
  int g = 0;
  int int_len;
  sscanf(len,"%d",&len);
  for(t=0;t < len ; t++){
    if(!isprint(rd[t])){
    }else{
      out[g] = rd[t];
      g++;
    }
  }
  out[g] = '\0';
  //
  return out;
}

void set_simpin(char *off, char *len, char *val){
  char pin[50]; strcpy(pin,read_flash_chunk(off,len,1));
  if(strcmp(pin,val) != 0){
    int max_len;
    sscanf(len,"%d",&max_len);
    if(strlen(val) < max_len){
      char padd = '\xff';
      int diff = max_len - strlen(val);
      char buff[100];
      int c = 0;
      while(c < diff){
        sprintf(buff,"%c%s",padd,val);
        strcpy(val,buff);
        c++;
      }
      char cmd[100];
      sprintf(cmd, "echo -ne %s | dd of=/dev/mtdblock%s bs=1 count=%s seek=%s",val,config_mtd_num,len,off);
      system(cmd);
    }else if(strlen(val) == max_len){
      char cmd[100];
      sprintf(cmd, "echo -ne %s | dd of=/dev/mtdblock%s bs=1 count=%s seek=%s",val,config_mtd_num,len,off);
      system(cmd);
    }else if(strlen(val) > max_len){
      printf("Value not equal to '12'");
    }
  }
}


int main(int argc, char *argv[])
{
  if( argc > 1 ){
    int var_len = 0;
    char str_config_mtd_num[150];
    sprintf(str_config_mtd_num,"cat /proc/mtd | grep -m 1 \"%s\" | head -c 4 | tail -c 1 \n",CONFIG_PART);
    FILE *tmp;
    extern FILE *popen();
    char buff[10];
    if(!(tmp = popen(str_config_mtd_num,"r"))){
      printf("Partition not found! \n");
      exit(1);
    }
    while(fgets(buff, sizeof(buff), tmp) !=NULL){
      strcpy(config_mtd_num, buff);
    }

    if (strcmp(argv[1],"mac")==0) {
      char offset[] = "0", *offset1;
		  char length[] = "6", *length1;
      length1 = length;
      offset1 = offset;
      char mac[50]; strcpy(mac,get_hex(offset1,length1));
      printf("%s\n",mac);
    }
    else if (strcmp(argv[1],"maceth")==0) {
      char offset[] = "0", *offset1;
		  char length[] = "6", *length1;
      length1 = length;
      offset1 = offset;
      char lanmac[50]; strcpy (lanmac, get_hex(offset1,length1));
      char *lanmac1; lanmac1 = lanmac;
      get_eth_mac(lanmac1);
    }
    else if (strcmp(argv[1],"name")==0) {
      char offset[] = "16", *offset1;
		  char length[] = "12", *length1;
      length1 = length;
      offset1 = offset;
      char out[50]; strcpy(out,read_flash_chunk(offset1,length1,var_len));
      printf("%s\n",out);
    }
    else if (strcmp(argv[1],"wps")==0) {
      char offset[] = "32", *offset1;
		  char length[] = "8", *length1;
      length1 = length;
      offset1 = offset;
      char out[50]; strcpy(out,read_flash_chunk(offset1,length1,var_len));
      printf("%s\n",out);
    }
    else if (strcmp(argv[1],"sn")==0) {
      char offset[] = "48", *offset1;
		  char length[] = "8", *length1;
      length1 = length;
      offset1 = offset;
      char out[50]; strcpy(out,read_flash_chunk(offset1,length1,var_len));
      printf("%s\n",out);
    }
    else if (strcmp(argv[1],"batch")==0) {
      char offset[] = "64", *offset1;
		  char length[] = "4", *length1;
      length1 = length;
      offset1 = offset;
      char out[50]; strcpy(out,read_flash_chunk(offset1,length1,var_len));
      printf("%s\n",out);
    }
    else if (strcmp(argv[1],"hwver")==0) {
      char offset[] = "80", *offset1;
		  char length[] = "4", *length1;
      length1 = length;
      offset1 = offset;
      char out[50]; strcpy(out,read_flash_chunk(offset1,length1,var_len));
      printf("%s\n",out);
    }
    else if (strcmp(argv[1],"simpin")==0) {
      char offset[] = "104", *offset1;
		  char length[] = "12", *length1;
      length1 = length;
      offset1 = offset;
      var_len=1;
      char out[50]; strcpy(out,read_flash_chunk(offset1,length1,var_len));
      if( argc > 2 ){
        if (strcmp(argv[2],"set")==0){
          if(!argv[3]){
            char v[] = "", *v1;
            v1 = v;
            set_simpin(offset1,length1,v1);
          }else{
            set_simpin(offset1,length1,argv[3]);
          }
        }
      }else{
        printf("%s\n",out);
      }
    }
    else{
      printf("Param %s not supported \n", argv[1]);
      print_help();
    }

  }else{
    print_help();
  }
	return 0;
}
