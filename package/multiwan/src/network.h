/*
 * (C) Teltonika 2015
 */
#ifndef NETWORK_H
#define NETWORK_H
#include <unistd.h>
#include <libubus.h>
#include <json/json.h>
#include <libubox/blobmsg_json.h>
#include <libubox/blob.h>
#include <libubox/blobmsg.h>
#include <string.h>

/* Get gateway of the specified WAN */
/* don't forget to free() the result */
char *get_gateway(const char *wan);

/* Get all DNS server of specified WAN */
/* don't forget to free() the result */
char *get_dnsserver(const char *wan);

#endif
