/*
 * (C) Teltonika 2015
 */
#include <limits.h>
#include "routes.h"
#include "utilities.h"
#define MAX_ROUTE_LENGTH 128
#define DNS_FILE "/tmp/resolv.conf"

static int create_route(struct rtentry *route, char *ifname, const char *gw,
			const char *dst, const char *genmask, short metric)
{
	struct sockaddr_in *addr;

	if (!route) {
		printf_dbg("route is NULL\n");
		return EINVAL;
	}

	if (dst) {
		addr = (struct sockaddr_in *) &route->rt_dst;
		addr->sin_family = AF_INET;
		if (!inet_aton(dst, &addr->sin_addr)) {
			printf_dbg("dst (%s) is invalid\n", dst);
			return EINVAL;
		}
	}

	if (gw) {
		addr = (struct sockaddr_in *) &route->rt_gateway;
		addr->sin_family = AF_INET;
		if (!inet_aton(gw, &addr->sin_addr)) {
			printf_dbg("gw (%s) is invalid\n", gw);
			return EINVAL;
		}
	}

	if (genmask) {
		addr = (struct sockaddr_in *) &route->rt_genmask;
		addr->sin_family = AF_INET;
		if (!inet_aton(genmask, &addr->sin_addr)) {
			printf_dbg("genmask (%s) is invalid\n", genmask);
			return EINVAL;
		}
	}

	if (metric > 0)
		route->rt_metric = metric+1;
	else
		route->rt_metric = 0;

	if (ifname)
		route->rt_dev = ifname;

	route->rt_flags = RTF_UP | RTF_GATEWAY;
	return 0;
}

int modify_gw(enum act mode, char *ifname, const char *gw, const char *dst,
	      const char *genmask, short metric)
{
	int fd, ret, q;
	struct rtentry route = {0};

	if (!ifname || !gw || !dst || !genmask) {
		printf_dbg("ifname, gw, dst or genmask are NULL\n");
		return ENOENT;
	}

	fd = socket(PF_INET, SOCK_DGRAM, 0);
	if (fd == -1) {
		printf_dbg("Failed to create a socket: %s\n",
			   strerror(errno));
		return errno;
	}
	printf_dbg("Args: %d %s %s %s %s %hd\n", mode, ifname, gw, dst, genmask,
		   metric);
	
	ret = create_route(&route, ifname, gw, dst, genmask, metric);
	if (ret) {
		printf_dbg("create_route() failed: %s\n", strerror(ret));
		close(fd);
		return ret;
	}

	if (mode == DEL_GW)
		q = SIOCDELRT;
	else
		q = SIOCADDRT;

	ret = ioctl(fd, q, &route);
	if (ret == -1) {
		printf_dbg("ioctl has failed: %s\n", strerror(errno));
		close(fd);
		return errno;
	}

	close(fd);
	return 0;
}

int rule_exists(const char *ip)
{
	struct in_addr address;
	char *buf;
	int ret;

	if (!ip || !inet_aton(ip, &address)) {
		printf_dbg("ip is NULL or invalid\n");
		return EINVAL;
	}

	buf = malloc(9);
	if (!buf) {
		printf_dbg("Not enough memory\n");
		return ENOMEM;
	}

	snprintf(buf, 9, "%08"PRIX32, ntohl(address.s_addr));
	ret = grep("/proc/net/route", (const char*[]){buf, NULL}, MAX_ROUTE_LENGTH);
	printf_dbg("%s converted to hexadecimal %s\n", ip, buf);
	free(buf);
	return ret;
}

short get_metric(const char *ifname)
{
	char *line, *token, *saveptr = NULL;
	short m = SHRT_MAX-1;

	if (!ifname) {
		printf_dbg("ifname is NULL\n");
		return m;
	}

	line = grep_line("/proc/net/route", (const char*[]){ifname, "0003", NULL},
			 MAX_ROUTE_LENGTH);
	if (!line) {
		printf_dbg("%s not found\n", ifname);
		return m;
	}

	/* 7th token is the metric */
	token = strtok_r(line, "\t", &saveptr);
	token = strtok_r(NULL, "\t", &saveptr);
	token = strtok_r(NULL, "\t", &saveptr);
	token = strtok_r(NULL, "\t", &saveptr);
	token = strtok_r(NULL, "\t", &saveptr);
	token = strtok_r(NULL, "\t", &saveptr);
	token = strtok_r(NULL, "\t", &saveptr);

	/* Not a issue here because we know that token value is short */
	if (token) {
		m = atoi(token);
		printf_dbg("%s metric is %hd\n", ifname, m);
	} else {
		printf_dbg("failed to get %s metric\n", ifname);
	}

	free(line);
	return m;
}

int add_dns(const char *dns, size_t buf_size)
{
	int ret;

	if (!dns) {
		printf_dbg("dns is NULL\n");
		return 1;
	}

	ret = grep(DNS_FILE, (const char*[]){dns, NULL}, buf_size);
	if (ret == 0) {
		char *buf;

		buf = malloc(strlen(dns) + sizeof("nameserver ") + 2);
		if (!buf) {
			printf_dbg("Not enough memory in add_dns()\n");
			return ENOMEM;
		}
		buf[0] = '\0';

		strcat(buf, "nameserver ");
		strncat(buf, dns, 15);
		printf_dbg("Appending %s to %s\n", buf, DNS_FILE);
		if (append(DNS_FILE, buf)) {
			free(buf);
			return 1;
		}

		free(buf);
		return 0;
	}

	printf_dbg("grep found no results\n");

	return 1;
}

int remove_dns(const char *dns, size_t buf_size)
{
	FILE *fp;
	char *buf;

	if (!dns) {
		printf_dbg("dns is NULL, bailing\n");
		return EINVAL;
	}

	fp = fopen(DNS_FILE, "r");
	if (fp == NULL) {
		printf_dbg("Unable to open " DNS_FILE " for reading\n");
		return EIO;
	}

	buf = malloc(buf_size);
	if (!buf) {
		printf_dbg("Not enough memory in remove_dns()\n");
		fclose(fp);
		return ENOMEM;
	}

	while (fgets(buf, buf_size, fp)) {
		if (!strstr(buf, dns)) {
			printf_dbg("appending %s to /tmp/resolv.conf.tmp\n",
				   buf);
			append("/tmp/resolv.conf.tmp", buf);
		}
	}

	free(buf);
	if (remove(DNS_FILE))
		printf_dbg("Failed to remove " DNS_FILE ": %s\n",
			   strerror(errno));
	if (rename("/tmp/resolv.conf.tmp", DNS_FILE))
		printf_dbg("Failed to rename /tmp/resolv.conf.tmp to " DNS_FILE ": %s\n",
			   strerror(errno));
	fclose(fp);
	return 0;
}
