/*
 * (C) Teltonika 2015
 */
#include "utilities.h"
#define BUF_SIZE 2048

int debug = 0;

int createdir(const char *path)
{
	int ret;
	
	ret = mkdir(path, S_IRWXU | S_IRGRP | S_IROTH);
	
	if (ret) {
		printf_dbg("Unable to mkdir(): %s\n",
			   strerror(errno));
		return errno;
	}

	return ret;
}

int grep(const char *file, const char *text[], size_t buf_size)
{
	char *ret;

	ret = grep_line(file, text, buf_size);
	if (ret) {
		free(ret);
		return 1;
	}

	return 0;
}

char *grep_line(const char *file, const char *text[], size_t buf_size)
{
	FILE *fp;
	char *buf;

	if (!file || !text) {
		printf_dbg("either file or text is NULL, bailing\n");
		return NULL;
	}

	fp = fopen(file, "r");
	if (fp == NULL) {
		printf_dbg("Unable to open %s for reading\n", file);
		return NULL;
	}

	buf = malloc(buf_size);
	if (!buf) {
		printf_dbg("Not enough memory for grep_line()\n");
		fclose(fp);
		return NULL;
	}

	while (fgets(buf, buf_size, fp)) {
		int i, br = 0;
		for (i = 0; text[i]; i++)
			if (!strstr(buf, text[i])) {
				br = 1;
				break;
			}
		if (!br) {
			fclose(fp);
			return buf;
		}
	}

	free(buf);
	fclose(fp);

	return NULL;
}

int append(const char *file, const char *text)
{
	FILE *fp;

	if (!file || !text) {
		printf_dbg("file or text is NULL, bailing\n");
		return EINVAL;
	}

	fp = fopen(file, "a");
	if (fp == NULL) {
		printf_dbg("Unable to open %s for appending\n", file);
		return ENOENT;
	}

	fprintf(fp, "%s", text);
	fclose(fp);
	return 0;
}

int ping(const char *ifname, unsigned int timeout, const char *dst)
{
	struct in_addr dsti;
	struct icmphdr icmp_hdr, rcv_hdr;
	struct sockaddr_in addr, recv;
	int sock, rc, ret;
	char data[BUF_SIZE] = {0}, buf[BUF_SIZE] = {0};
	struct timeval tv;
	socklen_t slen = sizeof(recv);
	char *payload = "teltonika";
	uintmax_t start;

	if (!dst)
		return EINVAL;

	sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_ICMP);
	if (sock == -1) {
		printf_dbg("socket() failed: %s\n", strerror(errno));
		return errno;
	}

	if (inet_aton(dst, &dsti) == 0) {
		printf_dbg("inet_aton() failed\n");
		close(sock);
		return 1;
	}

	printf_dbg("Pinging %s on %s with %u timeout\n", dst, ifname, timeout);

	tv.tv_sec = timeout;
	tv.tv_usec = 0;

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr = dsti;

	memset(&icmp_hdr, 0, sizeof(icmp_hdr));
	icmp_hdr.type = ICMP_ECHO;

	/* Set timeout */
	ret = setsockopt(sock, SOL_SOCKET, SO_RCVTIMEO, &tv, sizeof(tv));
	if (ret)
		printf_dbg("SOL_RCVTIMEO setsockopt() failed: %s\n",
			   strerror(errno));

	/* Bind to interface */
	if (ifname) {
		ret = setsockopt(sock, SOL_SOCKET, SO_BINDTODEVICE, ifname,
				 strlen(ifname)+1);
		if (ret) {
			printf_dbg("SO_BINDTODEVICE setsockopt() failed: %s\n",
				   strerror(errno));
			return errno;
		}
	}

	memcpy(data, &icmp_hdr, sizeof(icmp_hdr));
	memcpy(data + sizeof(icmp_hdr), payload, strlen(payload));
	rc = sendto(sock, data, sizeof(icmp_hdr) + strlen(payload),
		    0, (struct sockaddr*)&addr, sizeof(addr));

	if (rc == -1) {
		printf_dbg("sendto() failed: %s\n", strerror(errno));
		close(sock);
		return errno;
	}

	start = (uintmax_t) time(NULL);

	for (;;) {
retry:
		if (((uintmax_t) time(NULL)) - start > (uintmax_t) timeout) {
			close(sock);
			return -1;
		}

		rc = recvfrom(sock, buf, sizeof(buf), 0,
			      (struct sockaddr*)&recv, &slen);
		if (rc == -1) {
			if (errno == EINTR)
				goto retry;

			printf_dbg("recvfrom() error: %s\n", strerror(errno));
			close(sock);
			return errno;
		}

		/* There is no point in checking idenfication because
		 * the kernel already does that for us
		 * Comparing strings for extra carefulness
		 */
		memcpy(&rcv_hdr, buf, sizeof(rcv_hdr));

		if (rcv_hdr.type == ICMP_ECHOREPLY &&
		    strncmp(inet_ntoa(recv.sin_addr), dst, strlen(dst)) == 0 &&
		    strncmp(buf+sizeof(rcv_hdr), payload, strlen(payload)) == 0) {
			close(sock);
			return 0;
		}
	}

	close(sock);
	return -1;
}

int ping_n(int n, const char *ifname, unsigned int timeout, const char *dst)
{
	int i;

	if (!dst) {
		printf_dbg("NULL passed to ping_n()\n");
		return EINVAL;
	}

	for (i = 0; i < n; i++)
		if (!ping(ifname, timeout, dst)) {
			printf_dbg("%d attempt on %s is OK\n", i+1, ifname);
			return 0;
		}

	printf_dbg("Failed to ping in %d times\n", n);
	return -1;
}

int exists_not_directory(const char *file)
{
	struct stat buffer;
	int ret;

	ret = stat(file, &buffer);
	if (ret == -1) {
		printf_dbg("stat() failed: %s\n", strerror(errno));
		return 0;
	}

	return S_ISREG(buffer.st_mode) && !S_ISDIR(buffer.st_mode);
}

void reload_strongswan(void)
{
	pid_t pid;

	pid = vfork();
	switch (pid) {
		case 0:
			execl("/bin/sh", "sh", "-c", "/usr/sbin/ipsec reload;", (char*) 0);
			_exit(EXIT_FAILURE);
		case -1:
			printf_dbg("vfork() failed: %d\n", errno);
			break;
	}
}

char *get_config(struct uci_context *uci, const char *pkg,
		 const char *section, const char *option)
{
	struct uci_package *package = NULL;
	struct uci_section *sct = NULL;
	char *ret = NULL;
	const char *txt = NULL;

	ret = malloc(MAXSIZE);
	if (!ret) {
		printf_dbg("Not enough memory in get_config()\n");
		return NULL;
	}
	memset(ret, 0, MAXSIZE);

	if (uci_load(uci, pkg, &package) != UCI_OK) {
		char *err = NULL;

		uci_get_errorstr(uci, &err, NULL);
		printf_dbg("uci_load() failed: %s\n", err);

		free(ret);
		free(err);
		return NULL;
	}

	sct = uci_lookup_section(uci, package, section);
	if (sct)
		txt = uci_lookup_option_string(uci, sct, option);
	if (txt) {
		printf_dbg("got txt: %s\n", txt);
		strncpy(ret, txt, MAXSIZE);
	}
	uci_unload(uci, package);

	return ret;
}

void change_ptrs(char **ptr, char *new)
{
	if (ptr && *ptr) {
		free(*ptr);
		*ptr = NULL;
	}

	if (new && ptr)
		*ptr = new;
}

uint32_t ubus_exists(const char *path)
{
	struct ubus_context *ctx;
	uint32_t id = 0;
	ctx = ubus_connect(NULL);
	if (!ctx)
		return 0;
	ubus_lookup_id(ctx, path, &id);
	ubus_free(ctx);
	return id;
}

/* check strongswan enabled*/
int check_strongswan(struct uci_context *uci)
{
	struct uci_ptr ptr;
	struct uci_element* element_section = NULL;

	memset(&ptr, 0, sizeof(ptr));
	uci = uci_alloc_context();
	if (uci_lookup_ptr(uci, &ptr, "strongswan", true) != UCI_OK) {
		uci_free_context(uci);
		return -1;
	}
	uci_foreach_element(&ptr.p->sections, element_section)
	{
		//printf("check_strongswan ENABLED %s\n", element_section->name);
		struct uci_element* element_option = NULL;
		struct uci_section* section = NULL;
		section = uci_to_section(element_section);
		uci_foreach_element(&section->options, element_option)
		{
			struct uci_option* option = NULL;
			option = uci_to_option(element_option);
			if (strcmp(option->e.name, "enabled") == 0) {
				if (strcmp(option->v.string, "1") == 0) {
					uci_free_context(uci);
					return 1;
				}
			}
		}
	}
	uci_free_context(uci);
	return 0;
}
