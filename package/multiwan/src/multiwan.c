/*
 * (C) Copyright 2015 Teltonika
 */
#include "loggers.h"
#include "utilities.h"
#include "routes.h"
#include "network.h"
#include "multiwan.h"
#include <assert.h>
#include <uci.h>
#include <signal.h>
#include <pthread.h>

#define STATUS_FILE "/tmp/.mwan/cache"
#define DNSBUFSIZE 200
#define MAX_WANS 10
#define MAXBUFSIZE 500

static char *demand_gw = "10.112.112.112";
static char *dns = "8.8.8.8";
static struct w *wans;
static char *demand;
static short strogswan_enabled;
static int WANS;
static pthread_mutex_t mut;
static pthread_cond_t cond;
static int last_wan = -1;

static void get_wan_icmp_hosts(struct w *);
static int check_wan(struct w *);
static void add_3g_dns(void);
static void main_function(void);
static void update_mwan(void);
static int switch_con(void);
static void handler(int);
static void reload_gateway(struct w *);
static const char *get_wan(const char *);
/* Construct a dynamically allocated string with wan name
 * @i - index
 * Returns "wan" if i <= 1 else it returns "wan" + i
 */
static char *wan_name(int i)
{
	size_t size = 4+WANS;
	char *name = malloc(size);

	assert(name != NULL);

	if (i > 1)
		snprintf(name, size, "wan%d", i);
	else
		snprintf(name, size, "wan");
	return name;
}

/* Check if @str is a mobile interface or not
 * @str - string with IF
 * Returns whether @str is a mobile IF or not
 */
static bool is_mobile(const char *str)
{
	return str && (strcmp(str, "3g-ppp") == 0 ||
		       strcmp(str, "wwan0") == 0);
}

/* Get WAN name (network.interface.{ppp,ppp_dhcp}) from the
 * IF name for mobile connections
 * @IF - string with IF name
 * Returns WAN name for the given IF name
 */
static const char *get_wan(const char *IF)
{
	if (!IF)
		return NULL;

	if (strcmp(IF, "wwan0") == 0)
		return "ppp_dhcp";
	else
		return "ppp";
}

/* Initialize wans array
 * @uci - uci context
 */
static void get_wan_info(struct uci_context *uci)
{
	int i;

	assert(uci != NULL);

	for (i = 0; i < WANS; i++) {
		struct w *wan = &wans[i];
		char *name = wan_name(i+1), *ret;

		change_ptrs(&wan->name, name);
		ret = get_config(uci, "network", name, "enabled");
		if (ret && strcmp(ret, "0") == 0) {
			printf_dbg("%s is disabled (%s)\n", name, ret);
			free(ret);
			continue;
		}
		free(ret);

		wan->enabled = true;
		change_ptrs(&wan->dns, get_config(uci, "multiwan", name, "dns"));
		change_ptrs(&wan->health_fail_retries, get_config(uci,
			    "multiwan", name, "health_fail_retries"));
		change_ptrs(&wan->health_recovery_retries, get_config(uci,
			    "multiwan", name, "health_recovery_retries"));
		change_ptrs(&wan->health_interval, get_config(uci, "multiwan",
			    name, "health_interval"));
		change_ptrs(&wan->icmp_hosts, get_config(uci, "multiwan", name,
			    "icmp_hosts"));
		change_ptrs(&wan->timeout, get_config(uci, "multiwan", name,
			    "timeout"));
		change_ptrs(&wan->icmp_count, get_config(uci, "multiwan", name,
			    "icmp_count"));
		change_ptrs(&wan->ifname, get_config(uci, "network", name,
			    "ifname"));

		wan->metric = get_metric(wan->ifname);
		wan->ok = true;
		ret = get_config(uci, "multiwan", name, "priority");
		if (!ret) {
			printf_dbg("No priority found for %s\n", wan->name);
			wan->priority = WANS-i;
		} else {
			wan->priority = atoi(ret);
			free(ret);
		}

		reload_gateway(wan);
		printf_dbg("WAN %s info: %s %s %s %s %s %s %s %s %s\n", wan->name, wan->dns,
			   wan->health_fail_retries, wan->health_recovery_retries,
			   wan->health_interval, wan->icmp_hosts, wan->timeout, wan->icmp_count,
			   wan->ifname, wan->gateway);
	}
}

/* Read configuration of and ping @wan to test if it's alive
 * @wan - pointer to a struct that describes a WAN
 * Returns: 0 = WAN is good
 * 	    1 = tried to ping but failed
 * 	    everything else = some other error occured (use strerror)
 */
static int check_wan(struct w *wan)
{
	int count = 0, timeout = 0;
	char *ifname = NULL, *hosts = NULL, *h = NULL, *copy = NULL, *saveptr = NULL;

	assert(wan != NULL);

	if (!wan->enabled) {
		printf_dbg("%s is disabled\n", wan->name);
		return ENOENT;
	}

	if (demand && atoi(demand) > 0 && wan->ifname &&
	    strcmp(wan->ifname, "3g-ppp") == 0) {
		printf_dbg("check_wan() == 0 because demand is on and ifname == 3g-ppp\n");
		return 0;
	}

	get_wan_icmp_hosts(wan);

	if (!wan->icmp_count || !wan->timeout || !wan->icmp_hosts_real ||
	    !wan->ifname) {
		printf_dbg("icmp_count, timeout, icmp_hosts_real or ifname is NULL\n");
		return EINVAL;
	}

	count = atoi(wan->icmp_count);
	timeout = atoi(wan->timeout);
	ifname = wan->ifname;
	hosts = wan->icmp_hosts_real;

	if (count == 0) {
		printf_dbg("count is 0, setting it to 1\n");
		count = 1;
	}

	copy = malloc(strlen(hosts)+1);
	if (!copy) {
		printf_dbg("Not enough memory in check_wan()\n");
		return ENOMEM;
	}
	strcpy(copy, hosts);
	printf_dbg("All hosts: %s\n", copy);

	h = strtok_r(copy, " ", &saveptr);
	while (h) {
		if (!ping_n(count, ifname, timeout, h)) {
			free(copy);
			return 0;
		}

		h = strtok_r(NULL, " ", &saveptr);
	}

	printf_dbg("pinging has failed\n");
	free(copy);
	return 1;
}

/* Actually parse wan->icmp_hosts_real from wan->icmp_hosts
 * @wan - a pointer to a struct that describes a WAN
 */
static void get_wan_icmp_hosts(struct w *wan)
{
	assert(wan != NULL);

	if (!wan->icmp_hosts) {
		printf_dbg("wan->icmp_hosts is NULL\n");
		return;
	}

	if (!wan->enabled) {
		printf_dbg("wan->enabled is false\n");
		return;
	}

	if (strcmp(wan->icmp_hosts, "gateway") == 0) {
		change_ptrs(&wan->icmp_hosts_real, get_gateway(wan->name));
	} else if (strcmp(wan->icmp_hosts, "dns") == 0) {
		change_ptrs(&wan->icmp_hosts_real, get_dnsserver(wan->name));
	} else {
		free(wan->icmp_hosts_real);
		wan->icmp_hosts_real = malloc(strlen(wan->icmp_hosts)+1);
		if (!wan->icmp_hosts_real) {
			printf_dbg("Not enough memory in get_wan_icmp_hosts()\n");
			return;
		}
		memset(wan->icmp_hosts_real, 0, strlen(wan->icmp_hosts)+1);
		strncpy(wan->icmp_hosts_real, wan->icmp_hosts,
			strlen(wan->icmp_hosts));
		printf_dbg("wan->icmp_hosts_real: %s\n", wan->icmp_hosts_real);
	}
}

/* Add missing 3g DNS */
static void add_3g_dns(void)
{
	char *dns_file = "/tmp/resolv.conf.ppp", *buf = NULL,
	     *gw = NULL, *f = NULL, *saveptr = NULL, *IF = NULL;
	const char *from = NULL;
	FILE *fp = NULL;
	int rd = 0, i;
	bool changed = false;

	for (i = 0; i < WANS; i++)
		if (is_mobile(wans[i].ifname)) {
			from = get_wan(wans[i].ifname);
			IF = wans[i].ifname;
		}

	if (!from) {
		printf_dbg("No mobile interface found!\n");
		return;
	}

	if (!exists_not_directory(dns_file)) {
		dns_file = "/tmp/resolv.conf.auto";
		changed = true;
	}

	printf_dbg("dns_file is %s\n", dns_file);

	fp = fopen(dns_file, "r");
	if (fp == NULL) {
		printf_dbg("Failed to open %s\n", dns_file);
		return;
	}

	buf = malloc(DNSBUFSIZE);
	if (!buf) {
		printf_dbg("Not enough memory\n");
		fclose(fp);
		return;
	}

	if (!changed)
		rd = 1;
	else
		printf_dbg("Reading from %s to wan\n", from);

	while (fgets(buf, DNSBUFSIZE, fp)) {
		if (changed && strstr(buf, from))
			rd = 1;
		else if (changed && strstr(buf, "wan"))
			rd = 0;

		if (!rd)
			continue;

		if (buf[0] == '#' || !strstr(buf, "nameserver"))
			continue;

		printf_dbg("Parsing line %s\n", buf);

		f = strtok_r(buf, " \n", &saveptr);
		f = strtok_r(NULL, " \n", &saveptr);
		if (f) {
			if (rule_exists(f))
				continue;

			if (atoi(demand) > 0)
				gw = demand_gw;
			else
				gw = get_gateway(from);

			if (gw) {
				printf_dbg("Adding gw %s, host %s\n", gw, f);
				modify_gw(ADD_GW, IF, gw, f, "255.255.255.255", 0);
			} else {
				printf_dbg("No GW found for host %s\n", f);
			}

			if (gw != demand_gw)
				free(gw);
		}
		saveptr = NULL;
	}

	free(buf);
	fclose(fp);
}

/* Signal handler that notifies main thread that something happened or IOW
 * makes the main thread behave as if something changed
 * @signum - signal number
 */
static void handler(int signum)
{
	(void) signum;
	pthread_cond_signal(&cond);
}

/* Main thread function that gets struct w pointer as a parameter
 * and pings it continously. When something happens it informs the main thread
 * via a pthread_cond
 *
 * @arg - a pointer to thread arguments or IOW a pointer to struct w
 */
void *pingworker(void *arg)
{
	struct w *wan = arg;
	while (1) {
		int ret;
		ret = check_wan(wan);
		if (ret == 0) {
			if (wan->retries < 0)
				wan->retries = 0;
			wan->retries++;
		} else {
			if (wan->retries > 0)
				wan->retries = 0;
			wan->retries--;
		}

		if (wan->retries == -atoi(wan->health_fail_retries)) {
			if (wan->ok == false)
				continue;
			wan->ok = false;
			pthread_cond_signal(&cond);
		} else if (wan->retries == atoi(wans->health_recovery_retries)) {
			if (wan->ok == true)
				continue;
			wan->ok = true;
			pthread_cond_signal(&cond);
		}

		printf_dbg("(%s) Sleeping for %d\n", wan->name, atoi(wan->health_interval));
		sleep(atoi(wan->health_interval));
		printf_dbg("(%s) Woke up\n", wan->name);
	}
	return NULL;
}

static void reload_gateway(struct w *wan)
{
	if (!wan)
		return;

	if (wan->gateway != demand_gw) {
		free(wan->gateway);
		wan->gateway = NULL;
	}
	if (is_mobile(wan->ifname))
		if (demand && atoi(demand) > 0)
			wan->gateway = demand_gw;
		else
			wan->gateway = get_gateway(get_wan(wan->ifname));
	else
		wan->gateway = get_gateway(wan->name);
}

/* The main loop - set up the threads, wait for signal */
static void main_function(void)
{
	pthread_attr_t attr;
	pthread_t tr[WANS];
	int i;

	pthread_attr_init(&attr);
	pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
	for (i = 0; i < WANS; i++) {
		if (wans[i].enabled)
			pthread_create(&tr[i], &attr, pingworker, &wans[i]);
	}
	pthread_attr_destroy(&attr);

	while (1) {
		pthread_mutex_lock(&mut);
		pthread_cond_wait(&cond, &mut);
		printf_dbg("Main thread continuing its job\n");
		add_3g_dns();
		switch_con();
		update_mwan();
		pthread_mutex_unlock(&mut);
	}

}

/* Update status file with information from wans array */
static void update_mwan(void)
{
	int i;
	char *line;

	line = malloc(MAXSIZE);
	if (!line) {
		printf_dbg("Not enough memory in update_mwan()\n");
		return;
	}
	memset(line, 0, MAXSIZE);

	remove(STATUS_FILE);
	append(STATUS_FILE, "# Automatically Generated by Multi-Lite Script. Do not modify or remove. #\n");
	strcat(line, "wan_if_map=\"");
	for (i = 0; i < WANS; i++) {
		if (!wans[i].enabled)
			continue;
		strcat(line, wans[i].name);
		strcat(line, "[");
		strcat(line, wans[i].ifname);
		strcat(line, "]");
	}
	strcat(line, "\"\n");
	printf_dbg("%s", line);
	append(STATUS_FILE, line);

	memset(line, 0, MAXSIZE);
	strcat(line, "wan_fail_map=\"");
	for (i = 0; i < WANS; i++) {
		if (!wans[i].enabled || wans[i].ok)
			continue;
		strcat(line, wans[i].name);
		strcat(line, "[x]");
	}
	strcat(line, "\"\n");
	printf_dbg("%s", line);
	append(STATUS_FILE, line);

	memset(line, 0, MAXSIZE);
	strcat(line, "wan_recovery_map=\"\"\n");
	append(STATUS_FILE, line);
	free(line);
}

/* Modify routing table to "switch" connection to the most prioritized connection
 * Returns:
 * 0 = success
 * Anything else = error (use strerror)
 */
static int switch_con(void)
{
	int i = 0, j;
	short metric;
	char msg[MAXBUFSIZE];

	while (i < WANS && wans[i].ok != true)
		i++;

	if (i == WANS) {
		printf_dbg("All WANs are down, bailing out\n");
		return ENOENT;
	}

	if (last_wan != -1 && last_wan != i) {
		sprintf(msg, "Switched to %s WAN (%s)", i == 0 ? "main" : "backup",
				wans[i].ifname ? wans[i].ifname : "no interface");
		eventslog(msg);
		/* Gali pasikeisti IP čia tai perkraunam strongswan */
		if (strogswan_enabled == 1) {
			print_syslog("Reloading strongswan");
			reload_strongswan();
		}
	}
	last_wan = i;

	printf_dbg("Switching to %d\n", i);

	/* Workaround del DNS idejimo */
	if (is_mobile(wans[i].ifname)) {
                add_dns(dns, MAXSIZE);
	} else {
		remove_dns(dns, MAXSIZE);
	}


	for (j = 0; j < WANS; j++) {
		if (j != i && wans[j].enabled) {
			modify_gw(DEL_GW, wans[j].ifname, wans[j].gateway, "0.0.0.0", "0.0.0.0", wans[j].metric);
			/* Double GW fix */
			if ((metric = get_metric(wans[j].ifname)) != SHRT_MAX-1)
				modify_gw(DEL_GW, wans[j].ifname, wans[j].gateway, "0.0.0.0", "0.0.0.0", metric);
			wans[j].metric = 10;
			reload_gateway(&wans[j]);
			modify_gw(ADD_GW, wans[j].ifname, wans[j].gateway, "0.0.0.0", "0.0.0.0", wans[j].metric);
		}
	}

	modify_gw(DEL_GW, wans[i].ifname, wans[i].gateway, "0.0.0.0", "0.0.0.0", wans[i].metric);
	/* Double GW fix */
	if ((metric = get_metric(wans[i].ifname)) != SHRT_MAX-1)
		modify_gw(DEL_GW, wans[i].ifname, wans[i].gateway, "0.0.0.0", "0.0.0.0", metric);
	wans[i].metric = 0;
	reload_gateway(&wans[i]);
	modify_gw(ADD_GW, wans[i].ifname, wans[i].gateway, "0.0.0.0", "0.0.0.0", wans[i].metric);
	return 0;
}

/* Read the configuration and see if debug is enabled */
static void check_debug(struct uci_context *uci)
{
	char *cfg;
	assert(uci != NULL);

	cfg = get_config(uci, "multiwan", "config", "debug");
	if (strcmp(cfg, "1") == 0 || strcmp(cfg, "on") == 0 ||
	    strcmp(cfg, "true") == 0 || strcmp(cfg, "enabled") == 0)
		debug = 1;
	else
		debug = 0;

	free(cfg);
	cfg = NULL;
}

/* Comparator that is passed to qsort()
 * @a - first element
 * @b - second element
 * Returns -1, 0 or 1 depending on the values of wan->priority
 */
static int cmp(const void *a, const void *b)
{
	const struct w *wan_a = a, *wan_b = b;

	if (wan_a->priority > wan_b->priority)
		return -1;
	else if (wan_a->priority == wan_b->priority)
		return 0;
	else
		return 1;
}

int main(int argc, char *argv[])
{
	struct uci_context *uci;
	struct uci_package *pkg = NULL;
	struct uci_section *sct = NULL;
	char *cfg;
	int i;
	struct sigaction sa;

	(void) argc;
	(void) argv;

	uci = uci_alloc_context();
	if (!uci) {
		fprintf(stderr, "Failed to allocate uci context\n");
		return EXIT_FAILURE;
	}

	if (uci_add_delta_path(uci, "/var/state") == UCI_ERR_DUPLICATE) {
		fprintf(stderr, "uci_add_delta_path() failed: duplicate path\n");
		uci_free_context(uci);
		return EXIT_FAILURE;
	}

	uci_load(uci, "network", &pkg);
	if (!pkg){
		fprintf(stderr, "uci_load(uci, 'network', &pkg) failed\n");
		return EXIT_FAILURE;
	}
	for (i = 0; i < MAX_WANS; i++) {
		char p[MAXSIZE];
		uint32_t ret;
		if (i == 0)
			sprintf(p, "wan");
		else
			sprintf(p, "wan%d", i+1);
		sct = uci_lookup_section(uci, pkg, p);
		if (!sct){
			uci_unload(uci, pkg);
			break;
		}
		WANS++;
	}

	wans = calloc(WANS, sizeof(struct w));
	if (!wans) {
		fprintf(stderr, "Failed to allocate memory for wans\n");
		return EXIT_FAILURE;
	}

	cfg = get_config(uci, "multiwan", "config", "enabled");
	if (cfg && atoi(cfg) <= 0) {
		printf_dbg("multiwan is disabled, bye.\n");
		free(cfg);
		uci_free_context(uci);
		return EXIT_SUCCESS;
	}
	free(cfg);

	check_debug(uci);
	init_loggers();
	get_wan_info(uci);

	/* In case /tmp/.mwan doesn't exist */
	createdir("/tmp");
	createdir("/tmp/.mwan");

	change_ptrs(&demand, get_config(uci, "network", "ppp", "demand"));

	/* Sort wans array by priority decreasing order */
	qsort(wans, WANS, sizeof(struct w), cmp);

	eventslog("multiwan starting...");

	if (check_strongswan(uci) == 1) {
		printf_dbg("Strongswan is enabled\n");
		strogswan_enabled = 1;
	}
	free(cfg);

	sa.sa_handler = handler;
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = SA_RESTART;

	if (sigaction(SIGUSR1, &sa, NULL) == -1) {
		printf_dbg("Failed to register handler for SIGUSR1\n");
		return EXIT_FAILURE;
	}

	/* Initiation */
	update_mwan();
	switch_con();
	add_3g_dns();
	pthread_mutex_init(&mut, NULL);
	pthread_cond_init(&cond, NULL);
	main_function();

	/* Clean up everything */
	close_loggers();
	uci_free_context(uci);
	for (i = 0; i < WANS; i++) {
		free(wans[i].dns);
		free(wans[i].health_fail_retries);
		free(wans[i].health_recovery_retries);
		free(wans[i].health_interval);
		free(wans[i].icmp_hosts);
		free(wans[i].icmp_hosts_real);
		free(wans[i].timeout);
		free(wans[i].icmp_count);
		free(wans[i].ifname);
		if (wans[i].gateway != demand_gw)
			free(wans[i].gateway);
		free(wans[i].name);
	}
	free(wans);
	pthread_mutex_destroy(&mut);
	pthread_cond_destroy(&cond);
	return EXIT_SUCCESS;
}
