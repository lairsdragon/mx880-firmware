/*
 * (C) Teltonika 2015
 */

#ifndef LOGGERS_H
#define LOGGERS_H

#include <errno.h>
#include <syslog.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

/* Initialize the loggers */
void init_loggers(void);

/* Print to syslog */
#define print_syslog(...) syslog(LOG_USER, __VA_ARGS__)

/* Uninitialize the loggers */
void close_loggers(void);

/* Insert message into Backup database */
int eventslog(const char *msg);

#endif /* LOGGERS_H */
