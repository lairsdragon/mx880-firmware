/*
 * (C) Teltonika 2015
 */

#include "loggers.h"
#include "utilities.h"

static const char *ident = "multiwan";
static const char *action = "insert";
static const char *db_name = "Backup";

/* Initialize syslog */
static void init_syslog(void)
{
	if (debug)
		openlog(ident, LOG_PERROR, LOG_USER);
	else
		openlog(ident, 0, LOG_USER);
}

void init_loggers(void)
{
	init_syslog();
}

void close_loggers(void)
{
	closelog();
}

int eventslog(const char *msg)
{
	pid_t pid;

	if (!msg) {
		printf_dbg("msg is NULL\n");
		return EINVAL;
	}

	pid = vfork();
	switch (pid) {
		case 0:
			/* Child */
			execl("/usr/bin/eventslog", "eventslog", action,
			      db_name, msg, (char*) 0);
			_exit(EXIT_FAILURE);
		case -1:
			/* Father, error */
			printf_dbg("vfork() failed: %s\n", strerror(errno));
			return errno;
	}
	return 0;
}
