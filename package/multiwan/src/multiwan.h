#ifndef MULTIWAN_H
#define MULTIWAN_H
#include <stdbool.h>

/* WAN information */
struct w {
	char *dns;
	char *health_fail_retries;
	char *health_recovery_retries;
	char *health_interval;
	char *icmp_hosts;
	char *icmp_hosts_real;
	char *timeout;
	char *icmp_count;
	char *ifname;
	char *gateway;
	char *name;
	short metric;
	bool ok;
	bool enabled;
	int priority;
	int retries;
};

#endif
