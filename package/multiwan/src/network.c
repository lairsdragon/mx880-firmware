#include "network.h"
#include "utilities.h"

#define TIMEOUT 10
#define MAX_LEN 100
#define PREFIX "network.interface."
#define PREFIX_SIZE sizeof(PREFIX)
#define MAXBUFLEN 2000

static int make_request(const char *, char *);

static void cb(struct ubus_request *req, int type, struct blob_attr *msg)
{
	char *ret, *str;

	(void) type;
	(void) req;

	if (!msg) {
		printf_dbg("msg is NULL, bailing\n");
		return;
	}

	str = req->priv;
	ret = blobmsg_format_json(msg, true);
	strncpy(str, ret, MAXBUFLEN);
	free(ret);
	str[MAXBUFLEN-1] = '\0';

	printf_dbg("Got JSON: %s\n", str);
}

static int make_request(const char *wan, char *ret_str)
{
	struct ubus_context *ctx;
	int ret;
	struct blob_buf b = {0};
	uint32_t id;
	char path[MAX_LEN+PREFIX_SIZE] = PREFIX;

	if (!wan || strlen(wan) > MAX_LEN) {
		if (wan)
			printf_dbg("wan: %s, len: %zu\n", wan, strlen(wan));
		printf_dbg("wan is either NULL or too big\n");
		return -1;
	}

	strncat(path, wan, MAX_LEN);

	ctx = ubus_connect(NULL);
	if (!ctx) {
		printf_dbg("ubus: failed to connect to %s\n", path);
		return -1;
	}

	ret = ubus_lookup_id(ctx, path, &id);
	if (ret) {
		printf_dbg("ubus: lookup id failed (%s): %s\n", path,
			   ubus_strerror(ret));
		ubus_free(ctx);
		return ret;
	}

	blob_buf_init(&b, 0);
	ret = ubus_invoke(ctx, id, "status", b.head, cb, ret_str, TIMEOUT*1000);
	if (ret)
		printf_dbg("ubus: invoke failed (%s): %s\n", path,
			   ubus_strerror(ret));

	/* Cleanup */
	if (b.buf)
		free(b.buf);

	ubus_free(ctx);
	return ret;
}

char *get_gateway(const char *wan)
{
	int ret, i;
	struct json_object *json, *route;
	enum json_tokener_error err;
	char str[MAXBUFLEN] = {0};

	if (!wan) {
		printf_dbg("wan is NULL\n");
		goto exit;
	}

	ret = make_request(wan, str);
	if (ret) {
		printf_dbg("Failed to make a request in get_gateway()\n");
		goto exit;
	}

	json = json_tokener_parse_verbose(str, &err);
	if (!json) {
		printf_dbg("Failed to parse JSON in get_gateway(): %s\n",
			   json_tokener_error_desc(err));
		goto exit;
	}

	route = json_object_object_get(json, "route");
	if (!route) {
		printf_dbg("route object not found in get_gateway()\n");
		goto put_json;
	}

	if (!json_object_is_type(route, json_type_array)) {
		printf_dbg("route has wrong type\n");
		printf_dbg("route type: %s\n",
			   json_type_to_name(json_object_get_type(route)));
		goto put_json;
	}

	for (i = 0; i < json_object_array_length(route); i++) {
		struct json_object *trgt = NULL, *nexthop = NULL,
				   *rt = json_object_array_get_idx(route, i);
		json_bool jr;

		jr = json_object_object_get_ex(rt, "table", NULL);
		if (jr) {
			printf_dbg("table object found (%d), continuing...\n",
				   i);
			continue;
		}

		json_object_object_get_ex(rt, "target", &trgt);
		json_object_object_get_ex(rt, "nexthop", &nexthop);
		if (!trgt || !nexthop) {
			printf_dbg("Either trgt or nexthop is NULL (%d), continuing...\n",
				   i);
			continue;
		}

		if (strcmp(json_object_get_string(trgt), "0.0.0.0") == 0) {
			char *buf;

			buf = malloc(strlen(json_object_get_string(nexthop))+1);
			if (!buf) {
				printf_dbg("Not enough memory in get_gateway()\n");
				goto put_json;
			}

			strncpy(buf, json_object_get_string(nexthop),
				strlen(json_object_get_string(nexthop))+1);
			json_object_put(json);
			return buf;
		}
	}
	printf_dbg("Iterated and reached the end...\n");

put_json:
	json_object_put(json);
exit:
	return NULL;
}

char *get_dnsserver(const char *wan)
{
	int ret, i;
	struct json_object *json, *dns;
	char *buf;
	size_t buf_left = MAXBUFLEN;
	enum json_tokener_error err;
	char str[MAXBUFLEN] = {0};
	json_bool jb;

	printf_dbg("Initial buffer size: %zu\n", buf_left);

	if (!wan) {
		printf_dbg("wan is NULL\n");
		goto exit;
	}

	ret = make_request(wan, str);
	if (ret) {
		printf_dbg("Failed to make a request in get_dnsserver()\n");
		goto exit;
	}

	json = json_tokener_parse_verbose(str, &err);
	if (!json) {
		printf_dbg("Failed to parse JSON in get_dnsserver(): %s\n",
			   json_tokener_error_desc(err));
		goto put_json;
	}

	buf = malloc(MAXBUFLEN);
	if (!buf) {
		printf_dbg("Not enough memory in get_dnsserver()\n");
		goto put_json;
	}
	memset(buf, 0, MAXBUFLEN);

	jb = json_object_object_get_ex(json, "dns-server", &dns);
	if (!jb) {
		printf_dbg("Failed to get dns-server object in get_dnsserver()\n");
		goto free_buf;
	}

	if (!json_object_is_type(dns, json_type_array)) {
		printf_dbg("dns has wrong type in get_dnsserver()\n");
		printf_dbg("dns type: %s\n",
			   json_type_to_name(json_object_get_type(dns)));
		json_object_put(dns);
		goto free_buf;
	}

	for (i = 0; i < json_object_array_length(dns); i++) {
		struct json_object *el = json_object_array_get_idx(dns, i);

		if (!el) {
			printf_dbg("el not found (%d), continuing...\n", i);
			continue;
		}

		if (buf_left >= strlen(json_object_get_string(el))) {
			strncat(buf, json_object_get_string(el), buf_left);
			buf_left -= strlen(json_object_get_string(el));
			printf_dbg("buf now: %s, left: %zu\n", buf, buf_left);
			if (buf_left >= 1) {
				strncat(buf, " ", buf_left);
				buf_left -= 1;
				printf_dbg("buf now: %s, left: %zu\n", buf, buf_left);
			}
		} else {
			printf_dbg("Buffer is full, breaking the loop\n");
			json_object_put(el);
			break;
		}

		json_object_put(el);
	}

	json_object_put(dns);
	json_object_put(json);

	printf_dbg("Returning buffer: %s\n", buf);

	return buf;

free_buf:
	free(buf);
put_json:
	json_object_put(json);
exit:
	return NULL;
}
