/*
 * (C) Teltonika 2015
 */

#ifndef ROUTES_H
#define ROUTES_H
#include <errno.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <net/route.h>
#include <arpa/inet.h>
#include <string.h>
#include <unistd.h>
#include <inttypes.h>
#include <netinet/in.h>
#include "utilities.h"

/* Definition of two modes of modify_gw() */
enum act {
	ADD_GW,
	DEL_GW
};

/* Modify gateway:
 * mode: ADD_GW or DEL_GW
 *
 * ifname: interface name
 *
 * gw: gateway in ipv4 dot format
 *
 * dst: destination in ipv4 dot format
 * use "0.0.0.0" for default destination
 *
 * genmask: network mask (genmask) in ipv4 dot format
 *
 * metric: numeric value for metric
 * `route` uses 0 metric by default
 *
 * It's possible to delete a GW with only specifying dst
 * that emulates for example `route del default` behaviour
 */
int modify_gw(enum act mode, char *ifname, const char *gw, const char *dst,
	      const char *genmask, short metric);

/* Get ifname UG entry metric */
short get_metric(const char *ifname);

/* Does an specific IP have a rule? */
int rule_exists(const char *ip);

/* Add DNS to /tmp/resolv.conf */
int add_dns(const char *dns, size_t buf_size);

/* Remove DNS from /tmp/resolv.conf */
int remove_dns(const char *dns, size_t buf_size);

#endif
