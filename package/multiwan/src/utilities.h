/*
 * (C) Teltonika 2015
 */
#ifndef UTILITIES_H
#define UTILITIES_H
#define MAXSIZE 500
#include <sys/stat.h>
#include <sys/time.h>
#include <linux/icmp.h>
#include <linux/ip.h>
#include <arpa/inet.h>
#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <unistd.h>
#include <time.h>
#include <uci.h>
#include <libubus.h>

/* Debugging messages macro */
#define printf_dbg(fmt, args ...) \
	do { if (debug)  { fprintf(stderr, "[%s(), %s:%u] "			    \
				   fmt, __FUNCTION__, __FILE__, __LINE__, ## args); \
			 }							    \
	} while (0)

/* Create directory path with default settings of the system */
int createdir(const char *path);

/* Is debug enabled? */
extern int debug;

/* Search for text in file */
int grep(const char *file, const char *text[], size_t buf_size);

/* Search and return line with text in it */
char *grep_line(const char *file, const char *text[], size_t buf_size);

/* Append text to file */
int append(const char *file, const char *text);

/* Send ping request to IP and listen for response with timeout in secs
 * net.ipv4.ping_group_range has to be set to "0 0"
 * you could do that with: sysctl -w net.ipv4.ping_group_range="0 0" */
int ping(const char *ifname, unsigned int timeout, const char *dst);

/* Ping at most n times and see if it works or not */
int ping_n(int n, const char *ifname, unsigned int timeout, const char *dst);

/* Check if file exists and is *not* a directory */
int exists_not_directory(const char *file);

/* Reload strongswan */
void reload_strongswan(void);

/* Get config value from some package, section
 * @uci - uci context
 * @pkg - package name
 * @section - section name
 * @option - option name
 */
char *get_config(struct uci_context *uci, const char *pkg,
		 const char *section, const char *option);

/* Check if strongswan enabled
 * @uci - uci context
 */
int check_strongswan(struct uci_context *);

/* Change a string to new one and free in case it wasn't NULL */
/* @ptr - pointer to the old pointer containing a string
 * @new - the string we will replace @ptr with
 */
void change_ptrs(char **ptr, char *new);

/* Check if @path exists in ubus
 * @path - the path
 * Returns non-zero value if path exists
 */
uint32_t ubus_exists(const char *path);
#endif
