#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "rms_json.h"

int main(int argc, char *argv[])
{
	int dynamic_int[DYNAMIC_SIZE] = { 6, 7, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 26, 31, 32, 33, 34, 35};
	int static_int[STATIC_SIZE] = {1, 2, 3, 4, 8, 28, 29, 30};
	int all_int[ALL_SIZE] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35};
	//int custom[20];
	int *use;
	int size;
	int i, a;
	int p_v=0;
	char param[GSM_STRING_LEN];

	if( argc > 1 ){
		for(a = 1; a < argc; a++ ) {
			size=NULL;
			use=NULL;
			if(strncmp(argv[a],"static",6)==0 || strncmp(argv[a],"s",1)==0){
				if (p_v==0){
					printf("%s:%d;", VERSION_ID, VERSION);
					p_v=1;
				}
				use = static_int;
				size = STATIC_SIZE;

			}else if(strncmp(argv[a],"dynamic",7)==0 || strncmp(argv[a],"d",1)==0){
				if (p_v==0){
					printf("%s:%d;", VERSION_ID, VERSION);
					p_v=1;
				}
				use = dynamic_int;
				size = DYNAMIC_SIZE;

			}else if(strncmp(argv[a],"all",3)==0){
				if (p_v==0){
					printf("%s:%d;", VERSION_ID, VERSION);
					p_v=1;
				}
				use = all_int;
				size = ALL_SIZE;

			}else{
				if (p_v==0){
					printf("%s:%d;", VERSION_ID, VERSION);
					p_v=1;
				}
				return 1;
			}

			for( i = 0; i < size; i++ ) {
				switch (use[i]) {
					case IMEI: //IMEI
						get_param("--imei", param, 5);
						break;
					case MODEL: //Model
						get_param("--model", param, 5);
						break;
					case MANUF: //Manufacturer
						get_param("--manuf", param, 5);
						break;
					case REVISION: //Revision
						get_param("--revision", param, 5);
						break;
					case SERIAL: //Serial
						get_param("--serial", param, 5);
						break;
					case SIMSTATE:
						get_param("--simstate", param, 5);
						break;
					case PINSTATE:
						get_param("--pinstate", param, 5);
						break;
					case IMSI:
						get_param("--imsi", param, 5);
						break;
					case NETSTATE:
						get_param("--netstate", param, 5);
						break;
					case SIGNAL:
						get_param("--signal", param, 5);
						break;
					case OPERATOR:
						get_param("--operator", param, 5);
						break;
					case OPERNUM:
						get_param("--opernum", param, 5);
						break;
					case CONNSTATE:
						get_param("--connstate", param, 5);
						break;
					case CONNTYPE:
						get_param("--conntype", param, 5);
						break;
					case TEMP:
						get_param("--temp", param, 5);
						break;
					case RXCOUNTT:
						get_param("rx", param, 1);
						break;
					case TXCOUNTT:
						get_param("tx", param, 1);
						break;
					case RXCOUNTY:
						get_param("-y rx", param, 1);
						break;
					case TXCOUNTY:
						get_param("-y tx", param, 1);
						break;
					case FWVERSION:
						get_param("/etc/version", param, 2);
						break;
					case DIGITALINPUT:
						get_param("DIN1", param, 3);
						break;
					case DIGITALISOLATEDINPUT:
						get_param("DIN2", param, 3);
						break;
					case ANALOGINPUT:
						get_param("/sys/class/hwmon/hwmon0/device/in0_input", param, 2);
						break;
					case DIGITALOCOUTPUT:
						get_param("DOUT1", param, 3);
						break;
					case DIGITALRELAYOUTPUT:
						get_param("DOUT2", param, 3);
						break;
					case SIMSLOT:
						get_param("SIM", param, 3);
						break;
					case ROUTERNAME:
						get_param("/etc/config/system | grep -r routername | awk -F \"[']\" '{print $2}'", param, 2);
						break;
					case PRODUCTCODE:
						get_param("hwinfo.hwinfo.mnf_code", param, 6);
						break;
					case BATCHNUMBER:
						get_param("hwinfo.hwinfo.batch", param, 6);
						break;
					case HARDWAREREVISION:
						get_param("hwinfo.hwinfo.hwver", param, 6);
						break;
					case ROUTERUPTIME:
						get_param("/proc/uptime | awk -F'.' '{print $1}'", param, 2);
						break;
					case CONNECTIONUPTIME:
						get_param("call network.interface.wan status | grep uptime | awk '{print $2}' | awk -F ',' '{print $1}'", param, 4);
						break;
					case MOBILEIP:
						get_param("-p $(uci get network.ppp.ifname 2>&1)", param, 5);
						break;
					case SENT:
						get_param("-e $(uci get network.ppp.ifname 2>&1)", param, 5);
						break;
					case RECEIVED:
						get_param("-r $(uci get network.ppp.ifname 2>&1)", param, 5);
						break;
					default:
						strncpy(param, "Err", sizeof("Err"));
						break;
				}
				printf("%d:%s;",use[i],param);

			}
		}

	}else{
		printf("Error\n");
		return 1;
	}
	return 0;
}

int get_param(char *param, char *buffer, int use)
{
	char cmd[GSM_STRING_LEN];
	FILE* name = NULL;
	int iLength;

	if (use == 0)
		sprintf(cmd, "/usr/sbin/sysget %s 2>/dev/null", param);
	else if (use == 1)
		sprintf(cmd, "/usr/bin/mdcget %s 2>/dev/null", param);
	else if (use == 2)
		sprintf(cmd, "/bin/cat %s 2>/dev/null", param);
	else if (use == 3)
		sprintf(cmd, "/sbin/gpio.sh get  %s 2>/dev/null", param);
	else if (use == 4)
		sprintf(cmd, "ubus %s 2>/dev/null", param);
	else if (use == 5)
		sprintf(cmd, "gsmctl -n %s 2>/dev/null", param);
	else if (use == 6)
		sprintf(cmd, "uci get -q %s 2>/dev/null", param);

	if ((name = popen(cmd, "r")) == NULL){
		strncpy(buffer, "N/A", sizeof("N/A"));
		return -1;
	}

	if (fgets(buffer, GSM_STRING_LEN, name) == NULL) {
		pclose(name);
		strncpy(buffer, "N/A", sizeof("N/A"));
		return -1;
	} else {
		iLength = strlen(buffer);
		// trim 'new line' symbol at the end
		if (iLength && buffer[iLength-1] == '\n')
			buffer[iLength-1] = '\0';
	}
	pclose(name);
	return 0;
}
