#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define VERSION 1
#define DYNAMIC_SIZE 20
#define STATIC_SIZE 4
#define GSM_STRING_LEN 256
#define ALL_SIZE 35
#define VERSION_ID "0"

#define IMEI		1
#define MODEL		2
#define MANUF		3
#define REVISION	4
#define SERIAL		5
#define SIMSTATE	6
#define PINSTATE	7
#define IMSI		8
#define NETSTATE	9
#define SIGNAL		10
#define OPERATOR	11
#define OPERNUM		12
#define CONNSTATE	13
#define CONNTYPE	14
#define TEMP		15
#define RXCOUNTT	16
#define TXCOUNTT	17
#define RXCOUNTY	18
#define TXCOUNTY	19
#define FWVERSION	20
#define DIGITALINPUT			21
#define DIGITALISOLATEDINPUT	22
#define ANALOGINPUT				23
#define DIGITALOCOUTPUT			24
#define DIGITALRELAYOUTPUT		25
#define SIMSLOT					26
#define ROUTERNAME				27
#define PRODUCTCODE				28
#define BATCHNUMBER				29
#define HARDWAREREVISION		30
#define ROUTERUPTIME			31
#define CONNECTIONUPTIME		32
#define MOBILEIP				33
#define SENT					34
#define RECEIVED				35

int get_param(char *param, char *buffer, int use);
