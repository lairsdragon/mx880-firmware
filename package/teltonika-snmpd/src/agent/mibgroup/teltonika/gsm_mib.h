/*
 *  GSM MIB group interface - gsm.h
 *
 */

#ifndef _MIBGROUP_GSM_MIB_H
#define _MIBGROUP_GSM_MIB_H

config_require(util_funcs)

void init_gsm_mib(void);
extern FindVarMethod var_gsm;

#define IMEI1		1
#define MODEL1		2
#define MANUF1		3
#define REVISION1	4
#define SERIALNUM1	5
#define SIMSTATE1	6
#define PINSTATE1	7
#define IMSI1		8
#define ICCID1		9
#define LAC1		10
#define MCC1		11
#define MNC1		12
#define COPS1		13
#define PRIOR1		14
#define NETSTATE1	15
#define SIGNAL1		16
#define RSCP1		17
#define ECIO1		18
#define RSRP1		19
#define RSRQ1		20
#define SINR1		21
#define CELLID1		22
#define OPERATOR1	23
#define OPERNUM1	24
#define CONNSTATE1	25
#define CONNTYPE1	26
#define TEMP1		27
#define RXCOUNTT1	28
#define TXCOUNTT1	29
#define RXCOUNTY1	30
#define TXCOUNTY1	31
#define FWVERSION	32
#define DIGITALINPUT			33
#define DIGITALISOLATEDINPUT	34
#define ANALOGINPUT				35
#define DIGITALOCOUTPUT			36
#define DIGITALRELAYOUTPUT		37
#define SIMSLOT1				38
#define ROUTERNAME				39
#define PRODUCTCODE				40
#define BATCHNUMBER				41
#define HARDWAREREVISION		42
#define ROUTERSERIALNUM			43
#define ROUTERHOSTNAME			44
#define ROUTERUPTIME			45
#define CONNECTIONUPTIME		46
#define LANMAC					47
#define WLANMAC					48
#define MOBILEIP				49
#define SENT					50
#define RECEIVED				51
#define ENABLED					52
#define FIX						53
#define SAT						54
#define LAT						55
#define LONG					56
#define ALT						57
#define SPEED					58
#define COURSE					59
#define DATETIME				60

#endif	/* _MIBGROUP_GSM_MIB_H */
