/*
 *  GSM MIB group implementation - gsm.c
 *
 */

#include <net-snmp/net-snmp-config.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <ctype.h>
#include <net-snmp/net-snmp-includes.h>
#include <net-snmp/agent/net-snmp-agent-includes.h>

#include <sys/socket.h>
#include <sys/un.h>

#include "util_funcs.h"
#include "gsm_mib.h"
#include "struct.h"
#include <libgsm/sms.h>

#define GSM_STRING_LEN	128

char search_result[GSM_STRING_LEN];

#define BUFFER_SIZE 128
#define UNIX_SOCK_PATH "/tmp/gsmd.sock"

unsigned char nolog = 0;

int header_gsm(struct variable *, oid *, size_t *, int, size_t *, WriteMethod **);

/*
 * define the structure we're going to ask the agent to register our
 * information at
 */

struct variable1 gsm_variables1[] = {
	{IMEI1,			ASN_OCTET_STR, RONLY, var_gsm, 1, {1}},
	{MODEL1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {2}},
	{MANUF1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {3}},
	{REVISION1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {4}},
	{SERIALNUM1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {5}},
	{IMSI1,				ASN_OCTET_STR, RONLY, var_gsm, 1, {6}},
	{ROUTERNAME,		ASN_OCTET_STR, RONLY, var_gsm, 1, {7}},
	{PRODUCTCODE,		ASN_OCTET_STR, RONLY, var_gsm, 1, {8}},
	{BATCHNUMBER,		ASN_OCTET_STR, RONLY, var_gsm, 1, {9}},
	{HARDWAREREVISION,	ASN_OCTET_STR, RONLY, var_gsm, 1, {10}},
	{ROUTERSERIALNUM,	ASN_OCTET_STR, RONLY, var_gsm, 1, {11}},
	{ROUTERHOSTNAME,	ASN_OCTET_STR, RONLY, var_gsm, 1, {12}},
	{LANMAC,			ASN_OCTET_STR, RONLY, var_gsm, 1, {13}},
	{WLANMAC,			ASN_OCTET_STR, RONLY, var_gsm, 1, {14}},
};
struct variable1 gsm_variables2[] = {
	{ICCID1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {1}},
	{LAC1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {2}},
	{MCC1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {3}},
	{MNC1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {4}},
	{COPS1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {5}},
	{PRIOR1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {6}},
	{SIMSTATE1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {7}},
	{PINSTATE1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {8}},
	{NETSTATE1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {9}},
	{SIGNAL1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {10}},
	{RSCP1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {11}},
	{ECIO1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {12}},
	{RSRP1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {13}},
	{RSRQ1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {14}},
	{SINR1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {15}},
	{CELLID1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {16}},
	{OPERATOR1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {17}},
	{OPERNUM1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {18}},
	{CONNSTATE1,ASN_OCTET_STR, RONLY, var_gsm, 1, {19}},
	{CONNTYPE1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {20}},
	{TEMP1,		ASN_OCTET_STR, RONLY, var_gsm, 1, {21}},
	{RXCOUNTT1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {22}},
	{TXCOUNTT1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {23}},
	{RXCOUNTY1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {24}},
	{TXCOUNTY1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {25}},
	{FWVERSION,	ASN_OCTET_STR, RONLY, var_gsm, 1, {26}},
	{SIMSLOT1,	ASN_OCTET_STR, RONLY, var_gsm, 1, {27}},
	{ROUTERUPTIME,		ASN_OCTET_STR, RONLY, var_gsm, 1, {28}},
	{CONNECTIONUPTIME,	ASN_OCTET_STR, RONLY, var_gsm, 1, {29}},
	{MOBILEIP,			ASN_OCTET_STR, RONLY, var_gsm, 1, {30}},
	{SENT,				ASN_OCTET_STR, RONLY, var_gsm, 1, {31}},
	{RECEIVED,			ASN_OCTET_STR, RONLY, var_gsm, 1, {32}}
};

struct variable1 gsm_variables3[] = {
	{DIGITALINPUT,			ASN_OCTET_STR, RONLY, var_gsm, 1, {1}},
	{DIGITALISOLATEDINPUT,	ASN_OCTET_STR, RONLY, var_gsm, 1, {2}},
	{ANALOGINPUT,			ASN_OCTET_STR, RONLY, var_gsm, 1, {3}},
	{DIGITALOCOUTPUT,		ASN_OCTET_STR, RONLY, var_gsm, 1, {4}},
	{DIGITALRELAYOUTPUT,	ASN_OCTET_STR, RONLY, var_gsm, 1, {5}}
};

struct variable1 gsm_variables4[] = {
	{ENABLED,	ASN_OCTET_STR, RONLY, var_gsm, 1, {1}},
	{FIX,		ASN_OCTET_STR, RONLY, var_gsm, 1, {2}},
	{SAT,		ASN_OCTET_STR, RONLY, var_gsm, 1, {3}},
	{LAT,		ASN_OCTET_STR, RONLY, var_gsm, 1, {4}},
	{LONG,		ASN_OCTET_STR, RONLY, var_gsm, 1, {5}},
	{ALT,		ASN_OCTET_STR, RONLY, var_gsm, 1, {6}},
	{SPEED,		ASN_OCTET_STR, RONLY, var_gsm, 1, {7}},
	{COURSE,	ASN_OCTET_STR, RONLY, var_gsm, 1, {8}},
	{DATETIME,	ASN_OCTET_STR, RONLY, var_gsm, 1, {9}}
};
/*
 * Define the OID pointer to the Teltonika stolen ID
 */
oid gsm_variables_oid[] = { 1, 3, 6, 1, 4, 1, 15398, 10, 1, 1};
oid gsm_variables_oid2[] = { 1, 3, 6, 1, 4, 1, 15398, 10, 1, 2};
oid gsm_variables_oid3[] = { 1, 3, 6, 1, 4, 1, 15398, 10, 1, 5};
oid gsm_variables_oid4[] = { 1, 3, 6, 1, 4, 1, 15398, 10, 1, 6};
/*
 * Wrapper function to get internal params
 *
 */
int get_param(char *param, char *buffer, int use)
{
	char cmd[GSM_STRING_LEN];
	FILE* name = NULL;
	int iLength;

	if (use == 0)
		sprintf(cmd, "/usr/sbin/sysget %s 2>/dev/null", param);
	else if (use == 1)
		sprintf(cmd, "/usr/bin/mdcget %s 2>/dev/null", param);
	else if (use == 2)
		sprintf(cmd, "/bin/cat %s 2>/dev/null", param);
	else if (use == 3)
		sprintf(cmd, "/sbin/gpio.sh get  %s 2>/dev/null", param);
	else if (use == 4)
		sprintf(cmd, "ubus %s 2>/dev/null", param);
	else if (use == 5)
		sprintf(cmd, "gpsctl -n %s 2>/dev/null", param);
	if ((name = popen(cmd, "r")) == NULL)
		return -1;

	if (fgets(buffer, GSM_STRING_LEN, name) == NULL) {
		pclose(name);
		return -1;
	} else {
		iLength = strlen(buffer);
		// trim 'new line' symbol at the end
		if (iLength && buffer[iLength-1] == '\n')
			buffer[iLength-1] = '\0';
	}
	pclose(name);
	return 0;
}

/*
 * Wrapper function to get internal gsmctl params
 *
 */
int get_gsmctl_param(char *buffer, int param)
{
	struct sockaddr_un remote;
	int error = 0, socket_fd = -1, len;
	modem_dev device;
	struct uci_context *uci = NULL;
	char *ifname = NULL;
	int iLength;

	device = get_modem();
	if ((socket_fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
		printf("TIMEOUT\n");
		return 0;
	}
	remote.sun_family = AF_UNIX;
	strcpy(remote.sun_path, UNIX_SOCK_PATH);
	len = strlen(remote.sun_path) + sizeof(remote.sun_family);
	if (connect(socket_fd, (struct sockaddr *)&remote, len) == -1) {
		printf("TIMEOUT\n");

		return 0;
	}
	if(param == MOBILEIP || param == SENT || param == RECEIVED)
	{
		uci = uci_init();
		ifname = ucix_get_option(uci,"network", "ppp", "ifname");
	}

	switch(param){
		case IMEI1:
			error = gsmctl_get_imei(socket_fd, device, &buffer);
			break;

		case MODEL1:
			error = gsmctl_get_model(socket_fd, device, &buffer);
			break;

		case MANUF1:
			error = gsmctl_get_manuf(socket_fd, device, &buffer);
			break;

		case REVISION1:
			error = gsmctl_get_rev(socket_fd, device, &buffer);
			break;

		case SERIALNUM1:
			error = gsmctl_get_serialnum(socket_fd, device, &buffer);
			break;

		case SIMSTATE1:
			error = gsmctl_get_simstate(socket_fd, device, &buffer);
			break;

		case PINSTATE1:
			error = gsmctl_get_pinstate(socket_fd, device, &buffer);
			break;

		case IMSI1:
			error = gsmctl_get_imsi(socket_fd, device, &buffer);
			break;

		case ICCID1:
			error = gsmctl_get_iccid(socket_fd, device, &buffer);
			break;

		case LAC1:
			error = gsmctl_get_lac(socket_fd, device, &buffer);
			break;

		case MCC1:
			error = gsmctl_get_mcc(socket_fd, device, &buffer);
			break;

		case MNC1:
			error = gsmctl_get_mnc(socket_fd, device, &buffer);
			break;

		case COPS1:
			error = gsmctl_get_network_selection_mode(socket_fd, device, &buffer);
			break;

		case PRIOR1:
			error = gsmctl_get_network_access_order(socket_fd, device, &buffer);
			break;

		case NETSTATE1:
			error = gsmctl_get_netstate(socket_fd, device, &buffer);
			break;

		case SIGNAL1:
			error = gsmctl_get_signal_quality(socket_fd, device, &buffer);
			break;

		case RSCP1:
			error = gsmctl_get_signal_rscp(socket_fd, device, &buffer);
			break;

		case ECIO1:
			error = gsmctl_get_signal_ecio(socket_fd, device, &buffer);
			break;

		case RSRP1:
			error = gsmctl_get_signal_rsrp(socket_fd, device, &buffer);
			break;

		case RSRQ1:
			error = gsmctl_get_signal_rsrq(socket_fd, device, &buffer);
			break;

		case SINR1:
			error = gsmctl_get_signal_sinr(socket_fd, device, &buffer);
			break;

		case CELLID1:
			error = gsmctl_get_signal_cell_id(socket_fd, device, &buffer);
			break;

		case OPERATOR1:
			error = gsmctl_get_operator(socket_fd, device, &buffer);
			break;

		case OPERNUM1:
			error = gsmctl_get_opernum(socket_fd, device, &buffer);
			break;

		case CONNSTATE1:
			error = gsmctl_get_connstate(socket_fd, device, &buffer);
			break;

		case CONNTYPE1:
			error = gsmctl_get_conntype(socket_fd, device, &buffer);
			break;

		case TEMP1:
			error = gsmctl_get_temperature(socket_fd, device, &buffer);
			break;

		case MOBILEIP:
			error = gsmctl_get_ip(ifname, &buffer);
			break;

		case SENT:
			error = gsmctl_get_iface_stats(ifname, "tx", &buffer);
			break;

		case RECEIVED:
			error = gsmctl_get_iface_stats(ifname, "rx", &buffer);
			break;

		default:
			DEBUGMSGTL(("snmpd", "unknown sub-id %d in var_gsm\n", vp->magic));

	}

	if(param == MOBILEIP || param == SENT || param == RECEIVED)
	{
		uci_cleanup(uci);
		if(ifname)
			free(ifname);
	}

	if (error != NO_ERR && error != OK_ERR)
		printf("TIMEOUT\n");
	else{
		iLength = strlen(buffer);
		// trim 'new line' symbol at the end
		if (iLength && buffer[iLength-1] == '\n')
			buffer[iLength-1] = '\0';
		printf("%s", buffer);
	}

	close(socket_fd);
	return 0;
}

void init_gsm_mib(void)
{
	/* We might initialize our vars here */

	/*
	 * register ourselves with the agent to handle our mib tree
	 */
	REGISTER_MIB("mdex", gsm_variables1, variable1, gsm_variables_oid);
	REGISTER_MIB("mdex", gsm_variables2, variable1, gsm_variables_oid2);
	REGISTER_MIB("mdex", gsm_variables3, variable1, gsm_variables_oid3);
	REGISTER_MIB("mdex", gsm_variables4, variable1, gsm_variables_oid4);
}

u_char* var_gsm(struct variable *vp, oid *name, size_t *length, int exact, size_t *var_len, WriteMethod **write_method)
{
	if (header_generic(vp, name, length, exact, var_len, write_method) == MATCH_FAILED)
		return NULL;

	switch (vp->magic) {

	case IMEI1:
	case MODEL1:
	case MANUF1:
	case REVISION1:
	case SERIALNUM1:
	case SIMSTATE1:
	case PINSTATE1:
	case IMSI1:
	case ICCID1:
	case LAC1:
	case MCC1:
	case MNC1:
	case COPS1:
	case PRIOR1:
	case NETSTATE1:
	case SIGNAL1:
	case RSCP1:
	case ECIO1:
	case RSRP1:
	case RSRQ1:
	case SINR1:
	case CELLID1:
	case OPERATOR1:
	case OPERNUM1:
	case CONNSTATE1:
	case CONNTYPE1:
	case TEMP1:
	case MOBILEIP:
	case SENT:
	case RECEIVED:
		if (get_gsmctl_param(search_result, vp->magic) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case RXCOUNTT1:
		if (get_param("rx", search_result, 1) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case TXCOUNTT1:
		if (get_param("tx", search_result, 1) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case RXCOUNTY1:
		if (get_param("-y rx", search_result, 1) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case TXCOUNTY1:
		if (get_param("-y tx", search_result, 1) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case FWVERSION:
		if (get_param("/etc/version", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case DIGITALINPUT:
		if (get_param("DIN1", search_result, 3) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		if (strncmp(search_result, "0", sizeof("0")) == 0)
			sprintf(search_result, "Inactive");
		else
			sprintf(search_result, "Active");
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case DIGITALISOLATEDINPUT:
		if (get_param("DIN2", search_result, 3) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		if (strncmp(search_result, "0", sizeof("0")) == 0)
			sprintf(search_result, "Inactive");
		else
			sprintf(search_result, "Active");
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case ANALOGINPUT:
		if (get_param("/sys/class/hwmon/hwmon0/device/in0_input", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		double result;
		result = atof(search_result)/1000;
		sprintf(search_result, "%2.2f V", result);

		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case DIGITALOCOUTPUT:
		if (get_param("DOUT1", search_result, 3) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		if (strncmp(search_result, "0", sizeof("0")) == 0)
			sprintf(search_result, "Inactive");
		else
			sprintf(search_result, "Active");
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case DIGITALRELAYOUTPUT:
		if (get_param("DOUT2", search_result, 3) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		if (strncmp(search_result, "0", sizeof("0")) == 0)
			sprintf(search_result, "Inactive");
		else
			sprintf(search_result, "Active");
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case SIMSLOT1:
		if (get_param("SIM", search_result, 3) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case ROUTERNAME:
		if (get_param("/etc/config/system | grep -r routername | awk -F \"[']\" '{print $2}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case ROUTERHOSTNAME:
		if (get_param("/etc/config/system | grep -r hostname | awk -F \"[']\" '{print $2}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case PRODUCTCODE:
		if (get_param("/etc/config/hwinfo | grep -r 'mnf_code' | awk -F \"[']\" '{print $2}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case BATCHNUMBER:
		if (get_param("/etc/config/hwinfo | grep -r 'batch' | awk -F \"[']\" '{print $2}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case ROUTERSERIALNUM:
		if (get_param("/etc/config/hwinfo | grep -r 'serial' | awk -F \"[']\" '{print $2}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case HARDWAREREVISION:
		if (get_param("/etc/config/hwinfo | grep -r 'hwver' | awk -F \"[']\" '{print $2}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case ROUTERUPTIME:
		if (get_param("/proc/uptime | awk -F'.' '{print $1}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case CONNECTIONUPTIME:
		if (get_param("call network.interface.wan status | grep uptime | awk -F ' ' '{print $2}' | awk -F ',' '{print $1}'", search_result, 4) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case LANMAC:
		if (get_param("/sys/class/net/$(uci -q get network.lan.ifname | awk -F \"[ ]\" '{print $1}' 2>&1)/address", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case WLANMAC:
		if (get_param("/sys/class/net/wlan0/address", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case ENABLED:
		if (get_param("/etc/config/gps | grep -r 'enabled' | awk -F \"[']\" '{print $2}'", search_result, 2) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case FIX:
		if (get_param("--fixstatus", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case SAT:
		if (get_param("--satellites", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case LAT:
		if (get_param("--latitude", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case LONG:
		if (get_param("--longitude", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case ALT:
		if (get_param("--altitude", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case SPEED:
		if (get_param("--speed", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case COURSE:
		if (get_param("--course", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	case DATETIME:
		if (get_param("--datetime", search_result, 5) != 0)
			strncpy(search_result, "unknown", sizeof("unknown"));
		*var_len = strlen(search_result);
		*write_method = 0;
		return (u_char *) search_result;

	default:
		DEBUGMSGTL(("snmpd", "unknown sub-id %d in var_gsm\n", vp->magic));
  }

  return NULL;
}
