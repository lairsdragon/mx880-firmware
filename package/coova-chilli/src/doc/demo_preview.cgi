#!/usr/bin/lua

require("uci")

local uci = uci.cursor()
local page_config = "landingpage"
local debug_enable = 0


function get_values(section, options, make_id)
	local table = {}
	if type(options)=="table" then
		for i, option in ipairs(options) do
			table[option] = uci:get(page_config, section, option) or ""
		end
	else
		local value = uci:get(page_config, section, options) or ""
		if make_id then
			table[options] = value
		else
			return value
		end
	end

	if make_id then
		table.title_id = section .. "_title"
		table.text_id = section .. "_text"
	end
	return table
end

function debug(string)
	if debug_enable == 1 then
		os.execute("/usr/bin/logger -t hotspotlogin.cgi "..string)
	end
end

local page_title = uci:get("landingpage", "general", "title") or ""
local tos_enabled = get_values("terms", "enabled")

-- HTTP header
print [[
Content-Type: text/html; charset=iso-8859-1
]]

print ([[
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>]] ..page_title.. [[</title>
	<!--[if lt IE 9]><script src="/luci-static/mdex/html5.js"></script><![endif]-->
	<script type="text/JavaScript" src="/luci-static/mdex/hints.js"></script>
	<link rel="stylesheet" href="/luci-static/mdex/style.css">
	<link rel="stylesheet" href="/luci-static/resources/loginpage.css">
	<link rel="shortcut icon" href="/luci-static/mdex/favicon.ico">
	<style>
		.login_button {
			margin-top: 15px;
			text-align: center
		}

		.cbi-map-descr {
			text-align: center;
		}

		input {
			height: auto;
		}

		#logo_container {
			min-height: 50px;
			text-align: center;
		}

		#maincontent {
			min-width: 0px;
		}

	</style>
</head>

<body class="lang_en nil" style="width:100%;">
	<div id="maincontent" class="container">
		<div id="logo_container" >
			<img id="logo">
		</div>
		<form name="myForm">
			<div class="cbi-map">
]])



local self = get_values("welcome", {"title", "text"}, true)
print ([[
	<h2 style="text-align: center;">
		<a id="]] ..self.title_id.. [[">]] .. self.title .. [[</a>
	</h2>
	<div class="cbi-map-descr" id="]] .. self.text_id .. [[">
		]] .. self.text .. [[
			</div>
				<fieldset class="cbi-section">
					<fieldset class="cbi-section-node">
						<div class="cbi-value" style="text-align: center;">
							<div class="value-field">
								<label class="cbi-value-title1">Username</label><input class="cbi-input-user" type="text" name="UserName" value="admin">
							</div>
						</div>
						<div class="cbi-value cbi-value-last">
							<div class="value-field" style="text-align: center;">
								<label class="cbi-value-title1">Password</label><input id="focus_password" class="cbi-input-password" type="password" name="Password">
							</div>
						</div>
					</fieldset>
				</fieldset>
			</div>]])

if tos_enabled and tos_enabled == "1" then
	print ([[<div class="cbi-map-descr" style="text-align: center;">
			<script>
				function open_tos() {
					window.open('tos.lua', 'popup','width=700,height=500,scrollbars=yes,resizable=no,toolbar=no, directories=no,location=no,menubar=no,status=no,left=250,top=50');
				}
			</script>
			<input  type="checkbox" name="agree_tos" value="1">
			I understand, agree and ACCEPT the terms of the
			<a href="" onclick=open_tos(); "style="text-decoration: inherit;"> User Agreement </a>
		</div>]])
end

if get_values("link", "enabled") == "1" then
	local self = get_values("link", {"text", "url"})
	print ([[<div class="cbi-map-descr" style="text-align: center;">
				<a id="link" href="http://]]  .. self.url .. [[">]] .. self.text .. [[</a>
		</div>]])
end
print([[<div class="login_button" >
		<input type="submit" value="Login" class="cbi-button cbi-button-apply3" name="button">
	</div>
	</form>
</div>
</body>
</html>
]])
