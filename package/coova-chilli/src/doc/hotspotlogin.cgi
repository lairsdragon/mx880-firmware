#!/usr/bin/lua

require("uci")

local uci = uci.cursor()
local fs = require "luci.fs"
local string = require('string')
local find = string.find
local gsub = string.gsub
local char = string.char
local byte = string.byte
local format = string.format
local match = string.match
local gmatch = string.gmatch

local md5 = require"md5"
local bit = require"bit"

local config = "coovachilli"
local page_config = "landingpage"
local uamsecret = "uamsecret"
local loginpath = "/cgi-bin/hotspotlogin.cgi"
local debug_enable = 0
local userpassword

function get_values(section, option)
	local theme = uci:get(page_config, "general", "theme") or "custom"
	local value
	if theme ~= "custom" then
		local command = string.format("uci -c /etc/chilli/www/themes/ get %s.%s.%s", theme, section, option)
		value = getParam(command)
	else
		value = uci:get(page_config, section, option) or ""
	end
	return value
end

function debug(string)
	if debug_enable == 1 then
		os.execute("/usr/bin/logger -t hotspotlogin.cgi \""..string.."\"")
	end
end

function replace(page, repl)
	if page and repl then
		local tag_name
		local repl_string
		for word in string.gmatch(page, "%$%a+%$") do
			tag_name = word:gsub("%$", "")
--  		debug("tag:" ..tag_name)
			if repl[tag_name] then
				repl_string = repl[tag_name]:gsub("%%", "%%%%")
			else
				repl_string = ""
			end
			page = page:gsub("%$" .. tag_name .. "%$", repl_string)
		end
	end
	return page
end

--To use MSCHAPv2 Authentication with, 
--then uncomment the next two lines. 
--local ntresponse = 1
--local chilli_response = '/usr/local/sbin/chilli_response'

--Uncomment the following line if you want to use ordinary user-password (PAP)
--for radius authentication. 

function fromhex(str)
    return (gsub(str, '..', function (cc)
        return char(tonumber(cc, 16))
    end))
end

function tohex(str)
    return (gsub(str, '.', function (c)
        return format('%02x', byte(c))
    end))
end


function url_decode(str)
	if not str then return nil end
	str = string.gsub (str, "+", " ")
	str = string.gsub (str, "%%(%x%x)", function(h) return
		string.char(tonumber(h,16)) end)
	str = string.gsub (str, "\r\n", "\n")
	return str
end

function urlencode(str)
  if str then
    str = gsub(str, '\n', '\r\n')
    str = gsub(str, '([^%w ])', function(c)
      return format('%%%02X', byte(c))
    end)
    str = gsub(str, ' ', '+')
  end
  return str
end

function parse(str, sep, eq)
  if not sep then sep = '&' end
  if not eq then eq = '=' end
  local vars = {}
  for pair in gmatch(tostring(str), '[^' .. sep .. ']+') do
    if not find(pair, eq) then
      vars[urldecode(pair)] = ''
    else
      local key, value = match(pair, '([^' .. eq .. ']*)' .. eq .. '(.*)')
      if key then
        vars[url_decode(key)] = value
      end
    end
  end
  return vars
end

function getParam(string)
	local h = io.popen(string)
	local t = h:read()
	h:close()
	return t
end

function get_ifname(ip)
	local result = getParam(string.format("ip addr | grep \"%s\"", ip))
	local tun = string.match(result, "(tun%d+)")
	local ifname = "wlan0"

	if tun then
		local num = string.match(tun, "%d+")
		if num and tonumber(num) > 0 then
			ifname = string.format("wlan0-%s", num)
		end
	end

	debug("IFNAME: ".. ifname)
	return ifname
end

-- posted data (from a form)
local post_length = tonumber(os.getenv("CONTENT_LENGTH")) or 0
local params = {}

if os.getenv ("REQUEST_METHOD") == "POST" and post_length > 0 then
	debug("Request method post, reading stdin")
	POST_DATA = io.read (post_length)  -- read stdin
	if POST_DATA then
		debug("Parsing data")
		params = parse(POST_DATA)
	else
		debug("Cant get form data")
	end
	
elseif os.getenv ("REQUEST_METHOD") == "GET" then
	debug("Request method get")
	if os.getenv("QUERY_STRING") then
		query = os.getenv("QUERY_STRING")
	end

	if query then
		debug("Parsing data")
		params = parse(query)
	else
		debug("Can't get query string")
	end
end

--query and form values

local button = params['button'] or ""
local send = params['send'] or ""
local tel_num = params['TelNum'] or ""
local res = params['res'] or ""
local reason = params['reason']
local sms = params['sms'] or ""
local username = params['UserName'] or ""
local reply = params['reply'] or ""
reply = url_decode(reply)
local password = params['Password'] or ""
password = url_decode(password)
local uamip = params['uamip'] or ""
local uamport = params['uamport'] or ""
local userurl = params['userurl'] or ""
local userurldecode = url_decode(userurl)
local challenge = params['challenge'] or ""	
local redirurl = params['redirurl']
local redirurldecode = url_decode(redirurl)
local tos = params['agree_tos'] or "0"

--uci 
local section = "general"
uci:foreach(config, "general",
	function(s)
		local ip = string.match(s.net, "(%d+.%d+.%d+.%d+)")
		if uamip == ip then
			section = s[".name"]
		end
end)

local hotspot_id = section
local SSID = ""

uci:foreach("wireless", "wifi-iface",
	function(s)
		if s.hotspotid == hotspot_id then
			SSID = s.ssid												-- hotspot ssid now we can check if restricted
		end
end)



local is_restricted = uci:get("hotspot_scheduler", SSID, "restricted") or 0 --the restriction flag
local auth_mode = uci:get(config, section, "mode")
local page_title = uci:get("landingpage", "general", "title") or ""
local tos_enabled = get_values("terms", "enabled") or "0"
local path = uci:get("landingpage", "general", "loginPage") or "/etc/chilli/www/hotspotlogin.tmpl"
local page
local replace_tags = {
	pageTitle = page_title
}

if tos_enabled == "1" then
 	if button == "Login" and tos ~= "1" then
		debug("Terms of servise not aceepted")
 		button = ""
 		res = "failed"
 	end
end

if auth_mode == "extrad" or auth_mode == "intrad" then
	debug("Radius authentication")
	userpassword = 1
end


if (button and button ~= "") or res == "wispr" and username ~= "" then
	debug("Logging in...")
	print("Content-type: text/html\n\n")
	hexchal = fromhex(challenge)

	if uamsecret then
		debug("Uamsecret \""..uamsecret.."\" defined")
		newchal  = md5.sum(hexchal..""..uamsecret)
 	else
		debug("Uamsecret not defined")
 		newchal  = hexchal
 	end
 	
 	if ntresponse == 1 then
		debug("Encoding plain text into NT-Password ")
		--Encode plain text into NT-Password 
		--response = chilli_response -nt "$challenge" "$uamsecret" "$username" "$password"
		logonUrl = "http://"..uamip..":"..uamport.."/logon?username="..username.."&ntresponse="..response
 	elseif userpassword == 1 then
		debug("Encoding plain text password with challenge")
		--Encode plain text password with challenge 
		--(which may or may not be uamsecret encoded)
		
		--If challange isn't long enough, repeat it until it is
		while string.len(newchal) < string.len(password) do
			newchal = newchal..""..newchal
		end
		local result = ""
		local index = 1

		while index <= string.len(password) do
			result = result .. char(bit.bxor(string.byte(password, index), string.byte(newchal, index)))
			index = index + 1
		end
		
		pappassword = tohex(result)
		logonUrl = "http://"..uamip..":"..uamport.."/logon?username="..username.."&password="..pappassword
	
	else 
		debug("Generating a CHAP response with the password and the")
		--Generate a CHAP response with the password and the
		--challenge (which may have been uamsecret encoded)
		response = md5.sumhexa("\0"..password..""..newchal)
		logonUrl = "http://"..uamip..":"..uamport.."/logon?username="..username.."&response="..response.."&userurl="..userurl
	end
	
	print ([[<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
	<html>
		<head>
			<title>]] .. page_title.. [[ Login</title>
			<link rel="stylesheet" href="/luci-static/resources/loginpage.css">
			<meta http-equiv="Cache-control" content="no-cache">
			<meta http-equiv="Pragma" content="no-cache">
			<meta http-equiv='refresh' content="0;url=']] .. logonUrl .. [['>
		</head>
	<body >
		<div style="width:100%;height:100%;margin:auto;">
			<div style="text-align: center;position: absolute;top: 50%;left: 50%;height: 30%;width: 50%;margin: -15% 0 0 -25%;">
				<div style="width: 280px;margin: auto;">
					<small><img src="../luci-static/mdex/wait.gif"/> logging...</small>
				</div>
			</div>
		</div>
	</body>
	<!--
	<?xml version="1.0" encoding="UTF-8"?>
	<WISPAccessGatewayParam 
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:noNamespaceSchemaLocation="http://www.acmewisp.com/WISPAccessGatewayParam.xsd">
	<AuthenticationReply>
	<MessageType>120</MessageType>
	<ResponseCode>201</ResponseCode>
	<LoginResultsURL>]]..logonUrl..[[</LoginResultsURL>
	</AuthenticationReply>
	</WISPAccessGatewayParam>
	-->
	</html>
	]])
os.exit(0)
end

if send and send ~= "" and tel_num then
	debug("send sms")
	local ifname = get_ifname(uamip)
	local pass = getParam("/usr/bin/pwgen -nc 10 1")
	tel_num = tel_num:gsub("%%2B", "+")
	local exists = getParam("grep \"" ..tel_num.. "\" /etc/chilli/" .. ifname .. "/smsusers")
	local user = string.format("%s", pass)
	local message = getParam("/usr/sbin/gsmctl -Ss \"" ..tel_num.. " " .. pass .. "\"")

	if message == "OK" then
		sms = "sent"
		if exists then
			os.execute("sed -i 's/" ..exists.. "/" ..user.. "/g' /etc/chilli/" .. ifname .. "/smsusers")
		else
			os.execute("echo \"" ..user.. "\" >>/etc/chilli/" .. ifname .. "/smsusers")
		end
	else
		res = "notyet"
		sms = "error"
	end
end

--Default: It was not a form request

local result = 0

if res == "success" then -- If login successful
	debug("Result: success")
	result = 1
elseif res == "failed" then --If login failed
	debug("Result: failed")
	result = 2
elseif res == "logoff" then -- If logout successful
	debug("Result: logoff")
	result = 3
elseif res == "already" then -- If tried to login while already logged in
	debug("Result: already")
	result = 4
elseif res == "notyet" then -- If not logged in yet
	debug("Result: notyet")
	result = 5
elseif res == "wispr" then -- If login from smart client
	debug("Result: wispr")
	result = 6
elseif res == "popup1" then -- If requested a logging in pop up window
	debug("Result: popup1")
	result = 11
elseif res == "popup2" then -- If requested a success pop up window
	debug("Result: popup2")
	result = 12
elseif res == "popup3" then -- If requested a logout pop up window
	debug("Result: popup3")
	result = 13
end

--Otherwise it was not a form request
--Send out an error message
if result == 0 then
	section = "warning"
elseif result == 1 or result == 4 or result == 12 then
	section = "success"
	replace_tags.loginLogout = [[Click here to <a href='http://]] .. uamip .. [[:]] .. uamport .. [[/logoff'>logout</a>]]
elseif result == 3 or result == 13 then
	section = "logout"
	replace_tags.loginLogout = [[Click here to <a href='http://]].. uamip .. [[:]] .. uamport .. [[/prelogin'>Login</a>]]
elseif result == 2 or result == 5 then
	replace_tags.formHeader = [[<form name="myForm" method="post" action="]] .. loginpath .. [[">
			<INPUT TYPE="hidden" NAME="challenge" VALUE="]] .. challenge .. [[">
			<INPUT TYPE="hidden" NAME="uamip" VALUE="]] .. uamip .. [[">
			<INPUT TYPE="hidden" NAME="uamport" VALUE="]] .. uamport .. [[">
			<INPUT TYPE="hidden" NAME="userurl" VALUE="]] ..userurl .. [[">
			<INPUT TYPE="hidden" NAME="res" VALUE="]] .. res .. [[">]]
	replace_tags.formFooter = [[</form>]]

	local self
	debug("authmode:" ..auth_mode)
	if auth_mode == "sms" and result ~= 2 then
		section = "password"
		if sms == "notsent" then
			section = "phone"
		elseif sms == "error" then
			section = "error"
		end
	elseif result == 2 then
		section = "failed"
		if tos_enabled == "1" and tos == "0" and not reason then
			section = "terms"
			replace_tags.statusTitle = get_values("welcome", "title")
			replace_tags.statusContent = get_values(section, "warning")
		end
		if reason == "blocked" then
			replace_tags.statusContent = "User has reached data limit"
		end
	elseif result == 5 then
		section = "welcome"
	end


	if auth_mode ~= "sms" then
		replace_tags.inputUsername = [[<label class="cbi-value-title1">Username</label><input class="cbi-input-user" type="text" name="UserName">]]
	else
		replace_tags.inputUsername = [[<input type="hidden" name="UserName" value="-">]]
	end

	if auth_mode == "sms" and (sms == "notsent" or sms == "error")then
		replace_tags.inputPassword = [[<label class="cbi-value-tel">Phone number</label><input id="focus_password" class="cbi-input-password" type="text" name="TelNum" pattern="[0-9+]{4,20}">]]
	elseif auth_mode == "sms" then
		replace_tags.inputPassword = [[<label class="cbi-value-password">Password</label><input id="focus_password" class="cbi-input-password" type="text" name="UserName">]]
	else
		replace_tags.inputPassword = [[<label class="cbi-value-password">Password</label><input id="focus_password" class="cbi-input-password" type="password" name="Password">]]
	end

	if tos_enabled == "1" then --add terms of service
		if (auth_mode == "sms" and (sms ~= "notsent" and sms ~= "error")) or (auth_mode ~= "sms")then
			replace_tags.inputTos = [[
				<script>
					function open_tos() {
						window.open('tos.lua', 'popup','width=700,height=500,scrollbars=yes,resizable=no,toolbar=no, directories=no,location=no,menubar=no,status=no,left=250,top=50');
					}
				</script>
				<input  type="checkbox" name="agree_tos" value="1">
				I understand, agree and ACCEPT the terms of the
				<a href="" onclick=open_tos(); "style="text-decoration: underline;"> User Agreement </a>]]
		end
	end

	if is_restricted == "1" then
		replace_tags.submitButton = [[Access restricted!]]
	else
		if auth_mode == "sms" and (sms == "notsent" or sms == "error") then
			replace_tags.submitButton = [[<input type="submit" value="Send" class="cbi-button cbi-button-apply3" name="send">]]
		else
			replace_tags.submitButton = [[<input type="submit" value="Login" class="cbi-button cbi-button-apply3" name="button">]]
		end
	end
end

if get_values("link", "enabled") == "1" then --add link
	replace_tags.link = [[<a id="link" href="http://]]  .. get_values("link", "url") .. [[">]] .. get_values("link", "text") .. [[</a>]]
end

replace_tags.statusTitle = replace_tags.statusTitle or get_values(section, "title")
replace_tags.statusContent = replace_tags.statusContent or get_values(section, "text")
replace_tags.statusTitleId = section .. "_title"
replace_tags.statusContentId = section .. "_text"

if path then
	local file = assert(io.open(path, "r"))
	template = file:read("*all")
	file:close()
	page = replace(template, replace_tags)
end

-- HTTP header
print [[
Content-Type: text/html; charset=iso-8859-1
]]
--Print all page
print(page)
