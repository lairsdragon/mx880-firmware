
require "teltonika_lua_functions" 

local moduleType = luci.util.trim(luci.sys.exec("uci get system.module.type"))
local ModuleVidPid = luci.util.trim(luci.sys.exec("uci get system.module.vid"))..":"..luci.util.trim(luci.sys.exec("uci get system.module.pid"))
local m, s, o
local modulservice = "3G"

function debug(string)
		os.execute("logger " .. string)
end

if ModuleVidPid == "12D1:1573" or ModuleVidPid == "12D1:15C1" or ModuleVidPid == "1BC7:1201" then
	modulservice = "LTE"
end

if moduleType == "3g_ppp" then
	m = Map("simcard", translate("Mobile Configuration"),
		translate("Next, let's configure your mobile settings so you can start using internet right away."))
	m.wizStep = 2
	m.addremove = false

	s = m:section(NamedSection, "sim1", "", translate("Mobile Configuration (SIM1)"))
	s.addremove = false

	o = s:option(Value, "apn", translate("APN"), translate("APN (Access Point Name) is configurable network identifier used by a mobile device when connecting to a carrier"))
		
	o = s:option(Value, "pincode", translate("PIN number"), translate("SIM card PIN (Personal Identification Number) is a secret numeric password shared between a user and a system that can be used to authenticate the user"))
		o.datatype = "lengthvalidation(4,12,'^[0-9]+$')"

	o = s:option(ListValue, "auth_mode", translate("Authentication method"), translate("Authentication method that your carrier uses to authenticate new connections on its network"))
		o:value("chap", translate("CHAP"))
		o:value("pap", translate("PAP"))
		o:value("none", translate("None"))
		o.default = "none"

	o = s:option(Value, "username", translate("Username"), translate("Type in your username"))
		o:depends("auth_mode", "chap")
		o:depends("auth_mode", "pap")

	o = s:option(Value, "password", translate("Password"), translate("Type in your password"))
		o:depends("auth_mode", "chap")
		o:depends("auth_mode", "pap")
		o.password = true;

	o = s:option(ListValue, "service", translate("Service mode"), translate("Your network preference. If your local mobile network supports GSM (2G) and UMTS (3G) you can specify to which network you prefer to connect to"))
		o:value("lte-umts-gprs", translate("4G +3G +2G"))
		o:value("lte-umts", translate("4G +3G"))
		o:value("lte-gprs", translate("4G +2G"))
		o:value("umts-gprs", translate("3G +2G"))
		o:value("lte-only", translate("4G (LTE) only"))
		o:value("umts-only", translate("3G (UMTS) only"))
		o:value("gprs-only", translate("2G (GPRS) only"))
		o:value("auto", translate("Automatic"))
        o.default = "auto"

else
	m = Map("network_3g", translatef("Step - %s", modulservice), 
			translatef("Next, let's configure your %s settings so you can start using internet right away.", modulservice))
	m.wizStep = 2

	--[[
	config custom_interface '3g'
		option pin 'kazkas'
		option apn 'kazkas'
		option user 'kazkas'
		option password 'kazkas'
		option auth_mode 'chap' ARBA 'pap' (jei nerandu nieko ar kazka kita, laikau kad auth nenaudojama)
		option net_mode 'gsm' ARBA 'umts' ARBA 'auto' (prefered tinklas. jei nerandu nieko arba kazka kita laikau kad auto)
		option data_mode 'enabled' ARBA 'disabled' (ar leisti siusti duomenis. jei nera nieko ar kazkas kitas, laikau kad enabled)
	]]
	m.addremove = false

	s = m:section(NamedSection, "3g", "custom_interface", translatef(" %s Configuration", modulservice));
	s.addremove = false

	o = s:option(Value, "apn", translate("APN"), translate("APN (Access Point Name) is configurable network identifier used by a mobile device when connecting to a carrier"))

	o = s:option(Value, "pin", translate("PIN number"), translate("SIM card PIN (Personal Identification Number) is a secret numeric password shared between a user and a system that can be used to authenticate the user"))
	o.datatype = "range(0,9999)"

	auth = s:option(ListValue, "auth_mode", translatef(" %s authentication method", modulservice), translate("Authentication method that your carrier uses to authenticate new connections on its network"))

	auth:value("chap", translate("CHAP"))
	auth:value("pap", translate("PAP"))
	auth:value("none", translate("none"))
	auth.default = "none"

	o = s:option(Value, "user", translate("Username"), translate("Type in your username"))
	o:depends("auth_mode", "chap")
	o:depends("auth_mode", "pap")

	o = s:option(Value, "password", translate("Password"), translate("Type in your password"))
	o:depends("auth_mode", "chap")
	o:depends("auth_mode", "pap")
	o.password = true;

	o = s:option(ListValue, "net_mode", translate("Prefered network"), translate("Select network that you prefer"))

	o:value("gsm", translate("2G"))
	o:value("umts", translate("3G"))
	o:value("auto", translate("auto"))

	o.default = "auto"

	--[[o = s:option(Flag, "data_mode", translate("Data mode"))

	o.enabled = "enabled"
	o.disabled = "disabled"]]
end
function m.on_after_save()
	if m:formvalue("cbi.wizard.next") then
		m.uci:commit("simcard")
--kadangi jau po save tai reikia rankiniu budu perleist inis skripta
		luci.sys.call("/etc/init.d/sim_conf_switch restart >/dev/null")
		luci.sys.call("ifup ppp")
		luci.http.redirect(luci.dispatcher.build_url("admin/system/wizard/step-lan"))
	end
end
if m:formvalue("cbi.wizard.skip") then
	luci.http.redirect(luci.dispatcher.build_url("/admin/status/overview"))
end
return m
