--[[
Teltonika R&D. ver 0.1
]]--


local fs = require "nixio.fs"
local fw = require "luci.model.firewall"
require("luci.fs")
require("luci.config")

local uci = require("luci.model.uci").cursor()
ExpertMode = uci:get("system", "system", "expert")

local logDir, o, needReboot = false
local deathTrap = { }
m = Map("system", translate("Troubleshoot Settings"), translate(""))

s2 = m:section(TypedSection, "system", translate("Troubleshoot"))
s2.addremove = false

if ExpertMode and ExpertMode == "1" then
	con = s2:option(ListValue, "conloglevel", translate("System log level"), translate("You can watch logs by choosing the group from dropdown list and clicking button show"))
	con:value(8, translate("Debug"))
	con:value(7, translate("Info"))
	con:value(6, translate("Notice"))
	con:value(5, translate("Warning"))
	con:value(4, translate("Error"))
	con:value(3, translate("Critical"))
	con:value(2, translate("Alert"))
	con:value(1, translate("Emergency"))

	--
	-- log to flash or ram
	--
	logDir = s2:option(ListValue, "log_type", translate("Save log in"), translate("Specifies where logs will be saved. To apply setting router must be rebooted"))
	logDir:value("circular", translate("RAM memory"))
	logDir:value("file", translate("Flash memory"))

	function logDir.write(self, section, value)
		Value.write(self, section, value)
		if value == "file" then
			m.uci:set("system", section, "log_file", "/usr/var/log/messages")
		else
			m.uci:delete("system", section, "log_file")
		end
		m.uci:commit("system")
		needReboot = true
	end

	if luci.tools.status.show_mobile() then
	--
	-- enable gsmd log
	--
	o = s2:option(Flag, "enable_gsmd_log", translate("Include GSMD information"), translate("Check to include GSMD information to logs"))
	o.rmempty = false

	--
	-- enable pppd log
	--
		o = s2:option(Flag, "enable_pppd_debug", translate("Include PPPD information"), translate("Check to include PPPD information to logs"))
		o.rmempty = false

	--
	-- enable chat log
	--
	o = s2:option(Flag, "enable_chat_log", translate("Include chat script information"), translate("Check to include chat script information to logs"))
	o.rmempty = false

	end

	o2 = s2:option(Flag, "enable_topology", translate("Include network topology information"), translate("Check to include network topology information"))
	o2.rmempty = false
	o2.default = "0"

	function o2.write(self, section, value)

	end
end

	conLog = s2:option(Button, "_log")
	conLog.title      = translate("System log")
	conLog.inputtitle = translate("Show")
	conLog.inputstyle = "apply"

	conLog = s2:option(Button, "_kerlog")
	conLog.title      = translate("Kernel log")
	conLog.inputtitle = translate("Show")
	conLog.inputstyle = "apply"

	conLog = s2:option(Button, "_download")
	conLog.title      = translate("Troubleshoot file")
	conLog.inputtitle = translate("Download")
	conLog.inputstyle = "apply"
	conLog.timeload = true
	conLog.onclick = true

-----------------------Troubleshoot download-----------------
if m:formvalue("cbid.system.system._download") then
	local p1 = luci.http.formvalue("cbid.system.system.enable_topology")
	if p1 == "1" then
		luci.http.redirect(luci.dispatcher.build_url("admin/system/trdownload1"))
	else
		luci.http.redirect(luci.dispatcher.build_url("admin/system/trdownload"))
	end
end

if m:formvalue("cbid.system.system._log") then
	luci.http.redirect(luci.dispatcher.build_url("admin/status/syslog"))
end

if m:formvalue("cbid.system.system._kerlog") then
	luci.http.redirect(luci.dispatcher.build_url("admin/status/dmesg"))
end

function m.on_after_commit(self)
    -- do something if the UCI configuration got committed
	luci.sys.call("/etc/init.d/log restart")
	luci.http.redirect(luci.dispatcher.build_url("admin","system","admin","troubleshoot"))
end
return m, m2, m3
