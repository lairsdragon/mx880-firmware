require("luci.fs")
require("luci.config")


m = Map("openvpn", translate("Remote Monitoring "), translate(""))
m:chain("snmpd")

s = m:section(NamedSection, "teltonika_auth_service", "openvpn", translate("Remote Access Control"))
open_e = s:option(Flag, "enable", translate("Enable remote monitoring"), translate("Enable remote monitoring"))
open_e.rmempty = false

s = m:section(NamedSection, "", "", translate(""));
s.template = "admin_system/netinfo_monitoring"

function open_e.write(self, section, value)
	if value then
		m.uci:set("openvpn", "teltonika_auth_service", "enable", value)
		m.uci:commit("openvpn")
		m.uci:set("snmpd", "teltonika_auth_service", "enabled", value)
		m.uci:commit("snmpd")
-- 		luci.sys.call("uci set snmpd.teltonika_auth_service.enabled=\"".. value .."\"")
	end
end

function m.on_after_commit(self)
	luci.sys.call("/etc/init.d/openvpn restart")
end

return m
