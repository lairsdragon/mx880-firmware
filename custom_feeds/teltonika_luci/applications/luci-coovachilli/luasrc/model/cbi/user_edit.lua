--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: coovachilli.lua 3442 2008-09-25 10:12:21Z jow $
]]--
local ds = require "luci.dispatcher"
local function debug(string)
	luci.sys.call("logger \"" .. string .. "\"")
end
arg[1] = arg[1] or ""

m = Map( "coovachilli",	translate( "Hotspot Configuration" ), translate( "" ))
	local id = m.uci:get("coovachilli", arg[1], "id") or ""
	m.redirect = ds.build_url("admin/services/hotspot/general/" .. id)
	if m.uci:get("coovachilli", arg[1]) ~= "users" then
		luci.http.redirect(ds.build_url("admin/services/hotspot/general/" .. id))
		return
	end

users = m:section(NamedSection, arg[1], "user", translate( "Users Configuration Settings"))

name = users:option(Value, "username", translate("User name"), translate("" ))
	name.rmempty = false

pass = users:option(Value, "password", translate("User password"), translate("" ))
	pass.password = true
	pass.rmempty = false

idle = users:option( Value, "defidletimeout", translate("Idle timeout" ), translate("Max idle time in sec. (0, meaning unlimited)"))
	idle.default = "600"
	idle.datatype = "integer"

timeout = users:option( Value, "defsessiontimeout", translate("Session timeout" ), translate("Max session time in sec. (0, meaning unlimited)"))
	timeout.default = "600"
	timeout.datatype = "integer"

download_band = users:option(Value, "downloadbandwidth", translate("Download bandwidth"), translate("The max allowed download speed, in megabits." ))
	download_band.datatype = "integer"

	function download_band.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function download_band.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end

upload_band = users:option(Value, "uploadbandwidth", translate("Upload bandwidth"), translate("The max allowed upload speed, in megabits." ))
	upload_band.datatype = "integer"

	function upload_band.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function upload_band.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end

downloadlimit = users:option(Value, "downloadlimit", translate("Download limit"), translate("Disable hotspot user after download limit value in MB is reached"))
	downloadlimit.datatype = "integer"

	function downloadlimit.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function downloadlimit.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end

uploadlimit = users:option(Value, "uploadlimit", translate("Upload limit"), translate("Disable hotspot user after upload limit value in MB is reached"))
	uploadlimit.datatype = "integer"

	function uploadlimit.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function uploadlimit.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end


period = users:option(ListValue, "period", translate("Period"), translate("Period for which mobile data limiting should apply"))
	period:value("3", translate("Month"))
	period:value("2", translate("Week"))
	period:value("1", translate("Day"))
	--period:value("session", translate("Session"))

day = users:option(ListValue, "day", translate("Start day"), translate("A starting time for mobile data limiting period"))
	day:depends({period = "3"})
	for i=1,31 do
		day:value(i, i)
	end

hour = users:option(ListValue, "hour", translate("Start hour"), translate("A starting time for mobile data limiting period"))
	hour:depends({period = "1"})
	for i=1,23 do
		hour:value(i, i)
	end
	hour:value("0", "24")


weekday = users:option(ListValue, "weekday", translate("Start day"), translate("A starting time for mobile data limiting period"))
	weekday:value("1", translate("Monday"))
	weekday:value("2", translate("Tuesday"))
	weekday:value("3", translate("Wednesday"))
	weekday:value("4", translate("Thursday"))
	weekday:value("5", translate("Friday"))
	weekday:value("6", translate("Saturday"))
	weekday:value("0", translate("Sunday"))
	weekday:depends({period = "2"})

return m
