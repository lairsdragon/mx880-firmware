--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: coovachilli.lua 3442 2008-09-25 10:12:21Z jow $
]]--

local function debug(string)
	luci.sys.call("logger \"" .. string .. "\"")
end

local show = require("luci.tools.status").show_mobile()
local nw = require "luci.model.network"
local fs = require "luci.fs"
local localusers = "/etc/chilli/localusers"
local dsp = require "luci.dispatcher"

function round(num, idp)
  local mult = 10^(idp or 0)
  return math.floor(num * mult + 0.5) / mult
end

arg[1] = arg[1] or ""

m = Map( "coovachilli",	translate( "Wireless Hotspot Configuration" ), translate( "" ))
-- m.redirect = dsp.build_url("admin/services/hotspot/general/")
nw.init(m.uci)

-----------------------------------------------------------------------

scc = m:section( NamedSection, arg[1], "general", translate( "General Settings") )
	scc:tab("general", translate("Main Settings"))
	scc:tab("session", translate("Session Settings"))

cen = scc:taboption("general", Flag, "enabled", translate("Enable"), translate("Enable hotspot functionality on the router"))

net = scc:taboption("general", Value, "net", translate("AP IP"), translate("The IP address of this router on the hotspot network. 192.168.2.254/24 means network IP address 192.168.2.0 with a subnet mask 255.255.255.0" ))

mode = scc:taboption("general", ListValue, "mode", translate("Authentication mode"), translate("RADIUS server authentication mode"))
	mode.template = "chilli/auth_mode"
	mode:value("extrad", "External RADIUS")
	mode:value("intrad", "Internal RADIUS")
	mode:value("norad", "Without RADIUS")
	if show then
		mode:value("sms", "SMS OTP")
	end
	mode.default="extrad"

	function mode.write(self, section, value)

		if value == "intrad" then
			m.uci:set("radius", "radius", "enabled", "1")
			m.uci:set("radius", "general", "enabled", "1")
			m.redirect = dsp.build_url("admin/services/hotspot/radius/hotspot")
		else
			local disable_radius = true

			m.uci:foreach(self.config, "general", function(s)
				if s.mode == "intrad" and s.enabled == "1" and s[".name"] ~= section then
					disable_radius = false
				end
			end)

			if disable_radius then
				m.uci:set("radius", "general", "enabled", "0")
				m.uci:set("radius", "radius", "enabled", "0")
			end
		end
		m.uci:save("radius")
		m.uci:commit("radius")
		m.uci:set(self.config, section, self.option, value)
	end

rs1 = scc:taboption("general", Value, "radiusserver1", translate("RADIUS server #1" ), translate("The IP address of the first RADIUS server that is to be used to authenticate your wireless clients"))
	rs1:depends("mode","extrad")


rs2 = scc:taboption("general", Value, "radiusserver2", translate("RADIUS server #2" ), translate("The IP address of the second RADIUS server that is to be used to authenticate your wireless clients"))
	rs2:depends("mode","extrad")


rap = scc:taboption("general", Value, "radiusauthport", translate("Authentication port" ), translate("RADIUS server authentication port"))
	rap.datatype = "port"
	rap:depends("mode","extrad")

ras = scc:taboption("general", Value, "radiusacctport", translate("Accounting port" ), translate("RADIUS server accounting port"))
	ras.datatype = "port"
	ras:depends("mode","extrad")

-- hnm = scc:option( Value, "hotspotname", translate("Hotspot name" ), translate("The name of your hotspot. Will appear on the login screen"))

rcp = scc:taboption("general", Value, "radiussecret", translate("Secret key" ), translate("The secret key is used for authentication with the RADIUS server"))
	rcp.password = true
	rcp:depends("mode","extrad")

uam = scc:taboption("general", Value, "uamsecret", translate("UAM secret" ), translate("Shared secret between uamserver and hotspot."))
	uam.password = true
	uam:depends("mode","extrad")

nasid = scc:taboption("general", Value, "nasid", translate("NAS Identifier" ), translate("NAS Identifier"))
	nasid:depends("mode","extrad")

etern = scc:taboption("general", Flag, "externalpage", translate("External landing page" ), translate("Use external landing page"))
	etern:depends("mode","extrad")
	etern:depends("mode","intrad")
	etern:depends("mode","norad")

landing = scc:taboption("general", Value, "externadress", translate("Landing page address" ), translate("External landing page address (www.example.com)"))
	landing:depends("mode","extrad")
	landing:depends("mode","intrad")
	landing:depends("mode","norad")

prot = scc:taboption("general", ListValue, "protocol", translate("Protocol"), translate("Protocol to be used for landing page"))
	prot:value( "http", translate("HTTP"))
	prot:value( "https", translate("HTTPS"))
	prot.default = "http"



https = scc:taboption("general", Flag, "https", translate("HTTPS redirect"), translate(""))

key = scc:taboption("general", FileUpload, "sslkeyfile", translate("SSL key file"), translate(""))
	key:depends("https","1")

cert = scc:taboption("general", FileUpload, "sslcertfile", translate("SSL certificate file"), translate(""))
	cert:depends("https","1")


uli = scc:taboption("session", Value, "uamlogoutip", translate("Logout address" ), translate("IP address to instantly logout a client accessing it "))
	uli.default = "1.1.1.1"
	uli.datatype = "ip4addr"

idle = scc:taboption("session", Value, "defidletimeout", translate("Idle timeout" ), translate("Max idle time in sec. (0, meaning unlimited)"))
	idle.default = "600"
	idle.datatype = "integer"
	idle:depends("mode", "sms")
	idle:depends("mode", "norad")

timeout = scc:taboption("session", Value, "defsessiontimeout", translate("Session timeout" ), translate("Max session time in sec. (0, meaning unlimited)"))
	timeout.default = "600"
	timeout.datatype = "integer"
	timeout:depends("mode", "sms")
	timeout:depends("mode", "norad")

download_band = scc:taboption("session", Value, "downloadbandwidth", translate("Download bandwidth"), translate("The max allowed download speed, in megabits." ))
	download_band.datatype = "integer"
	download_band:depends("mode", "sms")
	download_band:depends("mode", "norad")

	function download_band.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function download_band.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end

upload_band = scc:taboption("session", Value, "uploadbandwidth", translate("Upload bandwidth"), translate("The max allowed upload speed, in megabits." ))
	upload_band.datatype = "integer"
	upload_band:depends("mode", "sms")
	upload_band:depends("mode", "norad")

	function upload_band.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function upload_band.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end

downloadlimit = scc:taboption("session", Value, "downloadlimit", translate("Download limit"), translate("Disable hotspot user after download limit value in MB is reached"))
	downloadlimit.datatype = "integer"
	downloadlimit:depends("mode", "sms")
	downloadlimit:depends("mode", "norad")

	function downloadlimit.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function downloadlimit.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end

uploadlimit = scc:taboption("session", Value, "uploadlimit", translate("Upload limit"), translate("Disable hotspot user after upload limit value in MB is reached"))
	uploadlimit.datatype = "integer"
	uploadlimit:depends("mode", "sms")
	uploadlimit:depends("mode", "norad")

	function uploadlimit.write(self, section, value)
		value = tonumber(value) * 1048576
		m.uci:set(self.config, section, self.option, value)
	end

	function uploadlimit.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option)
		if value then
			value = tonumber(value) / 1048576
		else
			value = nil
		end
		return value
	end


period = scc:taboption("session", ListValue, "period", translate("Period"), translate("Period for which mobile data limiting should apply"))
	period:value("3", translate("Month"))
	period:value("2", translate("Week"))
	period:value("1", translate("Day"))
	period:depends("mode", "norad")
	period:depends("mode", "sms")

day = scc:taboption("session", ListValue, "day", translate("Start day"), translate("A starting time for mobile data limiting period"))
	day:depends({period = "3", mode = "norad"})
	day:depends({period = "3", mode = "sms"})
	for i=1,31 do
		day:value(i, i)
	end

hour = scc:taboption("session", ListValue, "hour", translate("Start hour"), translate("A starting time for mobile data limiting period"))
	hour:depends({period = "1", mode = "norad"})
	hour:depends({period = "1", mode = "sms"})
	for i=1,23 do
		hour:value(i, i)
	end
	hour:value("0", "24")


weekday = scc:taboption("session", ListValue, "weekday", translate("Start day"), translate("A starting time for mobile data limiting period"))
	weekday:value("1", translate("Monday"))
	weekday:value("2", translate("Tuesday"))
	weekday:value("3", translate("Wednesday"))
	weekday:value("4", translate("Thursday"))
	weekday:value("5", translate("Friday"))
	weekday:value("6", translate("Saturday"))
	weekday:value("0", translate("Sunday"))
	weekday:depends({period = "2", mode = "norad"})
	weekday:depends({period = "2", mode = "sms"})

sc1 = m:section(TypedSection, "users", translate("Users Configuration"))
	sc1.id = arg[1]
	sc1.addremove = true
	sc1.anonymous = true
	sc1.template  = "chilli/tblsection"
	sc1.novaluetext = "There are no users created yet."
	sc1.extedit   = dsp.build_url("admin/services/hotspot/user_edit/%s")
	sc1.redirect = true

	function sc1.create(self, section)
		local user_exists = false
		local name = m:formvalue("_newinput.username") or ""
		local pass = m:formvalue("_newinput.pass") or ""
		m.uci:foreach(self.config, "users", function(s)
			if s.username == name and s.id == arg[1] then
				user_exists=true
			end
		end)
		if not user_exists then
			created = TypedSection.create(self, section)
			self.map:set(created, "username",   name)
			self.map:set(created, "password", pass)
			self.map:set(created, "id", arg[1])
		else
			m.message = translate("err: User \"" .. name .. "\" exists")
		end
	end

user = sc1:option(DummyValue, "username", translate("User name"), translate("Names of authorized users which will have the right to use wireless hotspot"))

	function user.parse(self, section, novld) --Custominis parse reiklaingas, kad saugand  neistrintu pasleptu sekciju
		local fvalue = self:formvalue(section)
		local cvalue = self:cfgvalue(section)

		-- If favlue and cvalue are both tables and have the same content
		-- make them identical
		if type(fvalue) == "table" and type(cvalue) == "table" then
			local equal = #fvalue == #cvalue
			if equal then
				for i=1, #fvalue do
					if cvalue[i] ~= fvalue[i] then
						equal = false
					end
				end
			end
			if equal then
				fvalue = cvalue
			end
		end

		if fvalue and #fvalue > 0 then -- If we have a form value, write it to UCI
			local val_err
			fvalue, val_err = self:validate(fvalue, section)
			fvalue = self:transform(fvalue)

			if not fvalue and not novld then
				self:add_error(section, "invalid", val_err)
			end

			if fvalue and (self.forcewrite or not (fvalue == cvalue)) then
				if self:write(section, fvalue) then
					-- Push events
					self.section.changed = true
					--luci.util.append(self.map.events, self.events)
				end
			end
		end
	end


	function user.write(self, section, value)
		if value and value ~= "" then
			m.uci:set(self.config, section, self.option, value)
			m.uci:set(self.config, section, "id", arg[1])
		end
	end
pass = sc1:option(DummyValue, "password", translate("Password"), translate("Passwords of authorized users which will have the right to use wireless hotspot"))
	pass.password = true

	function pass.parse(self, section, novld) --Custominis parse reiklaingas, kad saugand  neistrintu pasleptu sekciju
		local fvalue = self:formvalue(section)
		local cvalue = self:cfgvalue(section)

		-- If favlue and cvalue are both tables and have the same content
		-- make them identical
		if type(fvalue) == "table" and type(cvalue) == "table" then
			local equal = #fvalue == #cvalue
			if equal then
				for i=1, #fvalue do
					if cvalue[i] ~= fvalue[i] then
						equal = false
					end
				end
			end
			if equal then
				fvalue = cvalue
			end
		end

		if fvalue and #fvalue > 0 then -- If we have a form value, write it to UCI
			local val_err
			fvalue, val_err = self:validate(fvalue, section)
			fvalue = self:transform(fvalue)

			if not fvalue and not novld then
				self:add_error(section, "invalid", val_err)
			end

			if fvalue and (self.forcewrite or not (fvalue == cvalue)) then
				if self:write(section, fvalue) then
					-- Push events
					self.section.changed = true
					--luci.util.append(self.map.events, self.events)
				end
			end
		end
	end

idle = sc1:option(DummyValue, "defidletimeout", translate("Idle timeout" ), translate("Max idle time in sec. (0, meaning unlimited)"))
	idle.default = "600"
	idle.datatype = "integer"

	function idle.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option) or "0"
		if value == "0" then
			return "Unlimited"
		else
			return value .. " sec."
		end
	end

timeout = sc1:option(DummyValue, "defsessiontimeout", translate("Session timeout" ), translate("Max session time in sec. (0, meaning unlimited)"))
	timeout.default = "600"
	timeout.datatype = "integer"

	function timeout.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option) or "0"
		if value == "0" then
			return "Unlimited"
		else
			return value .. " sec."
		end
	end

download_band = sc1:option(DummyValue, "downloadbandwidth", translate("Download bandwidth"), translate("The max allowed download speed, in bits." ))
	download_band.datatype = "integer"

	function download_band.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option) or "0"
		if value == "0" then
			value = "Unlimited"
		else
			local tail
			local measure = {"bps.", "Kbps", "Mbps.", "Gbps."}
			value = tonumber(value)
			for i, n in pairs(measure) do
				tail = n
				if value >= 1024 and n ~= "Gbps." then
					value = value / 1024
				else
					break
				end
			end
			value = string.format("%s %s", round(value, 3), tail)
		end
		return value
	end

upload_band = sc1:option(DummyValue, "uploadbandwidth", translate("Upload bandwidth"), translate("The max allowed upload speed, in bits." ))
	upload_band.datatype = "integer"

	function upload_band.cfgvalue(self, section)
		local value = m.uci:get(self.config, section, self.option) or "0"
		if value == "0" then
			value = "Unlimited"
		else
			local tail
			local measure = {"bps.", "Kbps", "Mbps.", "Gbps."}
			value = tonumber(value)
			for i, n in pairs(measure) do
				tail = n
				if value >= 1024 and n ~= "Gbps." then
					value = value / 1024
				else
					break
				end
			end
			value = string.format("%s %s", round(value, 3), tail)
		end
		return value
	end

allowed = m:section(TypedSection, "uamallowed", translate("List Of Addresses The Client Can Access Without First Authenticating"))
	allowed.addremove = true
	allowed.anonymous = true
	allowed.template  = "cbi/tblsection"
	allowed.novaluetext = "There are no addresses created yet."

	function allowed.create(self, section)
		created = TypedSection.create(self, section)
		self.map:set(created, "instance", arg[1])
	end

enb = allowed:option( Flag, "enabled", translate("Enable"), translate(""))

domain = allowed:option( Value, "domain", translate("Address"), translate("Domain name, IP address or network segment"))

port = allowed:option( Value, "port", translate("Port"))
	port.datatype = "port"
	port:depends("subdomains", "")

sub = allowed:option( Flag, "subdomains", translate("Allow subdomains"), translate(""))


return m
