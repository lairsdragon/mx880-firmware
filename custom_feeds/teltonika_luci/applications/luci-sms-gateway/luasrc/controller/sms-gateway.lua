module("luci.controller.sms-gateway", package.seeall)

function index()
	local show = require("luci.tools.status").show_mobile()
	if  show then
		entry({"admin", "services", "sms_gateway","sms_counter"}, call("sms_counter")).leaf = true
	end
end

function sms_counter()
	local sys = require "luci.sys"
	step = tonumber(luci.http.formvalue("step"))
	function get_values()
		local values = sys.exec("/sbin/sms_counter.lua value both")
		local line_counter = 1
		send = {}
		recieved = {}
		if values then
			for line in values:gmatch("[^\r\n]+") do
				send[line_counter] = string.gsub(line," .*","")
				if tonumber(send[line_counter]) == nil then
					send[line_counter] = nil
				end
				recieved[line_counter] = string.gsub(line,".* ","")
				if tonumber(recieved[line_counter]) == nil then
					recieved[line_counter] = nil
				end
				line_counter = line_counter + 1
			end
		end
		for i=1,2 do
			send[i] = send[i] or "0"
			recieved[i] = recieved[i] or "0"
		end
	end
	if step then
		luci.sys.exec("/sbin/sms_counter.lua reset SLOT" .. step)
	end
	get_values()
	rv = {
			send = send,
			recieved = recieved
	}
	luci.http.prepare_content("application/json")
	luci.http.write_json(rv)
end
