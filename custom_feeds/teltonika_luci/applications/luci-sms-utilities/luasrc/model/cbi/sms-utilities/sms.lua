
local ds = require "luci.dispatcher"

m = Map("sms_utils", translate("SMS Utilities"),translate(""))

s = m:section(TypedSection, "rule", translate("SMS Rules"))
s.template  = "cbi/tblsection"
s.addremove = true
s.anonymous = true
s.sortable  = true
s.extedit   = ds.build_url("admin/services/sms/sms-utilities/%s")
s.template_addremove = "sms-utilities/cbi_addsms_rule"
s.novaluetext = translate("There are no SMS rules created yet")
s.hidden_rule = {"action", "get_configure"}

function s.create(self, section)
	local t = m:formvalue("_newinput.smstext") or ""
	local tr = m:formvalue("_newinput.tel") or ""
	local a = m:formvalue("_newinput.action")

	created = TypedSection.create(self, section)
	self.map:set(created, "smstext",   t)
	self.map:set(created, "tel", tr)
	self.map:set(created, "action", a)
end

function s.parse(self, ...)
	TypedSection.parse(self, ...)
	if created then
		m.uci:save("sms_utils")
		luci.http.redirect(ds.build_url("admin/services/sms/sms-utilities", created	))
	end
end

src = s:option(DummyValue, "action", translate("Action"), translate("The action to be performed when a rule is met"))
src.rawhtml = true
src.width   = "30%"
function src.cfgvalue(self, s)
	local z = self.map:get(s, "action")
	if z == "send_status" then
		return translate("Get status")
	elseif z == "iostatus" then
		return translate("Get I/O status")
	elseif z == "vpnstatus" then
		return translate("Get OpenVPN status")
	elseif z == "reboot" then
		return translate("Reboot")
	elseif z == "wifi" then
		local state = self.map:get(s, "value")
		if state == "off" then
			return translate("Switch WiFi off")
		else
			return translate("Switch WiFi on")
		end
	elseif z == "mobile" then
		local state = self.map:get(s, "value")
		if state == "off" then
			return translate("Switch mobile data off")
		else
			return translate("Switch mobile data on")
		end
	elseif z == "change_mobile_settings" then
		return translate("Change mobile data settings")
	elseif z == "list_of_profile" then
		return translate("Get list of profiles")
	elseif z == "change_profile" then
		return translate("Change profile")
	elseif z == "vpn" then
		return translate("Manage OpenVPN")
	elseif z == "dout" then
		local state = self.map:get(s, "value")
		if state == "off" then
			return translate("Switch output off")
		else
			return translate("Switch output on")
		end
	elseif z == "ssh_access" then
		return translate("SSH access Control")
	elseif z == "web_access" then
		return translate("Web access Control")
	elseif z == "firstboot" then
		return translate("Restore to default")
	elseif z == "switch_sim" then
		return translate("Force switch SIM")
	elseif z == "gps_coordinates" then
		return translate("GPS coordinates")
	elseif z == "fw_upgrade" then
		return translate("Force FW upgrade from server")
	elseif z == "config_update" then
		return translate("Force Config update from server")
	elseif z == "monitoring" then
		local state = self.map:get(s, "value")
		if state == "off" then
			return translate("Switch monitoring off")
		else
			return translate("Switch monitoring on")
		end
	elseif z == "gps" then
		local state = self.map:get(s, "value")
		if state == "off" then
			return translate("GPS off")
		else
			return translate("GPS on")
		end
	else
		return translate("N/A")
	end
end

src = s:option(DummyValue, "smstext", translate("SMS Text"), translate("SMS text that is required to trigger the rule"))
src.rawhtml = true
src.width   = "30%"

--src = s:option(DummyValue, "tel", translate("Sender's phone number"), translate("Text of message which will be expected to do the action"))
--src.rawhtml = true
--src.width   = "20%"

o = s:option(Flag, "enabled", translate("Enable"), translate("Make a rule active/inactive"))

local save = m:formvalue("cbi.apply")
if save then
	--Delete all usr_enable from sms_utils config
	m.uci:foreach("sms_utils", "rule", function(s)
		sms_inst = s[".name"] or ""
		smsEnable = m:formvalue("cbid.sms_utils." .. sms_inst .. ".enabled") or "0"
		sms_enable = s.enabled or "0"
		if smsEnable ~= sms_enable then
			m.uci:foreach("sms_utils", "rule", function(a)
				sms_inst2 = a[".name"] or ""
				local usr_enable = a.usr_enable or ""
				if usr_enable == "1" then
					m.uci:delete("sms_utils", sms_inst2, "usr_enable")
				end
			end)
		end
	end)
	m.uci:save("sms_utils")
	m.uci.commit("sms_utils")
end

return m
