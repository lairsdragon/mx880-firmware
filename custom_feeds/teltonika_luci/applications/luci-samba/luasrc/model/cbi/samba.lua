--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: samba.lua 6984 2011-04-13 15:14:42Z soma $
]]--
require("luci.tools.webadmin")
local sys = require "luci.sys"
local utl = require "luci.util"
local uci  = require "luci.model.uci".cursor()
local fw = require "luci.model.firewall"
fw.init(uci)

m = Map("samba", translate("Network Shares"))

s = m:section(TypedSection, "samba", translate("Samba"))
s.anonymous = true

--s:tab("general",  translate("General Settings"))
--s:tab("template", translate("Edit Template"))
o = s:option(Flag, "enable", translate("Enable"), translate("Enable samba"))
o.default= "0"
o.rmempty= false

s:option( Value, "name", translate("Hostname"))
s:option( Value, "description", translate("Description"))
s:option( Value, "workgroup", translate("Workgroup"))
--s:option( Value, "homes", translate("Share home-directories"), translate("Allow system users to reach their home directories via network shares"))

remote = s:option(Flag, "enable_remote", translate("Enable remote access"), translate(" Enable remote access"))
remote.default= "0"
remote.rmempty= false

function o.write(self, section, value)
	local __define__rule_name = "Enable_SAMBA_WAN"
	local remoteEnable = remote:formvalue(section)
	local openPort = "137 138 139"
	local fwRuleFound = false
		uci:set("samba", section, "enable", value)
		uci:commit("samba")

			-- scan existing rules
		uci:foreach("firewall", "rule", function(s)
			if s.name == __define__rule_name then
				fwRuleInstName = s[".name"]
				fwRuleEnabled = s.enable
				fwRulePort = s.dest_port
				fwRuleFound = true
			end
		end)
		
		-- update values if rule exists
		if fwRuleFound then
			os.execute("logger \"rado\"")

			os.execute("logger \"fwRulePort: |".. fwRulePort .."|\"")
-- 			os.execute("logger \"fwRuleEnabled: |".. fwRuleEnabled .."|\"")

			if remoteEnable == "1" then
				uci:set("firewall", fwRuleInstName, "enabled", "1")
				needsUpdate = true
			elseif remoteEnable == "0" or remoteEnable == nil then
				uci:set("firewall", fwRuleInstName, "enabled", "0")
				needsUpdate = true
			end
		end

		if not fwRuleFound then
			os.execute("logger \"nerado\"")
			local wanZone = fw:get_zone("wan")
			if not wanZone then
				m.message = translate("err: Error: could not add firewall rule!")
				return
			end

			local options = {
				target 		= "ACCEPT",
				proto 		= "tcp udp",
				dest_port 	= openPort,
				name 		= __define__rule_name,
				enabled		= remoteEnable
			}
			wanZone:add_rule(options)
			needsUpdate = true
		end

		if needsUpdate == true then
			uci:save("firewall")
			uci:commit("firewall")
		end
end

s = m:section(TypedSection, "sambashare", translate("Shared Directories"))
s.anonymous = true
s.addremove = true
s.template = "cbi/tblsection"

s:option(Value, "name", translate("Name"))
pth = s:option(Value, "path", translate("Path"))

local ioman = utl.trim(sys.exec("df | grep /mnt/ | grep -v /mnt/mtdblock | awk -F' ' '{print $6}'"))
for iomans in ioman:gmatch("[^\r\n]+") do 
	pth:value(iomans)
end

-- if nixio.fs.access("/etc/config/fstab") then
--         pth.titleref = luci.dispatcher.build_url("admin", "system", "fstab")
-- end

go = s:option(Flag, "guest_ok", translate("Allow guests"))
go.rmempty = false
go.enabled = "yes"
go.disabled = "no"

local sambausers = luci.sys.sambausers()
r = s:option(DynamicList, "users", translate("Allowed users"))
r.width   = "200px;"
for _, user in ipairs(sambausers) do
	r:value(user["username"], user["username"])
end
r:depends("guest_ok", "")

ro = s:option(Flag, "read_only", translate("Read-only"))
ro.rmempty = false
ro.enabled = "yes"
ro.disabled = "no"

-- cm = s:option(Value, "create_mask", translate("Create mask"))
-- cm.rmempty = true
-- cm.size = 4
-- 
-- dm = s:option(Value, "dir_mask", translate("Directory mask"))
-- dm.rmempty = true
-- dm.size = 4

return m
