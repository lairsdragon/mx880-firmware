--[[
LuCI - Lua Configuration Interface

Copyright 2015 Teltonika

]]--

module("luci.controller.radius", package.seeall)

function index()

entry({"admin", "services", "hotspot", "radius"}, arcombine(cbi("radius/radius"), cbi("radius/users_edit")),_("Radius Server"), 4).leaf = true


end