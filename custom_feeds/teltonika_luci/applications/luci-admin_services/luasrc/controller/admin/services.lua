--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008-2011 Jo-Philipp Wich <xm@subsignal.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: system.lua 8122 2011-12-20 17:35:50Z jow $
]]--

module("luci.controller.admin.services", package.seeall)
local utl = require "luci.util"
function index()
	local uci = require("luci.model.uci").cursor()
	ExpertMode = uci:get("system", "system", "expert")

	--ACCESS CONTROLL
		if ExpertMode and ExpertMode == "1" then
			entry({"admin", "services", "access_control"}, alias("admin", "services", "access_control", "general"), _("HTTP/SSH"), 2)
				entry({"admin", "services", "access_control", "general"}, cbi("admin_services/admin_access_control"), _("General"), 1).leaf = true
				entry({"admin", "services", "access_control", "safety"}, cbi("admin_services/safety"), _("Safety"), 2).leaf = true
		else 
			entry({"admin", "services", "access_control"}, cbi("admin_services/admin_access_control"), _("HTTP/SSH"), 2)
		end

	if ExpertMode and ExpertMode == "1" then
		-- SNMP
		entry({"admin", "services", "snmp"}, alias("admin", "services", "snmp", "snmp-settings"), _("SNMP"), 4)
			entry({"admin", "services", "snmp", "snmp-settings" }, cbi("tlt-snmp/tlt-snmp"), _("SNMP Settings"), 1).leaf = true
			entry({"admin", "services", "snmp", "trap-settings"},arcombine(cbi("tlt-snmp/tlt-trap"), cbi("tlt-snmp/tlt-trap-details")),_("Trap Settings"), 2).leaf = true

		-- DYNAMIC DNS
		entry({"admin", "services", "ddns"}, cbi("ddns/ddns_first"), _("Dynamic DNS"), 5)
			entry({"admin", "services", "ddns_edit"}, cbi("ddns/ddns"), nil).leaf = true

		--VRRP
		entry( {"admin", "services", "vrrp"}, cbi("vrrp"), _("VRRP"), 7)
		
		rs232 = uci:get("hwinfo", "hwinfo", "rs232")
		rs485 = uci:get("hwinfo", "hwinfo", "rs485")

		if rs232 == "1" then
			entry({"admin", "services", "rs"},  alias("admin", "services", "rs", "rs232"), _("RS232/RS485"), 8)
			entry({"admin", "services", "rs", "rs232"}, cbi("rs/rs232"), _("RS232"), 1)
		end
		if rs485 == "1" then
			if rs232 == "0" then
				entry({"admin", "services", "rs"},  alias("admin", "services", "rs", "rs485"), _("RS232/RS485"), 8)
			end
			entry({"admin", "services", "rs", "rs485"}, cbi("rs/rs485"), _("RS485"), 2)
		end

		in_out = uci:get("hwinfo", "hwinfo", "in_out")
		if in_out == "1" then
			entry({"admin", "services", "input-output"}, alias("admin", "services", "input-output", "status"), _("I/O Ports"), 9).i18n = "input-output"
			entry({"admin", "services", "input-output", "status"}, template("input-output/status"), _("Status"), 10).leaf = true
			entry({"admin", "services", "input-output", "inputs"}, arcombine(cbi("input-output/inputs"), cbi("input-output/input-details")), _("Input"), 20).leaf = true
			entry({"admin", "services", "input-output", "output"}, alias("admin", "services", "input-output", "output", "output_configuration"), _("Output"), 30)
			entry({"admin", "services", "input-output", "output", "output_configuration"}, cbi("input-output/output_configuration"), _("Output Configuration"), 1).leaf = true
			entry({"admin", "services", "input-output", "output", "on_off"}, template("input-output/output"), _("ON/OFF"), 2).leaf = true
			entry({"admin", "services", "input-output", "output", "post_get"}, cbi("input-output/post_get"), _("Post/Get Configuration"), 3).leaf = true
			entry({"admin", "services", "input-output", "output", "periodic"}, arcombine(cbi("input-output/periodic_control"), cbi("input-output/periodic_control_details")), _("Periodic Control"), 4).leaf = true
			entry({"admin", "services", "input-output", "output", "scheduler"}, cbi("input-output/output_scheduler"), _("Scheduler"), 5).leaf = true
		end
		
		usb = uci:get("hwinfo", "hwinfo", "usb")
		microsd = uci:get("hwinfo", "hwinfo", "microsd")
		if usb == "1" or microsd == "1" then
			entry({"admin", "services", "samba"}, alias("admin", "services", "samba", "filesystem"), _(translate("microSD/USB"), 10))
			entry({"admin", "services", "samba", "filesystem"}, cbi("filesystem"), _(translate("Mounted file systems")), 1).leaf=true
			entry({"admin", "services", "samba", "samba"}, cbi("samba"), _(translate("Samba")), 2).leaf=true
			entry({"admin", "services", "samba", "user"}, cbi("samba_user"), _(translate("Samba user")), 3).leaf=true
			entry({"admin", "services", "samba", "edit"}, call("safe_remove"), nil).leaf = true
		end
		
		gps = uci:get("hwinfo", "hwinfo", "gps")
		if gps == "1" then
			entry({"admin", "services", "gps"}, call("go_to"), _("GPS"), 15)
			entry({"admin", "services", "gps", "general"}, template("gps/gps"), _("GPS"), 1).leaf = true
			entry({"admin", "services", "gps", "settings"}, cbi("gps/gps_settings"), _("GPS Settings"), 2).leaf = true
		end
		
	end
end

function go_to()
	local enabled = utl.trim(luci.sys.exec("uci -q get gps.gps.enabled")) or "0"
	if enabled == "1" then
		luci.http.redirect(luci.dispatcher.build_url("admin", "services", "gps", "general").."/")
	else
		luci.http.redirect(luci.dispatcher.build_url("admin", "services", "gps", "settings"))
	end
end

function safe_remove(id)
	local mounts = luci.sys.mounts()
	local v = mounts[tonumber(id)]["fs"]
	luci.sys.call("/etc/init.d/samba stop; umount -l " .. v .. "; /etc/init.d/samba start")
	luci.http.redirect(luci.dispatcher.build_url("admin/services/samba"))
end

