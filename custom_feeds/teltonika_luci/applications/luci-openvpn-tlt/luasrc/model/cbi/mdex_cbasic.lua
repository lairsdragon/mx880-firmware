--[[ NOTE: kadangi idetas TAP pasirinkimas, tai atsirado tokie pakeitimai:
+ tap atsiranda tik esant tls
+ jei serveris:
	+ 'dev' = 'tap'
	+ 'server' keiciasi i 'server_bridge' = 'nogw'
	+ atsiranda checkbox 'duplicate_cn'
	+ dingsta IP ir netmask
+ jei clientas
	+ 'dev' = 'tap'
]]
local sys = require("luci.sys")
local ipc = require("luci.ip")
local uci = require("luci.model.uci").cursor()
local utl = require "luci.util"
local dsp = require "luci.dispatcher"
-- local mix = require("luci.mtask")


local VPN_INST, TMODE

local function cecho(string)
	sys.call("logger -t vpn \"" .. string .. "\"")
end

if arg[2] then
	VPN_INST = arg[2]
else
	--print("[Openvpn.cbasic] Fatal Err: Pass openvpn instance failed")
	--Shoud redirect back to overview
	return nil
end

local mode, o


function split(pString, pPattern)
	local Table = {}
	local fpat = "(.-)" .. pPattern
	local last_end = 1
	local s, e, cap = pString:find(fpat, 1)
	while s do
		if s ~= 1 or cap ~= "" then
			table.insert(Table,cap)
		end
		last_end = e+1
		s, e, cap = pString:find(fpat, last_end)
	end
	if last_end <= #pString then
		cap = pString:sub(last_end)
		table.insert(Table, cap)
	end
	return Table
end

local m = Map("openvpn", translatef("OpenVPN Instance: %s", VPN_INST:gsub("^%l", string.upper)), "")
m.redirect = dsp.build_url("admin/vpn/openvpn-tlt/")

if VPN_INST then
	--ROLE = VPN_INST:match("%l+")
	local a1 ={}
	a1 = VPN_INST:split("_")
	ROLE = a1[2]:lower()
	TNAME = a1[3]
	m.spec_dir = "/etc/openvpn/"
end


local s = m:section( NamedSection, VPN_INST, "openvpn", translate("Main Settings"), "")

o = s:option( Flag, "enable", translate("Enable"), translate("Enable current configuration"))
	o.forcewrite = true
	o.rmempty = false
	
proto = s:option( ListValue, "proto", translate("Protocol"), translate("A transport protocol used for connection. You can choose here between TCP and UDP"))
	proto:value("udp", translate("UDP"))
	proto:value("tcp-client", translate("TCP"))
	proto.default = "udp"

o = s:option( Value, "port", translate("Port"), translate("TCP/UDP port for both local and remote endpoint. Make sure that this port is open in firewall") )
	o.datatype = "port"
	o.rmempty = false

	function o.write(self, section, value)
		m.uci:set("openvpn", VPN_INST, "port", value)
		m.uci:save("openvpn")
		local fwRuleInstName = "nil"
		local needsPortUpdate = false

		m.uci:foreach("firewall", "rule", function(s)
				if s.name == "Allow-vpn-traffic" then
					fwRuleInstName = s[".name"]
					m.uci:set("firewall", fwRuleInstName, "dest_port", value)
					m.uci:save("firewall")
					m.uci.commit("firewall")

				end
			end)
		--if needsPortUpdate == true then

		--end
	end
	
if ROLE ~= "publicip" then
	lzo = s:option( Flag, "comp_lzo", "Enable LZO compression", translate("Use fast LZO compression. With LZO compression your VPN connection will generate less network traffic") )
		lzo.default = "yes"
		lzo.rmempty = false
		lzo.enabled = "yes"
		lzo.disabled = "no"
end

local remote = s:option( Value,"remote", translate("Remote host/IP address"), translate("IP address or domain name of OpenVPN server"))
	remote.rmempty = false
	if ROLE == "mpn" then
		remote.default = ""
	elseif ROLE == "businessip" then
		remote.default = "businessip.mdex.de"
	elseif ROLE == "publicip" then
		remote.default = "publicip.mdex.de"
	else
		remote.default = "fixedip.mdex.de"
	end

local cipher = s:option( ListValue, "cipher", translate("Cipher"), translate("Packet encryption algorithm (cipher)") )
	if ROLE == "publicip" then
		cipher.default = "none"
	else
		cipher.default = "BF-CBC"
	end
	
	cipher:value("none", translate("none"))
	cipher:value("DES-CBC", translate("DES-CBC 64"))
	cipher:value("RC2-CBC", translate("RC2-CBC 128"))
	cipher:value("DES-EDE-CBC", translate("DES-EDE-CBC 128"))
	cipher:value("DES-EDE3-CBC", translate("DES-EDE3-CBC 192"))
	cipher:value("DESX-CBC", translate("DESX-CBC 192"))
	cipher:value("BF-CBC", translate("BF-CBC 128 (default)"))
	cipher:value("RC2-40-CBC", translate("RC2-40-CBC 40"))
	cipher:value("CAST5-CBC", translate("CAST5-CBC 128"))
	cipher:value("RC2-64-CBC", translate("RC2-64-CBC 64"))
	cipher:value("AES-128-CBC", translate("AES-128-CBC 128"))
	cipher:value("AES-192-CBC", translate("AES-192-CBC 192"))
	cipher:value("AES-256-CBC", translate("AES-256-CBC 256"))
	
if ROLE == "publicip" then	
	auth_alg = s:option( ListValue, "auth", translate("HMAC authentication algorithm"), translate(""))
		auth_alg:value("none", translate("None"))
		auth_alg:value("sha1", translate("SHA1 (default)"))
		auth_alg:value("sha256", translate("SHA256"))
		auth_alg:value("sha384", translate("SHA384"))
		auth_alg.default = "none"
end
	
local fragmentation = s:option(Value, "fragment", translate("Maximum fragment size"))
	fragmentation:depends("proto", "udp")
	
	if ROLE == "publicip" then
		fragmentation.default = "1452"
	else
		fragmentation.default = "1300"
	end
	
local mssfix = s:option(Flag, "_mssfix", translate("Limit TCP packet size"))
	mssfix:depends("proto", "udp")
	mssfix.default = "1"
	mssfix.rmempty = false
	
	function mssfix.write(self, section, value)
		self.map:set(section, self.option, value)
		
		if value and value == "1" then
			local max_fragment = fragmentation:formvalue(section)
			self.map:set(section, "mssfix", max_fragment)
		else
			self.map:del(section, "mssfix")
		end
	end
	
local vpn_nat = s:option(Flag, "_vpn_nat", translate("Enable VPN NAT"))
	vpn_nat.default = "1"
	vpn_nat.rmempty = false
	
	function vpn_nat.write(self, section, value)
		value = value or "0"
		local masq
		m.uci:foreach("firewall", "zone", function(sec)
			if sec["name"] and sec["name"] == "vpn" then
				masq = sec["masq"] or "0"
				if value ~= masq then
					m.uci:set("firewall", sec[".name"], "masq", value)
				end
			end
		end)
		
		m.uci:save("firewall")
		m.uci:commit("firewall")
	end
	
	function vpn_nat.cfgvalue(self, section)
		local masq
		m.uci:foreach("firewall", "zone", function(sec)
			if sec["name"] and sec["name"] == "vpn" then
				masq = sec["masq"]
			end
		end)
		
		return masq or "0"
	end
	
local float = s:option(Flag, "float", translate("Allow remote peer's IP change"))
	float.default = "1"
	float.rmempty = false
	
local nobind = s:option(Flag, "nobind", translate("Do not bind to local address"))
	nobind.default = "1"
	nobind.rmempty = false
	
local key_interval = s:option(Value, "reneg_sec", translate("Key renegotiation interval"))
	key_interval.default = "86400"
	
local overide = s:option(Flag, "redirect_gateway", translate("Override gateway"))
	overide.rmempty = false
	overide.enabled = "def1"
	overide.disabled = ""
	
	if ROLE == "publicip" then
		overide.default = "def1"
	else
		overide.default = ""
	end
		
local throughput = s:option(Flag, "fast_io", translate("Optimize VPN throughput"))
	throughput.rmempty = false
	
	if ROLE == "publicip" then
		throughput.default = "1"
	else
		throughput.default = ""
	end

extra = s:option(TextValue, "_extra", "Extra options")
	extra.template = "openvpn/tvalue"

if ROLE == "mpn" then
	verify_name = s:option(TextValue,"verify_x509_name", translate("Verify X.509 name"))
		verify_name.template = "openvpn/tvalue"

	cert_type = s:option(ListValue,"remote_cert_tls", translate("Remote certificate type"))
		cert_type:value("client", translate("Client"))
		cert_type:value("server", translate("Server"))
		
	hmac = s:option(Value,"auth", translate("HMAC authentication algorithm"))

	keysize = s:option(Value,"keysize", translate("Cipher key size"))
	
	tls_version = s:option(Value,"tls_version_min", translate("Minimum TLS version"))

 	tls_cipher  = s:option(Value,"tls_cipher", translate("TLS cipher suite"))

end



auth = s:option(ListValue,"_auth", translate("Authentication"), translate("Authentication mode used to secure data session") )
	auth:value("pass", translate("Password"))
	if ROLE == "mpn" then
		auth:value("tls", translate("Certificate"))
		auth.default = "tls"
	end
	auth.nowrite = true
	
user_name = s:option( Value, "user", translate("User name"), translate("VPN client user name"))
	user_name:depends({_auth="pass"})
	
	function user_name.cfgvalue(self, section)
		local userval  = utl.trim(sys.exec("head -n 1 /etc/openvpn/auth_"..VPN_INST.."")) or ""
		return userval
	end
	
	function user_name.write(self, section)
	end

password = s:option( Value, "pass", translate("Password"), translate("VPN client password"))
	password:depends({_auth="pass"})
	
	function password.cfgvalue(self, section)
		local passval = utl.trim(sys.exec("head -n 2 /etc/openvpn/auth_"..VPN_INST.." | tail -n 1 2>/dev/null")) or ""
		return passval
	end
	
	function password.write(self, section)
	end
	
ca = s:option( FileUpload, "ca", translate("Certificate authority"), translate("The digital certificate verifies the ownership of a public key by the named subject of the certificate"))


crl_verify = s:option( FileUpload, "crl_verify", translate("Certificate revocation list"))

if ROLE == "mpn" then
	o = s:option( FileUpload, "cert", translate("Client certificate"), translate("Identify a client or a user, authenticating the client to the server and establishing precisely who they are"))
		o:depends({_auth="tls"})
		
	o = s:option( FileUpload, "key", translate("Client key"), translate("It has been generated for the same purpose as client certificate"))
		o:depends({_auth="tls"})
		
	tls_auth = s:option( Flag, "_tls_auth", "HMAC authentication", translate("Add an additional layer of HMAC authentication on top of the TLS control channel to protect against DoS attacks.") )
		tls_auth.rmempty = false

	tls_key = s:option( FileUpload, "tls_auth", translate("HMAC authentication key"), translate(""))
		tls_key:depends({_tls_auth="1"})

		function tls_key.cfgvalue(self, section)
			local val = self.map:get(section, "tls_auth")
			if val ~= nil and val ~= "" then
				t=split(val," ")
			else
				t={""}
			end
			return t[1]
		end

		function tls_key.write(self, section, value)
			local s = tls_auth:formvalue(section)
			AbstractValue.write(self, section, value .. " ".. s)
		end
end


function m.on_parse(self)
	--cecho("Entering on parse.")

	local _auth = m:formvalue("cbid.openvpn." .. VPN_INST .. "._auth")

	if not _auth then
		return
	end

	m.uci:delete("openvpn", VPN_INST, "client_config_dir")
	m.uci:delete("openvpn", VPN_INST, "auth_user_pass")

	if _auth == "pass" then
		m.uci:set("openvpn", VPN_INST, "auth_user_pass", "/etc/openvpn/auth_"..VPN_INST.."")
	end
end

function m.on_commit(map)

	local auth = m:formvalue("cbid.openvpn." .. VPN_INST .. "._auth")
	local vpnEnable = m:formvalue("cbid.openvpn." .. VPN_INST .. ".enable")
	-- it's not PID, this function returns fail or success (shell var $? = 0 or 1)
	local vpnPIDstatus = sys.call("pidof openvpn > /dev/null")
	local vpnRunning = false

	if vpnPIDstatus == 0 then
		vpnRunning = true
	end
	
	if auth == "pass" then
		if TMODE ~= "server" then
			local user = m:formvalue("cbid.openvpn." .. VPN_INST .. ".user")
			local pass = m:formvalue("cbid.openvpn." .. VPN_INST .. ".pass")
			sys.call("echo "..user.." > /etc/openvpn/auth_"..VPN_INST.."")
			sys.call("echo "..pass.." >> /etc/openvpn/auth_"..VPN_INST.."")
		end
	else
		sys.call("rm /etc/openvpn/auth_" .. VPN_INST .. " 2>/dev/null >/dev/null")
	end
	
	if vpnEnable then
	
		--Delete all usr_enable from openvpn config
		m.uci:foreach("openvpn", "openvpn", function(s)
			local usr_enable = s.usr_enable or ""
			local name = s[".name"]:split("_")
			if TMODE == name[1] then
				if usr_enable == "1" then
					open_vpn = s[".name"]
					m.uci:delete("openvpn", open_vpn, "usr_enable")
				end
			end
		end)
		m.uci:save("openvpn")
		m.uci.commit("openvpn")
		
		if vpnRunning then
			sys.call("/etc/init.d/openvpn reload > /dev/null")
			sys.call("/etc/init.d/firewall restart >/dev/null")
		else
			sys.call("/etc/init.d/openvpn start > /dev/null")
			sys.call("/etc/init.d/firewall restart >/dev/null")
		end
	else
		sys.call("/etc/init.d/openvpn stop > /dev/null")
	end
end

return m
