--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: zones.lua 8108 2011-12-19 21:16:31Z jow $
]]--

local ds = require "luci.dispatcher"
local fw = require "luci.model.firewall"

local m, s, o, p, i, v

m = Map("firewall",
	translate("Firewall"),
	translate("General settings allows you to set up default firewall policy."))

fw.init(m.uci)

s = m:section(TypedSection, "defaults", translate("Default Settings"))
-- s:option(Flag, "syn_flood", translate("Enable SYN flood protection"), translate("Makes router more resistant to SYN flood attacks"))

o = s:option(Flag, "drop_invalid", translate("Drop invalid packets"), translate("A Drop action is performed on a packet that is determined to be invalid"))
o.default = o.disabled

p = {
	s:option(ListValue, "input", translate("Input"), translate("DEFAULT* action that is to be performed for packets that pass through the Input chain")),
	s:option(ListValue, "output", translate("Output"), translate("DEFAULT* action that is to be performed for packets that pass through the Output chain")),
	s:option(ListValue, "forward", translate("Forward"), translate("DEFAULT* action that is to be performed for packets that pass through the Forward chain"))
}

for i, v in ipairs(p) do
	v:value("REJECT", translate("Reject"))
	v:value("DROP", translate("Drop"))
	v:value("ACCEPT", translate("Accept"))
end

-- o = s:option(Flag, "masq", translate("Masquerade LAN interface"), translate("This making port forwardings work to devices which don't have the default gateway IP configured correctly"))
-- o.default = o.enabled


s = m:section(TypedSection, "zone", translate("Firewall Zone Policies"))
s.template = "cbi/tblsection"
s.anonymous = true
s.addremove = false


o = s:option(DummyValue, "zone_name", translate("Zone name"), translate("Zone name"))
function o.cfgvalue(self, section)
	local z = self.map:get(section, "name")
	if z == "lan" then
		return translate("LAN zone")
	elseif z == "wan" then
		return translate("WAN zone")
	elseif z == "vpn" then
		return translate("VPN zone")
	end
end


o = s:option(Flag, "drop_invalid", translate("Drop invalid packets"), translate("A Drop action is performed on a packet that is determined to be invalid"))
o.width = "10%"
o.default = o.disabled

p = {
	s:option(ListValue, "input", translate("Input"), translate("DEFAULT* action that is to be performed for packets that pass through the Input chain")),
	s:option(ListValue, "output", translate("Output"), translate("DEFAULT* action that is to be performed for packets that pass through the Output chain")),
	s:option(ListValue, "forward", translate("Forward"), translate("DEFAULT* action that is to be performed for packets that pass through the Forward chain"))
}

for i, v in ipairs(p) do
	v:value("REJECT", translate("Reject"))
	v:value("DROP", translate("Drop"))
	v:value("ACCEPT", translate("Accept"))
end

o = s:option(Flag, "masq", translate("Masquerade interface"), translate("This making port forwardings work to devices which don't have the default gateway IP configured correctly"))
o.rmempty = false
o.default = "1"

return m
